'''
Author: Mohamed Aly
Email: maly@cern.ch
Description:

This script is responsible for loading slimmed Ntuples and plotting requested
variables for one process but different generators in various PS regions. This script
CANNOT be used with real DATA.

  - The path to the slimmed datasets -- same as output path form thbb_slimming.py
  - The process to be studied
  - The variables to plot
  - The MC generators
  - PS regions to make the plots in.
  - The path where the plots should be saved

'''

import uproot4 as uproot
from argparse import ArgumentParser, ArgumentTypeError
import sys
import os
import numpy as np
import pandas as pd
import h5py
import awkward as ak
from termcolor import cprint
from pathlib import Path
from tools.common import dhelp, REGION_TO_REGION_NAME_MAP
from tools.input_studies_common import (PROC_TO_AVAIL_GENS_MAP,
                                        GEN_IN_ARG_TO_GEN_NAME_MAP)
from tools.var_getters import VAR_TO_BRANCH_NAME_MAP, VAR_TO_EDGES_MAP
from tools.selectors import filter_by_region
from tools.style import GEN_TO_COLOR_MAP, GEN_TO_PRETTY_NAME, VAR_TO_PRETTY_NAME, PROC_TO_PRETTY_NAME
from tools.plotting import (Canvas, atlas_style, add_atlas_label, apply_plot_settings,
                            stick_text_on_plot, add_presel_cuts)
# ==================================================================
# Get arguments from CL
# ==================================================================
# Help messages
# ================
_OUTDIR_HELP = dhelp('Path to directory where output files will be stored ')
_INPUT_HELP = dhelp('The directory where the slimmed json files are stored')
_PROCESSES_HELP = dhelp('The samples to check in form: proc_name,(optional)gen,(optional)FS/AFII')
_GENS_HELP = dhelp('The generators to compare')
_VARS_HELP = dhelp('The branch to load from the Ntuples')
_REGIONS_HELP = dhelp('The Phase-Space regions to make plots in')
# ================
# Defaults
# ================

DEFAULT_IN_DIR = '/eos/user/m/maly/thbb/analysis_files/slimmed_Ntuples/'
DEFAULT_OUT_DIR = './output/proc_fracs/'
DEFAULT_PROC = 'ttb'
DEFAULT_GENS = 'nominal'
DEFAULT_VARS = 'bdt_tH'
DEFAULT_REGIONS = ['preselec', 'tH', 'ttb', 'ttc', 'ttlight', 'others']


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('--indir', default=DEFAULT_IN_DIR, help=_INPUT_HELP)
    parser.add_argument('--outdir', type=Path, default=DEFAULT_OUT_DIR, help=_OUTDIR_HELP)
    parser.add_argument('--procs', nargs="+", choices=PROC_TO_AVAIL_GENS_MAP.keys(), default=DEFAULT_PROC, help=_PROCESSES_HELP)
    parser.add_argument('--generator', choices=GEN_IN_ARG_TO_GEN_NAME_MAP.keys(), default=DEFAULT_GENS, help=_GENS_HELP)
    parser.add_argument('--regions', nargs="+", choices=REGION_TO_REGION_NAME_MAP.keys(), default=DEFAULT_REGIONS, help=_REGIONS_HELP)
    parser.add_argument('--proc_category', type=str)
    parser.add_argument('--var', choices=VAR_TO_BRANCH_NAME_MAP.keys(), default=DEFAULT_VARS, help=_VARS_HELP)
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--unweight', action='store_true')
    return parser.parse_args()


def run():
	# ======================
	# Prepare args
	# ======================
	args = get_args()
	# Be Verbose ?
	verbose = args.verbose
	# Make unweighted histos ?
	unweight = args.unweight
	# Ntuples Directory
	in_dir = args.indir
	# Slimmed files directory
	out_dir = args.outdir
	out_dir.mkdir(parents=True, exist_ok=True)
	# The prcoess requested
	procs = args.procs
	# Category for all these procs
	umbrella_proc = args.proc_category
	# The MC generators to compare
	gen = args.generator
	# The variables to plot
	var = args.var
	# The PS regions to compare
	regions = args.regions

	if verbose:
		cprint(f'INFO:: The samples to check is {procs} and the generators is {gen}\n', 'green')
	# Loaad the input datasets
	proc_gen_combos = [(proc, gen) for proc in procs]
	proc_gen_to_vars_map = prepare_input_data(in_dir, proc_gen_combos, var)
	# Loop over the regions of phase-space to make plots in
	for region in regions:
		# Get the region name as used in the analysis configuration
		proper_region = REGION_TO_REGION_NAME_MAP[region]
		if verbose:
			cprint(f'INFO:: Processing the following region: {proper_region}', 'green')
		
		if verbose:
			cprint(f'INFO:: Processing the following variable: {var}', 'green')
		proc_gen_to_integral = {}
		# Loop over the generators
		for proc_gen, unfiltered_data in proc_gen_to_vars_map.items():
			# Filter the data to select the current region
			data = filter_by_region(unfiltered_data, proper_region)
			# Get the variable data
			var_data = np.array(data[var])
			weights = np.array(data['weights'])
			hist = np.histogram(var_data, weights=weights)[0]
			proc_gen_to_integral[proc_gen] = np.sum(hist)

		with Canvas(out_dir/umbrella_proc/var/f'{umbrella_proc}_{var}_{proper_region}_fracs.pdf') as can:
			can.ax.pie([float(proc_gen_to_integral[v]) for v in proc_gen_to_integral], 
				       labels=[PROC_TO_PRETTY_NAME[''.join(str(k).split("_")[:-1])] for k in proc_gen_to_integral], 
				       autopct='%1.1f%%',
				       shadow=True,
				       textprops={'fontsize': 28, 'weight': 'bold'})

# ==================================================================
# Read in the slimmed datasets and get a dataframe for each
# Process+Generator combo with all requested variables, if they are available
# ==================================================================
def prepare_input_data(in_dir, proc_gen_combos, requested_var):
	# Prepare a dictionary to hold the dataframe for each proc+gen combo
	proc_gen_to_vars_map = {}
	# Loop over the requested proc+generator combos
	for proc_gen_combo in proc_gen_combos:
		# Make a string with the proc and generator combined
		str_combo = proc_gen_combo[0]+'_'+proc_gen_combo[1]
		# Array to hold the dataframe from each slimmed input file
		# There are multiple files due to the strucure of slimming script
		list_of_dfs = []
		# Loop over the files in the slimmed datasets directory
		for file_name in os.listdir(in_dir):
			# Skip irrelevant files
			if str_combo not in file_name:
				continue
			# Prepare a dictionary to hold the different datasets in each file
			# N-datasets for N variables kept during slimming
			var_name_to_data = {}
			# Open each file
			with h5py.File(in_dir+file_name, "r") as file:
				# Loop over the available variables, and save their data
				for var_name in list(file.keys()):
					var_name_to_data[var_name] = np.array(file[var_name])
			# Make a dataframe with columns = variables
			df = pd.DataFrame(var_name_to_data)
			# Append the dataframe from this file to the ones from other files
			list_of_dfs.append(df)
		# If no files were found for the requested proc+gen combo, raise error
		try:
			#print(list_of_dfs)
			proc_gen_to_vars_map[str_combo] = pd.concat(list_of_dfs, ignore_index=True)
		except ValueError:
			raise ValueError(f'The requested process+generator combo {str_combo} is not available in the slimmed datasets...')
	# Finally, get the names of variables available from the slimming,
	# and cross-check them again requested variables
	avail_variables = list(proc_gen_to_vars_map[list(proc_gen_to_vars_map.keys())[0]].columns)
	# Raise error if requested variables not available in slimmed variables
	if requested_var not in avail_variables:
		raise ValueError(f'The variables requested are not all in the slimmed ntuples. You can choose from: {avail_variables}\n')

	return proc_gen_to_vars_map


# ==================================================================
# Get the error on weighted histogram
# ==================================================================
def get_errs(var_data, weights, histo):
	histo_edges = np.array(histo[1])
	all_errs = []
	all_centers = []
	for bin_edge_idx in range(len(histo_edges)-1):
		this_edge = histo_edges[bin_edge_idx]
		next_edge = histo_edges[bin_edge_idx+1]
		data_in_bin = np.logical_and(var_data > this_edge, var_data <= next_edge)
		weights_in_bin = weights[data_in_bin]
		weights_sq = [weights_in_bin**2]
		err_sq = np.sum(weights_sq)
		err = np.sqrt(err_sq)
		all_errs.append(err)
		all_centers.append((this_edge+next_edge)/2)
	return all_errs, all_centers


if __name__ == '__main__':
    run()
