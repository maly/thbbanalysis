'''
Brief: 

Selection Methods 

'''
import numpy as np


def get_overall_selection(lazy_branches,variable,cuts,region,region_cut_dict,sample,sample_cut_dict,TotalNumEvents):
	all_cuts = []
	region_and_sample_cut = get_region_and_sample_selection(region,region_cut_dict,sample,sample_cut_dict)
	overall_sel = region_and_sample_cut
	for cut in cuts:
		cut_var, cut_val = cut.split('>')
		cut_sign = '>'
		cut_var_branch =  lazy_branches[cut_var][:]
		selection = np.array(eval('cut_var_branch'+cut_sign+cut_val))
		overall_sel = overall_sel & selection

	return overall_sel

#Filter data for different regions 
def get_region_and_sample_selection(region, region_cut_dict,sample,sample_cut_dict):
	region_cut = region_cut_dict[region]
	sample_cut = sample_cut_dict[sample]
	full_cut = region_cut & sample_cut
	return full_cut

