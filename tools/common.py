'''
Author: Mohamed Aly
Email: maly@cern.ch
Description:

Common tools and dictionaries for the TRExFitter Debugger in tH(bb)q single-lep

'''
import math


# Provide help for an argument in ARG Parser
def dhelp(info=None):
    """
    Help string formatter
    """
    if info:
        return f'{info}: default: %(default)s'
    return 'default: %(default)s'


# Integrate a histogram read by uproot
def integrate_uproot_histo(histo):
    bin_content = histo.values()
    integral = sum(bin_content)
    return integral


def round_floats(n, decimals=0):
    multiplier = 10 ** decimals
    return math.floor(n*multiplier + 0.5) / multiplier


def make_trexfitter_arg(job_name, region, sample, syst):
    return f'trex-fitter n config/{job_name}.config Regions={region}:Samples={sample}:Systematics={syst}*:SaveSuffix=_{region}_{syst}*_{sample}\n'


REGION_TO_REGION_NAME_MAP = {
    'ttb': 'CR_ttb',
    'ttc': 'CR_ttc',
    'ttlight': 'CR_ttlight',
    'tH': 'SR',
    'others': 'CR_others',
    'preselec': 'PR',
    'none': 'none',
}