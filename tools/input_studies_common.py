'''
Author: Mohamed Aly
Email: maly@cern.ch
Description:

Common tools and dictionaries to perform checks on
the NTuples fed to TRExFitter during the (n) step

'''
from argparse import ArgumentParser, ArgumentTypeError
# A map from the process+generator to DSID
# PROC_DSID_MAP = {
#  'singletop_Wtchannel_nominal':      ['410646', '410647'],
#  'singletop_Wtchannel_aMCNloP8':     ['412002_AFII'],
#  'singletop_Wtchannel_PowhegH7_0_4': ['411036_AFII', '411037_AFII'],
#  'singletop_tchannel_nominal':       ['410658', '410659'],
#  'singletop_tchannel_aMCNloP8':      ['412004_AFII'],
#  'singletop_tchannel_PowhegH7_0_4':  ['411032_AFII', '411033_AFII'],
#  'singletop_schannel_nominal':       ['410644', '410645'],
#  'singletop_schannel_aMCNloP8':      ['412005_AFII'],
#  'singletop_schannel_PowhegH7_0_4':  ['411034_AFII', '411035_AFII'],
#  'Wjets_nominal':                    ['364156', '364159', '364162', '364165',
#                                       '364170', '364173', '364176', '364179',
#                                       '364184', '364187', '364190', '364193', '364157',
#                                       '364160', '364163', '364166', '364171', '364174',
#                                       '364177', '364180', '364185', '364188', '364191',
#                                       '364194', '364158', '364161', '364164', '364167',
#                                       '364172', '364175', '364178', '364181', '364186',
#                                       '364189', '364192', '364195', '364168', '364169',
#                                       '364182', '364183', '364196', '364197'],
#  'Zjets_nominal':                    ['364100', '364103', '364106', '364109', '364114', '364117',
#                                       '364120', '364123', '364128', '364131', '364134', '364137',
#                                       '364101', '364104', '364107', '364110', '364115', '364118',
#                                       '364121', '364124', '364129', '364132', '364135', '364138',
#                                       '364102', '364105', '364108', '364111', '364116', '364119',
#                                       '364122', '364125', '364130', '364133', '364136', '364139',
#                                       '364112', '364113', '364126', '364127', '364140', '364141'],
#  'VV_nominal':                       ['364250', '364253', '364254', '364255',
#                                       '364288', '364289', '364290', '363355',
#                                       '363356', '363357', '363358', '363359',
#                                       '363360', '363489', '363494'],
#  'ttW_nominal':                      ['410155'],
#  'ttW_sherpa228':                    ['700000'],
#  'ttZ_nominal':                      ['410156', '410157', '410218', '410219', '410220'],
#  'ttZ_sherpa228':                    ['410142', '410143'],
#  'ttH_nominal':                      ['346343', '346344', '346345'],
#  'ttH_aMCNloP8':                     ['346443_AFII', '346444_AFII', '346445_AFII'],
#  'ttH_PowhegH7_0_4':                 ['346346_AFII', '346347_AFII', '346348_AFII'],
#  'ttb_nominal':                      ['410470'],
#  'ttb_aMCNloP8':                     ['410464_AFII', '410465_AFII', '410466_AFII'],
#  'ttb_PowhegH7_0_4':                 ['410557_AFII', '410558_AFII'],
#  'ttb_PowhegH7_1_3':                 ['411233_AFII', '411234_AFII'],
#  'ttb_aMCNloH7_1_3':                 ['412116_AFII', '412117_AFII'],
#  'ttc_nominal':                      ['410470'],
#  'ttc_aMCNloP8':                     ['410464_AFII', '410465_AFII', '410466_AFII'],
#  'ttc_PowhegH7_0_4':                 ['410557_AFII', '410558_AFII'],
#  'ttc_PowhegH7_1_3':                 ['411233_AFII', '411234_AFII'],
#  'ttc_aMCNloH7_1_3':                 ['412116_AFII', '412117_AFII'],
#  'ttlight_nominal':                  ['410470'],
#  'ttlight_aMCNloP8':                 ['410464_AFII', '410465_AFII', '410466_AFII'],
#  'ttlight_PowhegH7_0_4':             ['410557_AFII', '410558_AFII'],
#  'ttlight_PowhegH7_1_3':             ['411233_AFII', '411234_AFII'],
#  'ttlight_aMCNloH7_1_3':             ['412116_AFII', '412117_AFII'],
#  'ttbar_nominal':                    ['410470'],
#  'ttbar_aMCNloP8':                   ['410464_AFII', '410465_AFII', '410466_AFII'],
#  'ttbar_PowhegH7_0_4':               ['410557_AFII', '410558_AFII'],
#  'ttbar_PowhegH7_1_3':               ['411233_AFII', '411234_AFII'],
#  'ttbar_aMCNloH7_1_3':               ['412116_AFII', '412117_AFII'],
# }

# (v34) A map from the process+generator to DSID
PROC_DSID_MAP = {
 'singletop_Wtchannel_nominal':      ['410646', '410647'],
 'singletop_Wtchannel_aMCNloP8':     ['412002_AFII'],
 'singletop_Wtchannel_PowhegH7':     ['600727_AFII', '600728_AFII'],
 'singletop_tchannel_nominal':       ['410658', '410659'],
 'singletop_tchannel_aMCNloP8':      ['412004_AFII'],
 'singletop_tchannel_PowhegH7':      ['600017_AFII', '600018_AFII'],
 'singletop_tchannel_aMCNloH7':      ['500289_AFII'],
 'singletop_schannel_nominal':       ['410644', '410645'],
 'singletop_schannel_aMCNloP8':      ['412005_AFII'],
 'singletop_schannel_PowhegH7':      ['600019_AFII', '600020_AFII'],
 'singletop_tchannel_aMCNloH7':      ['500288_AFII'],
 'Wjets_nominal':                    ['364156', '364159', '364162', '364165',
                                      '364170', '364173', '364176', '364179',
                                      '364184', '364187', '364190', '364193', '364157',
                                      '364160', '364163', '364166', '364171', '364174',
                                      '364177', '364180', '364185', '364188', '364191',
                                      '364194', '364158', '364161', '364164', '364167',
                                      '364172', '364175', '364178', '364181', '364186',
                                      '364189', '364192', '364195', '364168', '364169',
                                      '364182', '364183', '364196', '364197'],
 'Zjets_nominal':                    ['364100', '364103', '364106', '364109', '364114', '364117',
                                      '364120', '364123', '364128', '364131', '364134', '364137',
                                      '364101', '364104', '364107', '364110', '364115', '364118',
                                      '364121', '364124', '364129', '364132', '364135', '364138',
                                      '364102', '364105', '364108', '364111', '364116', '364119',
                                      '364122', '364125', '364130', '364133', '364136', '364139',
                                      '364112', '364113', '364126', '364127', '364140', '364141'],
 'VV_nominal':                       ['364250', '364253', '364254', '364255',
                                      '364288', '364289', '364290', '363355',
                                      '363356', '363357', '363358', '363359',
                                      '363360', '363489', '363494'],
 'ttW_nominal':                      ['412123','410155'],
 'ttW_sherpa228':                    ['700168', '700205'],
 'ttZ_nominal':                      ['410156', '410157', '410218', '410219', '410220'],
 'ttZ_sherpa228':                    ['410142', '410143'],
 'ttH_nominal':                      ['346343', '346344', '346345'],
 'ttH_aMCNloP8':                     ['346443_AFII', '346444_AFII', '346445_AFII'],
 'ttH_PowhegH7':                   ['346346_AFII', '346347_AFII', '346348_AFII'],
 'ttb_nominal':                    ['410470'],
 'ttb_aMCNloP8':                   ['410464_AFII', '410465_AFII', '410466_AFII'],
 'ttb_PowhegH7':                   ['600666', '600667'],
 'ttb_PowhegH7_1_3':               ['411233_AFII','411234_AFII'],
 'ttb_aMCNloH7':                   ['412116_AFII', '412117_AFII'],
 'ttc_nominal':                    ['410470'],
 'ttc_aMCNloP8':                   ['410464_AFII', '410465_AFII', '410466_AFII'],
 'ttc_PowhegH7':                   ['600666', '600667'],
 'ttc_PowhegH7_1_3':               ['411233_AFII','411234_AFII'],
 'ttc_aMCNloH7':                    ['412116_AFII', '412117_AFII'],
 'ttlight_nominal':                    ['410470'],
 'ttlight_aMCNloP8':                   ['410464_AFII', '410465_AFII', '410466_AFII'],
 'ttlight_PowhegH7':                   ['600666', '600667'],
 'ttlight_PowhegH7_1_3':               ['411233_AFII','411234_AFII'],
 'ttlight_aMCNloH7':                   ['412116_AFII', '412117_AFII'],
 'ttbar_nominal':                    ['410470'],
 'ttbar_aMCNloP8':                   ['410464_AFII', '410465_AFII', '410466_AFII'],
 'ttbar_PowhegH7':                   ['600666', '600667'],
 'ttbar_PowhegH7_1_3':               ['411233_AFII','411234_AFII'],
 'ttbar_aMCNloH7':                   ['412116_AFII', '412117_AFII'],
}
PROC_CAMPAIGN_MAP = {
 'singletop_Wtchannel_nominal':      ['nom','syst'],
 'singletop_Wtchannel_aMCNloP8':     ['nom'],
 'singletop_Wtchannel_PowhegH7':     ['nom'],
 'singletop_tchannel_nominal':       ['nom','syst'],
 'singletop_tchannel_aMCNloP8':      ['syst'],
 'singletop_tchannel_PowhegH7':      ['nom'],
 'singletop_tchannel_aMCNloH7':      ['nom'],
 'singletop_schannel_nominal':       ['nom','syst'],
 'singletop_schannel_aMCNloP8':      ['nom'],
 'singletop_schannel_PowhegH7':      ['nom'],
 'singletop_tchannel_aMCNloH7':      ['nom'],
 'Wjets_nominal':                    ['syst'],
 'Zjets_nominal':                    ['syst'],
 'VV_nominal':                       ['syst'],
 'ttW_nominal':                      ['syst'],
 'ttW_sherpa228':                    ['nom'],
 'ttZ_nominal':                      ['syst'],
 'ttZ_sherpa228':                    ['nom'],
 'ttH_nominal':                      ['nom','syst'],
 'ttH_aMCNloP8':                     ['nom'],
 'ttH_PowhegH7':                      ['nom'],
 'ttb_nominal':                    ['nom','syst'],
 'ttb_aMCNloP8':                   ['nom'],
 'ttb_PowhegH7':                   ['nom'],
 'ttb_PowhegH7_1_3':                ['nom'],
 'ttb_aMCNloH7':                    ['nom'],
 'ttc_nominal':                     ['nom','syst'],
 'ttc_aMCNloP8':                   ['nom'],
 'ttc_PowhegH7':                   ['nom'],
 'ttc_PowhegH7_1_3':               ['nom'],
 'ttc_aMCNloH7':                    ['nom'],
 'ttlight_nominal':                     ['nom','syst'],
 'ttlight_aMCNloP8':                   ['nom'],
 'ttlight_PowhegH7':                   ['nom'],
 'ttlight_PowhegH7_1_3':               ['nom'],
 'ttlight_aMCNloH7':                   ['nom'],
 'ttbar_nominal':                     ['nom','syst'],
 'ttbar_aMCNloP8':                   ['nom'],
 'ttbar_PowhegH7':                   ['nom'],
 'ttbar_PowhegH7_1_3':               ['nom'],
 'ttbar_aMCNloH7':                   ['nom'],
}
def get_all_procs(generator):
    import itertools
    gen = GEN_IN_ARG_TO_GEN_NAME_MAP[generator]
    dsids = [v for k, v in PROC_DSID_MAP.items() if gen in k]
    dsids = list(set(itertools.chain.from_iterable(dsids)))
    PROC_DSID_MAP[f'all_{generator}'] = dsids
def get_proc_by_dsid(dsid):
    return [proc for proc, id_of_proc in PROC_DSID_MAP.items() if id_of_proc == dsid and 'ttbar' not in proc]
# # A map from simple generator name to suffix in proc name
# GEN_IN_ARG_TO_GEN_NAME_MAP = {
#     'nominal': '_nominal',
#     'PH7old': '_PowhegH7_0_4',
#     'PH7new': '_PowhegH7_1_3',
#     'aMCP8':  '_aMCNloP8',
#     'aMCH7new':  '_aMCNloH7_1_3',
#     'sherpa':  '_sherpa228',
# }
# (V34) A map from simple generator name to suffix in proc name
GEN_IN_ARG_TO_GEN_NAME_MAP = {
    'nominal': '_nominal',
    'PH7old': '_PowhegH7_0_4',
    'PH7new': '_PowhegH7_1_3',
    'PH7': '_PowhegH7',
    'aMCP8':  '_aMCNloP8',
    'aMCH7new':  '_aMCNloH7_1_3',
    'aMCH7':  '_aMCNloH7',
    'sherpa':  '_sherpa228',
}
# # A map from a process to available generators
# PROC_TO_AVAIL_GENS_MAP = {
#     'singletop_Wtchannel': ['nominal', 'PH7old', 'aMCP8'],
#     'singletop_tchannel': ['nominal', 'PH7old', 'aMCP8'],
#     'singletop_schannel': ['nominal', 'PH7old', 'aMCP8'],
#     'Wjets':  ['nominal'],
#     'Zjets':  ['nominal'],
#     'VV':   ['nominal'],
#     'ttW':  ['nominal', 'sherpa'],
#     'ttZ':  ['nominal', 'sherpa'],
#     'ttH':  ['nominal', 'PH7old', 'aMCP8'],
#     'ttb':  ['nominal', 'PH7old', 'PH7new', 'aMCP8', 'aMCH7new'],
#     'ttc':  ['nominal', 'PH7old', 'PH7new', 'aMCP8', 'aMCH7new'],
#     'ttlight':  ['nominal', 'PH7old', 'PH7new', 'aMCP8', 'aMCH7new'],
#     'ttbar':  ['nominal', 'PH7old', 'PH7new', 'aMCP8', 'aMCH7new'],
# }
# (V34) A map from a process to available generators
PROC_TO_AVAIL_GENS_MAP = {
    'singletop_Wtchannel': ['nominal', 'PH7', 'aMCP8'],
    'singletop_tchannel': ['nominal', 'PH7', 'aMCP8','aMCH7'],
    'singletop_schannel': ['nominal', 'PH7', 'aMCP8','aMCH7'],
    'Wjets':  ['nominal'],
    'Zjets':  ['nominal'],
    'VV':   ['nominal'],
    'ttW':  ['nominal', 'sherpa'],
    'ttZ':  ['nominal', 'sherpa'],
    'ttH':  ['nominal', 'PH7old', 'aMCP8'],
    'ttb':  ['nominal', 'PH7', 'PH7new', 'aMCP8', 'aMCH7'],
    'ttc':  ['nominal', 'PH7', 'PH7new', 'aMCP8', 'aMCH7'],
    'ttlight':  ['nominal', 'PH7', 'PH7new', 'aMCP8', 'aMCH7'],
    'ttbar':  ['nominal', 'PH7', 'PH7new', 'aMCP8', 'aMCH7'],
}
# # A map from a process to available simulation type (FS or AFII)
# PROC_TO_AVAIL_SIM_MAP = {
#      'singletop_Wtchannel_nominal':      ['FS', 'AFII'],
#      'singletop_Wtchannel_aMCNloP8':     ['AFII'],
#      'singletop_Wtchannel_PowhegH7_0_4': ['AFII'],
#      'singletop_tchannel_nominal':       ['FS', 'AFII'],
#      'singletop_tchannel_aMCNloP8':      ['AFII'],
#      'singletop_tchannel_PowhegH7_0_4':  ['AFII'],
#      'singletop_schannel_nominal':       ['FS', 'AFII'],
#      'singletop_schannel_aMCNloP8':      ['AFII'],
#      'singletop_schannel_PowhegH7_0_4':  ['AFII'],
#      'Wjets_nominal':                    ['FS'],
#      'Zjets_nominal':                    ['FS'],
#      'VV_nominal':                       ['FS'],
#      'ttW_nominal':                      ['FS'],
#      'ttW_sherpa228':                    ['FS'],
#      'ttZ_nominal':                      ['FS'],
#      'ttZ_sherpa228':                    ['FS'],
#      'ttH_nominal':                      ['FS', 'AFII'],
#      'ttH_aMCNloP8':                     ['AFII'],
#      'ttH_PowhegH7_0_4':                 ['AFII'],
#      'ttb_nominal':                      ['FS', 'AFII'],
#      'ttb_aMCNloP8':                     ['AFII'],
#      'ttb_PowhegH7_0_4':                 ['AFII'],
#      'ttb_PowhegH7_1_3':                 ['AFII'],
#      'ttb_aMCNloH7_1_3':                 ['AFII'],
#      'ttc_nominal':                      ['FS', 'AFII'],
#      'ttc_aMCNloP8':                     ['AFII'],
#      'ttc_PowhegH7_0_4':                 ['AFII'],
#      'ttc_PowhegH7_1_3':                 ['AFII'],
#      'ttc_aMCNloH7_1_3':                 ['AFII'],
#      'ttlight_nominal':                  ['FS', 'AFII'],
#      'ttlight_aMCNloP8':                 ['AFII'],
#      'ttlight_PowhegH7_0_4':             ['AFII'],
#      'ttlight_PowhegH7_1_3':             ['AFII'],
#      'ttlight_aMCNloH7_1_3':             ['AFII'],
#      'ttbar_nominal':                    ['FS', 'AFII'],
#      'ttbar_aMCNloP8':                   ['AFII'],
#      'ttbar_PowhegH7_0_4':               ['AFII'],
#      'ttbar_PowhegH7_1_3':               ['AFII'],
#      'ttbar_aMCNloH7_1_3':               ['AFII'],
# }
PROC_TO_AVAIL_SIM_MAP = {
 'singletop_Wtchannel_nominal':      ['FS', 'AFII'],
 'singletop_Wtchannel_aMCNloP8':     ['AFII'],
 'singletop_Wtchannel_PowhegH7':     ['AFII'],
 'singletop_tchannel_nominal':       ['FS', 'AFII'],
 'singletop_tchannel_aMCNloP8':      ['AFII'],
 'singletop_tchannel_PowhegH7':      ['AFII'],
 'singletop_tchannel_aMCNloH7':      ['AFII'],
 'singletop_schannel_nominal':       ['FS', 'AFII'],
 'singletop_schannel_aMCNloP8':      ['AFII'],
 'singletop_schannel_PowhegH7':      ['AFII'],
 'singletop_tchannel_aMCNloH7':      ['AFII'],
     'Wjets_nominal':                    ['FS'],
     'Zjets_nominal':                    ['FS'],
     'VV_nominal':                       ['FS'],
     'ttW_nominal':                      ['FS'],
     'ttW_sherpa228':                    ['FS'],
     'ttZ_nominal':                      ['FS'],
     'ttZ_sherpa228':                    ['FS'],
     'ttH_nominal':                      ['FS', 'AFII'],
     'ttH_aMCNloP8':                     ['AFII'],
     'ttH_PowhegH7_0_4':                 ['AFII'],
    'ttb_nominal':                       ['FS', 'AFII'],
    'ttb_aMCNloP8':                      ['AFII'],
    'ttb_PowhegH7':                      ['FS'],
    'ttb_PowhegH7_1_3':                  ['AFII'],
    'ttb_aMCNloH7':                      ['AFII'],
    'ttc_nominal':                       ['FS', 'AFII'],
    'ttc_aMCNloP8':                      ['AFII'],
    'ttc_PowhegH7':                      ['FS'],
    'ttc_PowhegH7_1_3':                  ['AFII'],
    'ttc_aMCNloH7':                      ['AFII'],
    'ttlight_nominal':                       ['FS', 'AFII'],
    'ttlight_aMCNloP8':                      ['AFII'],
    'ttlight_PowhegH7':                      ['FS'],
    'ttlight_PowhegH7_1_3':                  ['AFII'],
    'ttlight_aMCNloH7':                      ['AFII'],
    'ttbar_nominal':                       ['FS', 'AFII'],
    'ttbar_aMCNloP8':                      ['AFII'],
    'ttbar_PowhegH7':                      ['FS'],
    'ttbar_PowhegH7_1_3':                  ['AFII'],
    'ttbar_aMCNloH7':                      ['AFII'],
}

# Given a process, generator and simulation type, get the DSIDs
def get_proc_dsid(proc, which_gen='nominal', which_sim='FS'):
    proc_surname = GEN_IN_ARG_TO_GEN_NAME_MAP[which_gen]
    proc_full_name = proc+proc_surname
    dsids = PROC_DSID_MAP[proc_full_name]
    if(which_sim == 'AFII'):
        # Add _AFII to DSID if AFII is not the only option
        dsids = [dsid+'_AFII' if '_AFII' not in dsid else dsid for dsid in dsids]
    return dsids


# ==================================================================
# An argument type to parse args of the form (proc, (optional)gen,(optional)FS/AFII)
# User can feed A)proc_name B)proc_name,gen C)proc_name,FS/AFII D)proc_name,gen,FS/AFII
# ==================================================================
def process_type(arg):

    # If more than one detail about proc is given (i.e. user fed B,C or D)
    if "," in arg:
        # Separate the proc details into assuming ',' separation
        proc_details = [s.strip() for s in arg.split(",")]

        # ======================
        # If proc_name+gen or proc_name+FS/AFII given
        # ======================
        if(len(proc_details) == 2):
            # proc_name always first
            proc_name = proc_details[0]
            # either a gen or simulation type given
            gen_or_sim = proc_details[1]
            # ======================
            # Check input is valid
            # ======================
            if(proc_name not in PROC_TO_AVAIL_GENS_MAP.keys()):
                raise ArgumentTypeError(f"The sample {proc_name} is not recognised..\n"
                                        + f"Choose a sample from {PROC_TO_AVAIL_GENS_MAP.keys()}")

            elif(gen_or_sim not in GEN_IN_ARG_TO_GEN_NAME_MAP.keys() and gen_or_sim not in ['FS', 'AFII']):
                raise ArgumentTypeError(f"The second argument {gen_or_sim} is not recognised..\n"
                                        + f"Choose either a generator from {GEN_IN_ARG_TO_GEN_NAME_MAP.keys()}"
                                        + f" or a simulation type from [FS, AFII]")
            # ======================
            # Check if 2nd arg is gen or FS/AFII
            # ======================
            # If a generator is given
            elif(gen_or_sim in GEN_IN_ARG_TO_GEN_NAME_MAP.keys()):

                # Check it is available for the sample requested
                avail_gens_for_proc = PROC_TO_AVAIL_GENS_MAP[proc_name]
                if gen_or_sim not in avail_gens_for_proc:
                    raise ArgumentTypeError(f"The chosen generator is not available for the sample {proc_name} \n"
                                            + f"Choose a generator from {avail_gens_for_proc}")
                else:
                    # If gen requested is available for the proc_name
                    # Define it as the gen to return by parser
                    gen = gen_or_sim
                    # For the proc+gen combo, choose FS/AFII
                    gen_name = GEN_IN_ARG_TO_GEN_NAME_MAP[gen]
                    # If both FS/AFII available, FS will be used
                    sim = PROC_TO_AVAIL_SIM_MAP[proc_name+gen_name][0]
                    # Confirm for the user that the arg is parsed
                    print(f'INFO:: The script will use the {sim} simulation for {proc_name+gen_name} sample')

            # If FS/AFII given
            elif(gen_or_sim in ['FS', 'AFII']):
                # Parser return the nominal generator
                gen = 'nominal'
                # Check simulation is available for the sample requested
                gen_name = GEN_IN_ARG_TO_GEN_NAME_MAP[gen]
                avail_sim_for_proc = PROC_TO_AVAIL_SIM_MAP[proc_name+gen_name]
                if gen_or_sim not in avail_sim_for_proc:
                    raise ArgumentTypeError(f"The chosen simulation is not available for the sample {proc_name} \n"
                                            + f"Choose a simulation from {avail_sim_for_proc}")
                else:
                    # If requested simulaiton type available for the proc+nominal combo
                    # define ut as the sim type to return by parser
                    sim = gen_or_sim
                    # Confirm for the user that the arg is parsed
                    print(f'INFO:: The script will use the {sim} simulation for {proc_name+gen_name} sample')

        # ======================
        # If Proc+Gen+FS/AFII
        # ======================
        elif(len(proc_details) == 3):
            # proc_name is alwyas first
            proc_name = proc_details[0]
            # Generator is 2nd
            gen = proc_details[1]
            # FS/AFII is 3rd
            sim = proc_details[2]
            # ======================
            # Check input is valid
            # ======================
            if(proc_name not in PROC_TO_AVAIL_GENS_MAP.keys()):
                raise ArgumentTypeError(f"The first argument {proc_name} is not recognised..\n"
                                        + f"Choose a sample from {PROC_TO_AVAIL_GENS_MAP.keys()}")

            if(gen not in GEN_IN_ARG_TO_GEN_NAME_MAP.keys()):
                raise ArgumentTypeError(f"The second argument {gen_or_sim} is not recognised..\n"
                                        + f"Choose either a generator from {GEN_IN_ARG_TO_GEN_NAME_MAP.keys()}")

            if(sim not in ['FS', 'AFII']):
                raise ArgumentTypeError(f"The third argument {gen_or_sim} is not recognised..\n"
                                        + f"Choose either a generator from [FS,AFII]")
            # ======================
            # Check all 3 args work together
            # ======================
            if(gen not in PROC_TO_AVAIL_GENS_MAP[proc_name]):
                raise ArgumentTypeError(f"The chosen generator is not available for the sample {proc_name} \n"
                                        + f"Choose a generator from {PROC_TO_AVAIL_GENS_MAP[proc_name]}")

            gen_name = GEN_IN_ARG_TO_GEN_NAME_MAP[gen]
            if(sim not in PROC_TO_AVAIL_SIM_MAP[proc_name+gen_name]):
                raise ArgumentTypeError(f"The chosen simulation is not available for the sample {proc_name} \n"
                                        + f"Choose a simulation from {PROC_TO_AVAIL_SIM_MAP[proc_name+gen_name]}")
            # Confirm for the user that the arg is parsed
            print(f'INFO:: The script will use the {sim} simulation for {proc_name+gen_name} sample')
        # ======================
        # If >3 args are comma-separared, raise parsing error
        # ======================
        elif(len(proc_details) > 3):
            raise ArgumentTypeError(f"Too many process details in {arg}. "
                                    + f" Expected maximum 3 but got {len(proc_details)}..")
    # If only one detail about proc is given (i.e. user fed A)
    else:
        # Only the process name is given
        proc_name = arg
        # ======================
        # Check input is valid
        # ======================
        if(proc_name not in PROC_TO_AVAIL_GENS_MAP.keys()):
            raise ArgumentTypeError(f"The sample {proc_name} is not recognised..\n"
                                    + f"Choose a sample from {PROC_TO_AVAIL_GENS_MAP.keys()}")
        else:
            # The process name is valid
            # parser returns nominal generator
            gen = 'nominal'
            # parser returns avialable FS/AFII (FS if both available)
            gen_name = GEN_IN_ARG_TO_GEN_NAME_MAP[gen]
            sim = PROC_TO_AVAIL_SIM_MAP[proc_name+gen_name][0]
            # Confirm for the user that the arg is parsed
            print(f'INFO:: The script will use the {sim} simulation for {proc_name+gen_name} sample')       

    # Combine the parsed details into a tuple
    proc_details_tuple = (proc_name, gen, sim)

    return proc_details_tuple