GEN_TO_COLOR_MAP = {
    'nominal': 'black',
    'PH7': 'darkred',
    'PH7new': 'salmon',
    'aMCP8':  'darkgreen',
    'aMCH7':  'springgreen',
    'sherpa':  'orange',
}
GEN_SIM_TO_COLOR_MAP = {
    'nominal_FS': 'black',
    'nominal_AFII': 'grey',
    'PH7_FS': 'darkred',
    'PH7new_AFII': 'salmon',
    'aMCP8_AFII':  'darkgreen',
    'aMCH7_AFII':  'springgreen',
    'sherpa_AFII':  'orange',
}

GEN_TO_PRETTY_NAME = {
    'nominal': 'Nominal (FS)',
    'nominal': 'Nominal (AFII)',
    'PH7old': 'PowHeg + Herwig 7.0.4 (AFII)',
    'PH7': 'PowHeg + Herwig 7.?.? (FS)',
    'PH7new': 'PowHeg + Herwig 7.1.3 (AFII)',
    'aMCP8':  'aMC@NLO + Pythia8 (AFII)',
    'aMCH7':  'aMC@NLO + Herwig 7.1.3 (AFII)',
    'sherpa':  'Sherpa 2.2.8 (AFII)',
}
GEN_SIM_TO_PRETTY_NAME = {
    'nominal_FS': 'Nominal (FS)',
    'nominal_AFII': 'Nominal (AFII)',
    'PH7old_AFII': 'PowHeg + Herwig 7.0.4 (AFII)',
    'PH7_FS': 'PowHeg + Herwig 7.?.? (FS)',
    'PH7new_AFII': 'PowHeg + Herwig 7.1.3 (AFII)',
    'aMCP8_AFII':  'aMC@NLO + Pythia8 (AFII)',
    'aMCH7_AFII':  'aMC@NLO + Herwig 7.1.3 (AFII)',
    'sherpa_AFII':  'Sherpa 2.2.8 (AFII)',
}
GEN_SIM_TO_SHORT_NAME = {
    'nominal_FS': 'nominal(FS)',
    'nominal_AFII': 'nominal(AFII)',
    'PH7old_AFII': 'PH7.0.4(AFII)',
    'PH7_FS':       'PH7.X.X(FS)',
    'PH7new_AFII': 'PH7.1.3(AFII)',
    'aMCP8_AFII':  'aMCP8(AFII)',
    'aMCH7_AFII':  'aMCH7.1.3(AFII)',
    'sherpa_AFII':  'Sherpa2.2.8(AFII)',
}

VAR_TO_PRETTY_NAME = {
    'bdt_tH':                'BDT(tH)',
    'bdt_ttb':               r'BDT($t\bar{t} + \geq 1b$)',
    'bdt_ttc':                r'BDT($t\bar{t} + \geq 1c$)',
    'bdt_ttlight':            r'BDT($t\bar{t} + \geq 0$ light)',
    'bdt_others':            'BDT(Other Backgrounds)',
    'weight_mc':             'MC Weight',
    'jet0_pt':               r'$pT_{jet0}$',
    'HF_Classification':     'HF Classification',
}
PROC_TO_PRETTY_NAME = {
    'singletopWtchannel': r'single-top + $W$',
    'singletoptchannel':  r'single-top $t$-channel',
    'singletopschannel':  r'single-top $s$-channel',
    'Wjets':               r'$W+jets$',
    'Zjets':               r'$Z+jets$',
    'VV':                  r'Diboson',
    'ttW':                 r'$t\bar{t}+W$',
    'ttZ':                 r'$t\bar{t}+Z$',
    'ttH':                 r'$t\bar{t}+H$',
    'ttb':                 r'$t\bar{t}+\geq 1b$',
    'ttc':                 r'$t\bar{t}+\geq 1c$',
    'ttlight':             r'$t\bar{t}+\geq 0$ light',
    'ttbar':             r'$t\bar{t} + jets$',
}