'''
Author: Mohamed Aly
Email: maly@cern.ch
Description:

This script is responsible for slimming Ntuples used by the tH(bb)q analysis. The user specifies:
  - The path to the Ntuples
  - The path where slimmed dataset will be saved
  - The Variables to keep from the Ntuples (Default BDT scores)
  - The MC Campaigns to loop over (Default a+d+e)
  - The samples to study. This is given as a tuple of the form:
  		(process_name, process_generator, AFII or FS)

The script determines the fastest way to slim the files for each sample; If all files combined fit
in 4 GB memory, uproot.conctenate is used to read them all in one go. Else, uproot.iterate() is used
to read the files in chunks of 4 GB.  A verbose option is available to follow what the code does.

Each variable chosen will be saved as a dataset in H5 files. The number of files will depend on the
uproot method chosen by the script.
'''

import uproot4 as uproot
from argparse import ArgumentParser, ArgumentTypeError
import pandas as pd
import sys
import os
import glob
import numpy as np
import h5py
import time
import awkward as ak
from termcolor import cprint
from pathlib import Path
from tools.common import dhelp, REGION_TO_REGION_NAME_MAP
from tools.input_studies_common import (PROC_TO_AVAIL_GENS_MAP, PROC_TO_AVAIL_SIM_MAP,
                                        GEN_IN_ARG_TO_GEN_NAME_MAP, get_proc_dsid,
                                        process_type, PROC_CAMPAIGN_MAP)
from tools.var_getters import VAR_TO_BRANCH_NAME_MAP
from tools.selectors import (get_sample_req, num_obj_req)


# ==================================================================
# Get arguments from CL
# ==================================================================
# Help messages
# ================
_OUTDIR_HELP = dhelp('Path to directory where output files will be stored ')
_INPUT_HELP = dhelp('The directory where the NTuples are stored')
_PROCESSES_HELP = dhelp('The samples to check in form: proc_name,(optional)gen,(optional)FS/AFII')
_CAMPAIGNS_HELP = dhelp('The campaigns to check')
_VARS_HELP = dhelp('The branches to load from the Ntuples')
_DATA_HELP = dhelp('Slim data')
# ================
# Defaults
# ================

DEFAULT_IN_DIR = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/tH_v34_minintuples_v0/'
DEFAULT_OUT_DIR = '/eos/user/m/maly/thbb/analysis_files/slimmed_Ntuples/tmp_06_08_21'
DEFAULT_PROC = [('ttb', 'PH7new', 'AFII')]
DEFAULT_CAMPAIGNS = ['mc16a', 'mc16d','mc16e']
DEFAULT_VARS = ['bdt_tH', 'bdt_ttb', 'bdt_ttc', 'bdt_ttlight', 'bdt_others']


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('--indir', default=DEFAULT_IN_DIR, help=_INPUT_HELP)
    parser.add_argument('--outdir', type=Path, default=DEFAULT_OUT_DIR, help=_OUTDIR_HELP)
    parser.add_argument('--campaigns', nargs="+", default=DEFAULT_CAMPAIGNS, help=_OUTDIR_HELP)
    parser.add_argument('--processes', nargs="+", type=process_type, default=DEFAULT_PROC, help=_PROCESSES_HELP)
    parser.add_argument('--variables', nargs="+", choices=VAR_TO_BRANCH_NAME_MAP.keys(), default=DEFAULT_VARS, help=_VARS_HELP)
    parser.add_argument('--skimmed_ntuples', action='store_true')
    parser.add_argument('--tree', default='nominal_Loose')
    parser.add_argument('--data', action='store_true', help=_DATA_HELP)
    parser.add_argument('--verbose', action='store_true')
    return parser.parse_args()


CAMPAIGN_TO_RTAG = {
	'mc16a': 'r9364',
	'mc16d': 'r10201',
	'mc16e': 'r10724',
}

# ================
# Main
# ================
def run():
	# ======================
	# Prepare args
	# ======================
	args = get_args()
	skimmed_ntuples = args.skimmed_ntuples
	# Be Verbose ?
	verbose = args.verbose
	# Ntuples Directory
	in_dir = args.indir
	# Slimmed files directory
	out_dir = args.outdir
	out_dir.mkdir(parents=True, exist_ok=True)
	# The samples to process
	procs_details = args.processes
	# The variables to keep
	variables = args.variables
	# The Tree to get branches from
	tree = args.tree
	# The campaigns to process
	nude_campaigns = args.campaigns
	if len(nude_campaigns) == 1 and nude_campaigns[0] == 'none':
		# If 'none' given, data is assumed to be given
		nude_campaigns = []
	# Data will be studied?
	study_data = args.data
	# Determine if both Data and MC will be processed, or either of the two
	data_only, data_and_mc, mc_only = False, False, False
	if study_data and len(nude_campaigns) == 0:
		data_only = True
		if verbose:
			cprint(f'INFO:: The script will be reading DATA files only', 'green')
	elif study_data and len(nude_campaigns) != 0:
		data_and_mc = True
		if verbose:
			cprint(f'INFO:: The script will be reading DATA and MC files', 'green')
	elif not study_data and len(nude_campaigns) != 0:
		mc_only = True
		if verbose:
			cprint(f'INFO:: The script will be reading MC files only', 'green')
	else:
		cprint("ERROR:: Neither Data or MC campaigns were specified", 'red')
		exit(0)
	if verbose:
		cprint(f'INFO:: The samples to check are {procs_details}', 'green')

	# Start a loop over processes requested
	for one_proc_details in procs_details:
		# ======================
		# Prepare some diagnostic info for benchmarking
		# ======================
		tot_disk_space_h5 = 0  # The amount of space consumed by slimmed down files
		tot_disk_space_ntuples = 0  # The amount of space consumed by the original Ntuples
		# Read the proc_name, generator and sim type from the parsed tuples
		proc_name = one_proc_details[0]
		proc_gen = one_proc_details[1]
		proc_sim = one_proc_details[2]
		# Get the DSIDs for the requested process details
		dsids = get_proc_dsid(proc_name, which_gen=proc_gen, which_sim=proc_sim)
		# Get the cuts which define the sample (mainly relevant for ttbar)
		sample_req = get_sample_req(proc_name)
		# Get the campaign directory name.
		proc_surname = GEN_IN_ARG_TO_GEN_NAME_MAP[proc_gen]
		proc_full_name = proc_name+proc_surname
		campaigns = get_correct_campaign_dir(nude_campaigns, proc_sim, proc_full_name, skimmed_ntuples)
		# The variables to be included in the slimmed files
		og_branches = [VAR_TO_BRANCH_NAME_MAP[var] for var in variables]
		clubcard_branches = ['foam[:,1]', 'njets', 'nbjets', 'leptons_tight[:,0]','leptons_PLIVtight[:,0]']
		mc_weight_branches = ['weight_mc*xsec_weight*weight_pileup*weight_bTagSF_DL1r_Continuous*weight_jvt*weight_forwardjvt/totalEventsWeighted*weight_leptonSF', 'runNumber']
		
		# Determine the branches and files that must be kept from Ntuples depending on whether MC or Data
		if data_only:
			# Keep the requested branches and obligatory ones for Phase-space selection (no weights)
			branches = og_branches+clubcard_branches
			files = [in_dir+'data_nom/*']
		elif data_and_mc:
			# Need two sets of branches and two sets of files
			data_branches = og_branches+clubcard_branches
			data_files = in_dir+'data_nom/*'
			mc_branches = og_branches+clubcard_branches+mc_weight_branches
			if skimmed_ntuples:
				dsids = [dsid.replace("_AFII","") for dsid in dsids]
				mc_files = [in_dir+"*"+dsid+"*"+campaign+'/*' for campaign in campaigns for dsid in dsids]
				mc_weight_branches = [br.replace("xsec_weight*","") for br in mc_weight_branches]
				mc_weight_branches = [br.replace("totalEventsWeighted*","") for br in mc_weight_branches]
				mc_branches = og_branches+clubcard_branches+mc_weight_branches
			else:
				if proc_sim != 'AFII':
					mc_files = [in_dir+campaign+dsid+"_user*" for campaign in campaigns for dsid in dsids]
				else:
					mc_files = [in_dir+"*"+dsid+"*"+campaign+'/*' for campaign in campaigns for dsid in dsids]
				mc_branches = og_branches+clubcard_branches+mc_weight_branches
		elif mc_only:
			# Need requested branches, obligaotry branches and weight branches
			branches = og_branches+clubcard_branches+mc_weight_branches
			if skimmed_ntuples:
				dsids = [dsid.replace("_AFII","") for dsid in dsids]
				files = [in_dir+"*"+dsid+"*"+campaign+'/*' for campaign in campaigns for dsid in dsids]
				mc_weight_branches = [br.replace("xsec_weight*","") for br in mc_weight_branches]
				mc_weight_branches = [br.replace("totalEventsWeighted*","") for br in mc_weight_branches]
				branches = og_branches+mc_weight_branches
			else:
				if proc_sim != 'AFII':
					files = [in_dir+campaign+dsid+"_user*" for campaign in campaigns for dsid in dsids]
				else:
					files = [in_dir+campaign+dsid+"*" for campaign in campaigns for dsid in dsids]

		# If only data or MC are being studied, there is one set of files and one set of branches to read
		if data_only or mc_only:
			slim_t1 = time.time()  # Time the slimming starts
			data_uproot, unslimmed_disk_space = read_with_uproot(files, branches, sample_req, tree, verbose)
			# Get totalEventsWeighted and xsec_weight
			totalEventsWeighted = -1
			if skimmed_ntuples:
				totalEventsWeighted_data, _ = read_with_uproot(files, 'totalEventsWeighted', 'totalEvents>0', 'sumWeights', verbose)
				for campaign_data in totalEventsWeighted_data:
					# If chunks from uproot.iterate
					if isinstance(campaign_data, list):
						for datum in campaign_data:
							totalEventsWeighted_df = ak.to_pandas(datum) # Turn data into pandas dataframe
							totalEventsWeighted_df.reset_index(inplace=True)
							totalEventsWeighted_float = totalEventsWeighted_df['totalEventsWeighted'].sum()
							totalEventsWeighted+=totalEventsWeighted_float
					else:
						totalEventsWeighted_df = ak.to_pandas(campaign_data) # Turn data into pandas dataframe
						totalEventsWeighted_df.reset_index(inplace=True)
						totalEventsWeighted_float = totalEventsWeighted_df['totalEventsWeighted'].sum()
						totalEventsWeighted+=totalEventsWeighted_float
				
			cprint(f"INFO:: Total Events Weighted: {totalEventsWeighted}", 'green')
			
			# ======================
			# Save Slimmed Dataset
			# =====================
			# Prepare the output slimmed dataset file name
			slimmed_file_name = f'{proc_name}_{proc_gen}_{proc_sim}.h5'
			out_path = f'{out_dir}/{slimmed_file_name}'
			# Save slimmed datasets into H5 files
			tot_disk_space_h5 = clean_up_and_dump(data_uproot, True, out_path, og_branches, variables, tree, verbose, totalEventsWeighted)
			# Time when slimming for particlular dataset is done
			slim_t2 = time.time()
			# Print Diagnostics
			if verbose:
				cprint(f'Time Elapsed in Slimming: {slim_t2-slim_t1}', 'green')
				cprint(f'Disk Space for Slimmed H5 Files: {tot_disk_space_h5*1e-9} GB', 'green')
				cprint(f'Disk Space for Input Ntuples: {unslimmed_disk_space*1e-9} GB', 'green')

		# If both data and MC are being studied.
		if data_and_mc:
			slim_t1 = time.time()
			#==============
			# Prepare for Output
			#===============
			# Prepare the output slimmed dataset file name
			slimmed_file_name = f'{proc_name}_{proc_gen}_{proc_sim}.h5'
			out_path = f'{out_dir}/{slimmed_file_name}'
			#==============
			# Process DATA
			#===============
			data_uproot, unslimmed_disk_space = read_with_uproot(data_files, data_branches, sample_req, tree, verbose)
			# Save slimmed datasets into H5 files
			tot_disk_space_h5 = clean_up_and_dump(data_uproot, False, out_path, data_branches, variables, tree, verbose)
			#==============
			# Process MC
			#===============
			data_uproot, unslimmed_disk_space = read_with_uproot(mc_files, mc_branches, sample_req, tree, verbose)
			# Save slimmed datasets into H5 files
			tot_disk_space_h5 = clean_up_and_dump(data_uproot, True, out_path, og_branches, variables, tree, verbose)
			# Time when slimming for particlular dataset is done
			slim_t2 = time.time()
			# Print Diagnostics
			if verbose:
				cprint(f'Time Elapsed in Slimming: {slim_t2-slim_t1}', 'green')
				cprint(f'Disk Space for Slimmed H5 Files: {tot_disk_space_h5*1e-9} GB', 'green')
				cprint(f'Disk Space for Input Ntuples: {unslimmed_disk_space*1e-9} GB', 'green')


def read_with_uproot(files: list , branches: list, cuts: str, tree: str, verbose: bool)-> list:
	'''
	Function to loop through file paths, check if files in the paths are not-empty
	and if not, opens them with uproot concant/iterate depending on the input size
	Args:
        files: A list of file paths, with elements differing in MC campaign (size 1 for data)
		branches: A list of branch names to retrieve from input
		cuts: A string with the cuts to apply on the sample (e.g. br1 > 2 && br2 > 3)
		tree: The name of the tree to retrieve branches from
		verbose: Verbosity 
    Returns:
       A list of awkward arrays if concat is used (one per campaign), or a list of list of
	   awk arrays if iterate is used (one per campaign per chunk)
	'''
	tot_unslimmed_disk_space = 0 # Input size
	counter = 0 # Chunk counter used in output file naming
	data_from_dirs = [] # Output list
	for files_dir in files:
		if verbose:
			cprint(f'INFO:: Slimming {files_dir}', 'green')
		# Turn path to glob in oreder to pass it to OS library functions
		root_files = glob.glob(files_dir)
		# Remove empty files
		new_files, unslimmed_disk_space = filter_out_empty_files(root_files, tree)
		# Skip empty directories 
		if(new_files is None):
			continue
		tot_unslimmed_disk_space+=unslimmed_disk_space
		# If the whole set of ntuples for this process can fit in memory (4GB), use uproot.concat
		if(unslimmed_disk_space*1e-9 < 4.):
			if verbose:
				cprint(f'INFO:: Combined files {files_dir} can fit in 4GB memory... using uproot.concatenate', 'green')
			try:
				data_uproot = uproot.concatenate(new_files, branches, cut=cuts)
				data_from_dirs.append(data_uproot)
			except uproot.KeyInFileError:
				cprint(f'ERROR:: The Tree you asked for {tree} is not available -- make sure it is in all files', 'red')
				exit(0)
			
		# If size of Ntuples is too big, read it in chunks that fit in memory
		else:
			if verbose:
				cprint(f'INFO:: Combined files {files_dir} cannot fit in 4GB memory... using uproot.iterate', 'green')
			all_data = []
			try:
				# Iterate over all the files in chunks of max 4 GBs (Memory Size)
				for data in uproot.iterate(new_files, branches, cut=cuts, step_size='4 GB'):				
					all_data.append(data)
				data_from_dirs.append(all_data)
			
			except uproot.KeyInFileError:
				raise  uproot.KeyInFileError
				cprint(f'ERROR:: The Tree you asked for {tree} is not available -- make sure it is in all files', 'red')
				exit(0)
			
	return data_from_dirs, tot_unslimmed_disk_space


def filter_out_empty_files(files: list, tree: str)->(list, float):
	'''
	Depending on file sizes, create a new list of files which are of appropriate size
	Args:
        files: A list of absolute file paths from one directory (unpacked from wildcard filepath with glob)
		tree: The name of the tree to retrieve branches from
	Returns:
		new_files: An updated list of file paths after removing empty files paths
		tot_disk_space_ntuples: Total occupied space by input NTuples in Bytes
	'''
	new_files = [] # Output list of files
	tot_disk_space_ntuples = 0 # Total space occupied by input
	files_dir = os.path.dirname(files[0]) # The directory where the files are
	# Alert user if no files were found in the directory 
	if len(files) < 1:
		cprint(f'WARNING:: There are no files in {files_dir} with the specified DSID', 'yellow')
		return None, None
	for file in files:
		# Get the size in bytes of the Ntuples in the directory
		ntuples_size = os.path.getsize(file)
		# Get rid of files < 0.1 MBs
		if ntuples_size*1e-6 < 0.1 :
			cprint(f'WARNING:: Dropping {file} with size {ntuples_size*1e-6} MBs', 'yellow')
			continue 
		else:
			new_files.append(file)
		# Store the size of input Ntuples (before slimming)
		tot_disk_space_ntuples += ntuples_size
	# Alert user if all files from directory have been removed because of size
	if len(new_files) == 0 and files!=0:
		cprint(f'WARNING:: All files in {files_dir} are < 0.1 MB -- no files are processed from directory', 'yellow')
		return None, None 
	# Get Nominal Loose tree
	new_files= list(map(lambda ls: ls+f':{tree}', new_files))
	return new_files, tot_disk_space_ntuples
		

def clean_up_and_dump(data_uproot: list, mc: bool, out_path: str, og_branches: list, variables: list, tree: str, verbose: bool, totalEventsWeighted: float) -> float:
	'''
	Function which will rename the branches that the user asked for to their simple names that user
	interacts with, then update the weights column with runNumber information and rename the column
	to 'weights' and then write out the data to H5 file
	Args:
		data_uproot: list (or list of lists) of awkward arrays as outputted from :func:`~thbb_slimming.read_with_uproot`
		mc: bool which tells the code if the data to be dumped is MC (to apply weights)
		out_path; Path where the H5 file is to be stored, up to (and including) file naming
		og_branches: The branches that the user asked for in args
		variables: The simple names for the branches the user asked for in args
		tree: The name of the tree to retrieve branches from, which will be used in outfile name
		verbose: Verbosity
	Returns:
		tot_disk_space_h5: The total disk space occupied by the H5 files dumped
	'''
	counter = 0
	tot_disk_space_h5 = 0
	for campaign_data in data_uproot:
		# If chunks from uproot.iterate
		if isinstance(campaign_data, list):
			for datum in campaign_data:
				data = ak.to_pandas(datum) # Turn data into pandas dataframe
				# Rename the variables columns titles to match the simple naming system made for user
				for idx, branch in enumerate(og_branches):
					data.rename(columns={branch: variables[idx]}, inplace=True)
				# If MC is being studied, an expression for weight was in the branches
				# simplify the weights column name and use runNumber to get another weight factor
				if mc:	
					if totalEventsWeighted>0: # if skimmed_ntuples
						data = sort_out_weights(data, totalEventsWeighted)
					else:
						data = sort_out_weights(data, totalEventsWeighted)
				# Add a counter to file path before dumping to avoid overwriting 
				counter_path = out_path.replace(".h5",f"_{tree}_{str(counter)}.h5")
				if verbose:
					cprint(f'INFO:: Saving chunk {counter} of slimmed dataset to: ', 'green')
					cprint(f'	{counter_path} ', 'green')
				# Dump out the data in a file
				write_out(data, counter_path)
				counter+=1 # Increment counter for next chunk of data
				tot_disk_space_h5 += os.path.getsize(counter_path) # Add size of the H5 file to total
		else:
			data = ak.to_pandas(campaign_data) # Turn data into pandas dataframe
			# Rename the variables columns titles to match the simple naming system made for user
			for idx, branch in enumerate(og_branches):
				data.rename(columns={branch: variables[idx]}, inplace=True)
			# If MC is being studied, an expression for weight was in the branches
			# simplify the weights column name and use runNumber to get another weight factor
			if mc:	
				if totalEventsWeighted>0: # if skimmed_ntuples
					data = sort_out_weights(data, totalEventsWeighted)
				else:
					data = sort_out_weights(data, totalEventsWeighted)
			# Add a counter to file path before dumping to avoid overwriting
			counter_path = out_path.replace(".h5",f"_{tree}_{str(counter)}.h5")
			if verbose:
				cprint(f'INFO:: Saving chunk {counter} of slimmed dataset to: ', 'green')
				cprint(f'	{counter_path} ', 'green')
			# Dump out the data in a file
			write_out(data, counter_path)			
			counter += 1 # Increment counter for next campaign
			tot_disk_space_h5 += os.path.getsize(counter_path) # Add size of the H5 file to total
	return tot_disk_space_h5

def write_out(data: pd.DataFrame, out_path: str) -> None:
	'''
	Function to write out the data into an H5 file
	Args:
		data: A pandas dataframe with branches as columns
		out_path: The full path of the output file 
	Returns:
		None
	'''
	with h5py.File(out_path, "w") as file:
		# Save each variable data as a dataset in the H5 file
		for var, vals in data.items():
			file.create_dataset(var, data=np.array(vals))


def sort_out_weights(data: pd.DataFrame, totalEventsWeighted: float) -> pd.DataFrame:
	'''
	Apply weight factor due to runNumber, rename MC weight column and drop excessive weight columns
	Args:
		data: A pandas dataframe with branches as columns
	Returns:
		data: Updated datagrame with clean column name and extra factor applied to weights
	'''
	if totalEventsWeighted>0:
		data.rename(columns={'weight_mc*weight_pileup*weight_bTagSF_DL1r_Continuous*weight_jvt*weight_forwardjvt/weight_leptonSF': "weights"}, inplace=True)
		data['weights'] = data['weights']*1/totalEventsWeighted
	else:
		data.rename(columns={'weight_mc*xsec_weight*weight_pileup*weight_bTagSF_DL1r_Continuous*weight_jvt*weight_forwardjvt/totalEventsWeighted*weight_leptonSF': "weights"}, inplace=True)
	
	data['weights'] = np.where(data['runNumber'] < 290000.,
                               data['weights'] * 36207.66,
                               data['weights'])
	data['weights'] = np.where(data['runNumber'] >= 310000.,
                               data['weights'] * 58450.1,
                               data['weights'])
	data['weights'] = np.where(np.logical_and(data['runNumber'] < 310000., data['runNumber'] >= 290000.),
                               data['weights'] * 44307.4,
                               data['weights'])
	data.drop(columns=['runNumber'], inplace=True)
	return data

def get_correct_campaign_dir(nude_campaigns, sim_type, proc_full_name, skimmed_ntuples):
	'''
	Get the correct campaign directory containing the files to be slimmed down
	Args:
		nude_campaigns: The campaign names without any additions (e.g. mc16a, etc..)
		sim_type: The type of simulation being processed -- AFII/FS
		proc_full_name: The full name of the process (proc_generator_FS?AFII)
	'''

	avail_campaigns = PROC_CAMPAIGN_MAP[proc_full_name]
	if len(avail_campaigns) >=2:
		# Decide where to look for the files (nom-only or syst directories)
		if(sim_type == 'AFII'):
			if skimmed_ntuples:
				campaigns = ["a875*"+CAMPAIGN_TO_RTAG[campaign]+"*" for campaign in nude_campaigns]
			else:
				campaigns = [campaign+'_nom/' for campaign in nude_campaigns]
		else:
			if skimmed_ntuples:
				campaigns = ["s3126*"+CAMPAIGN_TO_RTAG[campaign]+"*" for campaign in nude_campaigns]			
			else:
				campaigns = [campaign+'_nom/' for campaign in nude_campaigns]
	else:
		# if sim_type!=avail_campaigns[0]:
		# 	print(f'ERROR: {sim_type} is not available for {proc_full_name} -- only {avail_campaigns[0]} is defined')
		# 	exit(0)
		if skimmed_ntuples:
			if avail_campaigns[0] == 'AFII':
				campaigns = ["a875*"+CAMPAIGN_TO_RTAG[campaign]+"*" for campaign in nude_campaigns]
			else:
				campaigns = ["s3126*"+CAMPAIGN_TO_RTAG[campaign]+"*" for campaign in nude_campaigns]
		else:
			campaigns = [campaign+f'_{avail_campaigns[0]}/' for campaign in nude_campaigns]
	return campaigns


if __name__ == '__main__':
    run()
