#include "ttbarAnalysis.h"
#include <string>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TStopwatch.h>
#include <TFile.h>
#include <TH1.h>

void runAnalysis(string NtuplesPath){   
string ChainPath, OutFile;
TChain * chain = new TChain("ttbar_mc16ALL_AFII");  
ChainPath = NtuplesPath + "/ttbar_mc16ALL_AFII.root";   
chain->Add(ChainPath.c_str());   
int NumEntries=chain->GetEntries();
OutFile = "Histos.root";
ttbarAnalysis *ttbarAna = new ttbarAnalysis(chain, OutFile.c_str());   

TStopwatch BigBen;         
//  Loop timer   
ttbarAna->loop(NumEntries);   
BigBen.Stop();       
BigBen.Print();   
delete chain;              
delete ttbarAna;

}