'''
Brief: 

Plot styling methods and maps 

'''
import numpy as np

def make_flattened_arr_getter(branch_name,inner_element=None):
	def get_flattened_arr(lazy_branches,branch_name,inner_element):
		if(inner_element is None):
			return np.flatten(lazy_branches[branch_name])
		else:
			try:
				return lazy_branches[branch_name][:,inner_element]
			else:
				print(f'The inner element requested from branch {branch_name} is bigger than smallest inner array size')
				print(f'Check you have enough objects in all events to ask for this element')
				exit(0)
			


VAR_GETTERS = {
	'n_tophad_jets_CBT0_ttAll': 'n_tophad_jets_CBT0_ttAll',
	'n_tophad_jets_CBT123_ttAll': 'n_tophad_jets_CBT123_ttAll',
	'n_tophad_jets_CBT4_ttAll': 'n_tophad_jets_CBT4_ttAll',
	'n_tophad_jets_CBT5_ttAll': 'n_tophad_jets_CBT5_ttAll',
	'n_nontophad_jets_CBT0_ttAll': 'n_nontophad_jets_CBT0_ttAll',
	'n_nontophad_jets_CBT123_ttAll':'n_nontophad_jets_CBT123_ttAll',
	'n_nontophad_jets_CBT4_ttAll': 'n_nontophad_jets_CBT4_ttAll',
	'n_nontophad_jets_CBT5_ttAll': 'n_nontophad_jets_CBT5_ttAll',
	'leptons_pt': make_flattened_arr_getter('leptons_pt'),
	'leptons_eta': make_flattened_arr_getter('leptons_eta'),
	'leptons_phi': make_flattened_arr_getter('leptons_phi'),
	'leptons_e': make_flattened_arr_getter('leptons_e'),
	'leptons_ID': make_flattened_arr_getter('leptons_ID'),
	'leptons_tight': make_flattened_arr_getter('leptons_tight'),
	'leptons_truthIFFClass': make_flattened_arr_getter('leptons_truthIFFClass'),
	'jets_pt': make_flattened_arr_getter('jets_pt'),
	'jets_pt0': make_flattened_arr_getter('jets_pt',inner_element=0),
	'jets_eta': make_flattened_arr_getter('jets_eta'),
	'jets_phi': make_flattened_arr_getter('jets_phi'),
	'jets_e': make_flattened_arr_getter('jets_e'),
	'jets_tagWeightBin_DL1r_Continuous': make_flattened_arr_getter('jets_tagWeightBin_DL1r_Continuous'),
	'bjets_pt': make_flattened_arr_getter('bjets_pt'),
	'bjets_eta': make_flattened_arr_getter('bjets_eta'),
	'bjets_phi': make_flattened_arr_getter('jets_eta'),
	'bjets_e': make_flattened_arr_getter('bjets_e'),
	'bjets_tagWeightBin_DL1r_Continuous': make_flattened_arr_getter('bjets_tagWeightBin_DL1r_Continuous'),
	'nonbjets_pt': make_flattened_arr_getter('nonbjets_pt'),
	'nonbjets_eta': make_flattened_arr_getter('nonbjets_eta'),
	'nonbjets_phi': make_flattened_arr_getter('nonbjets_phi'),
	'nonbjets_e': make_flattened_arr_getter('nonbjets_e'),
	'nonbjets_tagWeightBin_DL1r_Continuous': make_flattened_arr_getter('nonbjets_tagWeightBin_DL1r_Continuous'),
	'jets_truthPartonLabel': make_flattened_arr_getter('jets_truthPartonLabel'),
	'jets_truthflav': make_flattened_arr_getter('jets_truthflav'),
	'bjets_truthPartonLabel': make_flattened_arr_getter('bjets_truthPartonLabel'),
	'bjets_truthflav': make_flattened_arr_getter('bjets_truthflav'),
	'nonbjets_truthPartonLabel': make_flattened_arr_getter('nonbjets_truthPartonLabel'),
	'nonbjets_truthflav': make_flattened_arr_getter('nonbjets_truthflav'),
	'Ht': 'Ht',
	'njets': 'njets',
	'nalljets': 'nalljets',
	'nbjets': 'nbjets',
	'nnonbjets': 'nnonbjets',
	'njets_CBT0': 'njets_CBT0',
	'njets_CBT123': 'njets_CBT123',
	'njets_CBT4': 'njets_CBT4',
	'njets_CBT5': 'njets_CBT5',
	'met_pz': make_flattened_arr_getter('nbjets'),
	'alljet_m': 'alljet_m',
	'bjets_maxeta': 'bjets_maxeta',
	'foxWolfram_0_momentum': 'foxWolfram_0_momentum',
	'foxWolfram_1_momentum': 'foxWolfram_1_momentum',
	'foxWolfram_2_momentum': 'foxWolfram_2_momentum',
	'foxWolfram_3_momentum': 'foxWolfram_3_momentum',
	'foxWolfram_4_momentum': 'foxWolfram_4_momentum',
	'aplanarity': 'aplanarity',
	'sphericity': 'sphericity',
	'sphericity_t': 'sphericity_t',
	'W1_pt': 'W1_pt',
	'W1_eta': 'W1_eta',
	'W1_phi': 'W1_phi',
	'W1_m': 'W1_m',
	'W2_pt': 'W2_pt',
	'W2_eta': 'W2_eta',
	'W2_phi': 'W2_phi',
	'W2_m': 'W2_m',
	'top1_pt': 'top1_pt',
	'top1_eta': 'top1_eta',
	'top1_phi': 'top1_phi',
	'top1_y': 'top1_y',
	'top2_pt': 'top2_pt',
	'top2_eta': 'top2_eta',
	'top2_phi': 'top2_phi',
	'top2_m': 'top2_m',
	'top2_y': 'top2_y',
	'top_nohiggsbbreco_eta': 'top_nohiggsbbreco_eta',
	'top_nohiggsbbreco_phi': 'top_nohiggsbbreco_phi',
	'top_nohiggsbbreco_m': 'top_nohiggsbbreco_m',
	'top_nohiggsbbreco_y': 'top_nohiggsbbreco_y',
	'higgs_pt': 'higgs_pt',
	'higgs_eta': 'higgs_eta',
	'higgs_phi': 'higgs_phi',
	'higgs_m': 'higgs_m',
	'higgs_maxptbb_pt': 'higgs_maxptbb_pt',
	'higgs_maxptbb_eta': 'higgs_maxptbb_eta',
	'higgs_maxptbb_phi': 'higgs_maxptbb_phi',
	'higgs_maxptbb_m': 'higgs_maxptbb_m',
	'higgs_notoprecobb_pt': 'higgs_notoprecobb_pt',
	'higgs_notoprecobb_eta': 'higgs_notoprecobb_eta',
	'higgs_notoprecobb_phi': 'higgs_notoprecobb_phi',
	'higgs_notoprecobb_m': 'higgs_notoprecobb_m',
	'tophiggs_chi2': 'tophiggs_chi2',
	'higgs_phi': 'higgs_phi',
	'higgs_phi': 'higgs_phi',
	'higgs_phi': 'higgs_phi',
	'chi2_min_tophad_m_ttAll' : 'chi2_min_tophad_m_ttAll',  
	'chi2_min_tophad_pt_ttAll': 'chi2_min_tophad_pt_ttAll',   
	'chi2_min_tophad_eta_ttAll': 'chi2_min_tophad_eta_ttAll',   
	'chi2_min_tophad_phi_ttAll': 'chi2_min_tophad_phi_ttAll' , 
	'chi2_min_Whad_m_ttAll': 'chi2_min_Whad_m_ttAll',      
	'chi2_min_Whad_pt_ttAll': 'chi2_min_Whad_pt_ttAll',     
	'chi2_min_Whad_eta_ttAll': 'chi2_min_Whad_eta_ttAll',    
	'chi2_min_Whad_phi_ttAll': 'chi2_min_Whad_phi_ttAll',  	
 }

VAR_EDGES_MAP = {
	'n_tophad_jets_CBT0_ttAll': np.linspace(0,5,6),
	'n_tophad_jets_CBT123_ttAll': np.linspace(0,5,6),
	'n_tophad_jets_CBT4_ttAll': np.linspace(0,5,6),
	'n_tophad_jets_CBT5_ttAll': np.linspace(0,5,6),
	'n_nontophad_jets_CBT0_ttAll': np.linspace(0,5,6),
	'n_nontophad_jets_CBT123_ttAll': np.linspace(0,5,6),
	'n_nontophad_jets_CBT4_ttAll': np.linspace(0,5,6),
	'n_nontophad_jets_CBT5_ttAll': np.linspace(0,5,6),
 }

LOOKUPS_FOR_EDGES  = [
    ('n*jets*', np.linspace(0,5,6)),
    ('*_pt*', np.linspace(20,600,100) ),
    ('*_m*', np.linspace(20,600,100) ),
    ('*_eta*', np.linspace(-3,3,20) ),
    ('*_phi*', np.linspace(0,3.14,30) ),
    ('*jets*DL1r*', np.linspace(0,6,7) ),
    ('*Ht*', np.linspace(20,900,200) ),
]
 