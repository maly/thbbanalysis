
//////////////////////////////////////////////////////////
// This booking and filling histograms of NumJets 
//////////////////////////////////////////////////////////
#ifndef NUM_JETS_PLOTS_h
#define NUM_JETS_PLOTS_h

#include "ttbar_mc16ALL_AFII.h"
#include <string>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1.h>


class NumJetsPlots{
private:
	TH1F* n_tophadjets_CBT0_ttAll_histo;
	TH1F* n_tophadjets_CBT123_ttAll_histo;
	TH1F* n_tophadjets_CBT4_ttAll_histo;
	TH1F* n_tophadjets_CBT5_ttAll_histo;
	TH1F* n_nontophadjets_CBT0_ttAll_histo;
	TH1F* n_nontophadjets_CBT123_ttAll_histo;
	TH1F* n_nontophadjets_CBT4_ttAll_histo;
	TH1F* n_nontophadjets_CBT5_ttAll_histo;

public:
	NumJetsPlots(TObjArray* histos_list);
	void Fill(const double EventWt, const int n_tophad_jets_CBT0,  const int n_tophad_jets_CBT123, const int n_tophad_jets_CBT4,
				const int n_tophad_jets_CBT5, const int n_nontophad_jets_CBT0, const int n_nontophad_jets_CBT123, const int n_nontophad_jets_CBT4,
				const int n_nontophad_jets_CBT5); //initialization code
	virtual ~NumJetsPlots();
};
#endif // #ifdef ttbar_mc16ALL_AFII_cxx