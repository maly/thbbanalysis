'''
Author: Mohamed Aly
Email: maly@cern.ch
Description:

This script is desgined to study the heavy flavour content of NLO jets in ttbar+jets events.

The tH(bb)q analysis up to Aug 2021 has been using a simplified classification of HF jets, based on
variable HFSimpleClassification:
  - HFSimpleClassification = +1 for tt+ N b events
  - HFSimpleClassification = -1 for tt+ N c events
  - HFSimpleClassification = 0 for tt+ N lights events

A more extensive classification can be used based on variable HFClassification:
  - HFClassification = 1000*b + 100*B + 10*c + 1*C
    such that : b == 1 bjet inside 1 NLO jet (NLO jet is from a real b-quark)
                B == 2 bjets inside 1 NLO jet (NLO jet is from g->bb)
                c == 1 cjet inside 1 NLO jet (NLO jet is from a real c-quark)
                C == 2 cjets inside 1 NLO jet (NLO jet is from g->cc)

We want to document the fraction of ttbar+jets events with (incomplete):
  - 1b + 0 everything else -- HFClassification = 1000
  - 1c + 0 ...             -- HFClassification = 10
  - 2b + 0 everything else -- HFClassification = 2000
  - 2c + 0 ...             -- HFClassification = 20
  - 1B + 0 everything else -- HFClassification = 100
  - 1C + 0 ...             -- HFClassification = 1
  - 2B + 0 everything else -- HFClassification = 200
  - 2C + 0 ...             -- HFClassification = 2
  - N light + 0 ...         -- HFClassification = 0



  - 1B/C -- HFClassification = 100 or 1
  - 2B/C -- HFClassification = 200 or 1
  - 1b + nB -- HFClassification = 1000 + n*100 (overlap with 2b + nB ?)
  - 2b + nB -- HFClassification = 2000 + n*100
  


plotting requested
variables for one process but different generators in various PS regions. This script
CANNOT be used with real DATA.

  - The path to the slimmed datasets -- same as output path form thbb_slimming.py
  - The process to be studied
  - The variables to plot
  - The MC generators
  - PS regions to make the plots in.
  - The path where the plots should be saved

'''
import plotly.graph_objects as go
import uproot4 as uproot
from argparse import ArgumentParser, ArgumentTypeError
import sys
import os
import numpy as np
import pandas as pd
import h5py
import awkward as ak
from termcolor import cprint
from pathlib import Path
from tools.common import dhelp, REGION_TO_REGION_NAME_MAP
from tools.input_studies_common import (PROC_TO_AVAIL_GENS_MAP,
                                        GEN_IN_ARG_TO_GEN_NAME_MAP, process_type)
from tools.var_getters import VAR_TO_BRANCH_NAME_MAP, VAR_TO_EDGES_MAP
from tools.selectors import filter_by_region
from tools.style import GEN_TO_COLOR_MAP, GEN_TO_PRETTY_NAME, VAR_TO_PRETTY_NAME, PROC_TO_PRETTY_NAME
from tools.plotting import (Canvas, atlas_style, add_atlas_label, apply_plot_settings,
                            stick_text_on_plot, add_presel_cuts)

# nominal = uproot.open("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/tH_v31_minintuples_v3/mc16a_nom/410470_AFII_user.nbruscin.22889431._000017.output.root")["nominal_Loose"]
# #print(nominal.keys())
# import matplotlib.pyplot as plt
# hf_class = nominal['HF_Classification'].array()
# unique = []
# for entry in hf_class:
#     if entry not in unique:
#         unique.append(entry)
# print(unique)

# hist = plt.hist(hf_class, bins =np.linspace(-4000, 4000, 800), fill=False)
# #plt.xscale("log")
# plt.savefig("tmp.pdf")

# ==================================================================
# Get arguments from CL
# ==================================================================
# Help messages
# ================
_OUTDIR_HELP = dhelp('Path to directory where output files will be stored ')
_INPUT_HELP = dhelp('The directory where the slimmed json files are stored')
_PROCESSES_HELP = dhelp('The samples to check in form: proc_name,(optional)gen,(optional)FS/AFII')
_GENS_HELP = dhelp('The generators to compare')
_VARS_HELP = dhelp('The branches to load from the Ntuples')
_REGIONS_HELP = dhelp('The Phase-Space regions to make plots in')
# ================
# Defaults
# ================

DEFAULT_IN_DIR = '/eos/user/m/maly/thbb/analysis_files/slimmed_Ntuples/ttbar_hf_class_nom/'
DEFAULT_OUT_DIR = './output/HF_class/HTopWorkshop2021/'
DEFAULT_VARS = ['HF_Classification']
DEFAULT_PROCS = [('ttb', 'nominal', 'AFII'), ('ttc', 'nominal', 'AFII'), ('ttlight', 'nominal', 'AFII')]
DEFAULT_REGIONS = ['tH', 'ttb', 'ttc', 'ttlight']

# =========
# Choices
# ==========
VAR_CHOICES = ['HF_SimpleClassification', 'HF_Classification', 'HF_SimpleClassificationGhost', 'HF_ClassificationGhost']


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('--indir', default=DEFAULT_IN_DIR, help=_INPUT_HELP)
    parser.add_argument('--outdir', type=Path, default=DEFAULT_OUT_DIR, help=_OUTDIR_HELP)
    parser.add_argument('--variables', nargs="+", choices=VAR_CHOICES, default=DEFAULT_VARS, help=_VARS_HELP)
    parser.add_argument('--processes', nargs="+", type=process_type, default=DEFAULT_PROCS, help=_PROCESSES_HELP)
    parser.add_argument('--regions', nargs="+", choices=REGION_TO_REGION_NAME_MAP.keys(), default=DEFAULT_REGIONS, help=_REGIONS_HELP)
    parser.add_argument('--verbose', action='store_true')
    return parser.parse_args()


components = ['1b', '1b+1B', '1b+2B', '1b+1C', '1b+1c', '1c', '1c+1B', '1c+1C', '2b', '2c', '3b', '3c', '1B', '1C', '2B', '2C', 'light']
COMPONENT_TO_HFCLASS_MAP = {
    '1b':    1000,
    '1b+1B': 1100,
    '1b+2B': 1200,
    '1b+1C': 1001,
    '1b+1c': 1010,
    '1c':    10,
    '1c+1B': 110,
    '1c+1C': 11,
    '2b':   2000,
    '2c':   20,
    '3b':   3000,
    '3c':   30,
    '1B':   100,
    '1C':   1,
    '2B':   200,
    '2C':   2,
    'light': 0,
}


def run():
    # ======================
    # Prepare args
    # ======================
    args = get_args()
    # Be Verbose ?
    verbose = args.verbose
    # Ntuples Directory
    in_dir = args.indir
    # Slimmed files directory
    out_dir = args.outdir
    out_dir.mkdir(parents=True, exist_ok=True)
    # The processes to study
    procs = args.processes
    # The variables to plot
    variables = args.variables
    # The PS regions to compare
    regions = args.regions

    if verbose:
        cprint(f'INFO:: The sample to check is {proc} and the generators to comapre are {gens}\n', 'green')
    # Loaad the input datasets
    proc_details_to_vars_map = prepare_input_data(in_dir, procs, variables)
    # Loop over the requested variables to be used for the classification (Only HFClassification implemented)
    for var in variables:
        if verbose:
            cprint(f'INFO:: Using the following variable for Heavy Flavour Classification: {var}', 'green')
        # Loop over the regions of phase-space to make plots in
        for region in regions:
            # Get the region name as used in the analysis configuration
            proper_region = REGION_TO_REGION_NAME_MAP[region]
            if verbose:
                cprint(f'INFO:: Processing the following region: {proper_region}', 'green')
            # Prepare a CSV file to contain a table with the HF structure of NLO jets
            filename = proper_region+".csv"  # File
            out_path = out_dir/'Tables'/var  # OutputDirectory
            out_path.mkdir(parents=True, exist_ok=True)
            out_path = out_path/filename  # Path+FileName
            # Remove output file if it exists
            if os.path.isfile(out_path):
                os.unlink(out_path)
            # Preparw the dataframe which holds the HF categories
            basic_column_headers = ['Sample', 'Generator']  # Name tags for rows
            # The different flavour composition categories
            super_column_headers = [comp+' (%)' for comp in components]+['Total Fraction (%)']
            # For each flavour composition, are the HF jets prompt or not..
            super_columns_sub_headers = ['Prompt + NonPrompt', 'Prompt Only']
            # MultiIndex to produce Flavour X (Prompt, NonPrompt) combo for each flavour
            super_df_cols = pd.MultiIndex.from_product([super_column_headers, super_columns_sub_headers])
            # Make DataFrame with Columns = Flav Comp and SubColumns = Prompt/All
            # Rows will be proc+generator combos
            super_df = pd.DataFrame(columns=super_df_cols)
            # Add nametag columns to the end of dataframe
            for basic_col in basic_column_headers:
                super_df[basic_col] = ""
            proc_names = []
            gen_names = []
            # Open the output file
            with open(out_path, 'w') as outfile:
                # Loop over the ttbar samples to study (e.g. inclusive, ttb, ...)
                for proc, unfiltered_data in proc_details_to_vars_map.items():
                    # List to hold the the nametags
                    basic_column_vals = []
                    super_column_vals = []
                    # Get proc name (generator and AFII/FS always last two in string)
                    proc_deets = proc.split('_') 
                    proc_name = '_'.join(proc_deets[0:-2])
                    proc_names.append(proc_name)
                    basic_column_vals.append(proc_name)
                    # Get generator name
                    gen_name = proc_deets[-2]
                    basic_column_vals.append(gen_name)
                    gen_names.append(gen_name)
                    # The total fraction of events captured by component split
                    p_plus_np_frac_total, p_only_frac_total = 0, 0
                    for comp in components:
                        # Get the value of variable to cut on for selecting a category
                        hf_class_val = COMPONENT_TO_HFCLASS_MAP[comp]
                        # Filter the data to select the current region
                        data = filter_by_region(unfiltered_data, proper_region)
                        # Select events with both prompt and non-prompt jets in category
                        prompt_plus_noprompt_struct = data[abs(data[var]) == hf_class_val]
                        # Select events with only prompt jets (non-negative HFClassification)
                        prompt_only_struct = data[data[var] == hf_class_val]
                        # Calculate fraction of events with jets in the current HF category
                        prompt_plus_noprompt_frac = (prompt_plus_noprompt_struct.shape[0]/data.shape[0])*100
                        prompt_only_frac = (prompt_only_struct.shape[0]/data.shape[0])*100
                        # Add the fractions to the row
                        super_column_vals.extend([prompt_plus_noprompt_frac, prompt_only_frac])
                        # Add up the fractions from this category to get total frac selected
                        # by the implemented categories
                        p_plus_np_frac_total += prompt_plus_noprompt_frac
                        p_only_frac_total += prompt_only_frac

                    # Add the totals to dataframe row
                    super_column_vals.extend([p_plus_np_frac_total, p_only_frac_total])
                    # Add nametags to row
                    super_column_vals.extend(basic_column_vals)
                    # add the row to the end of the dataframe (loc gets row number)
                    super_df.loc[len(super_df)] = super_column_vals
                # Get the proc and generator to be the first two columns
                cols = list(super_df.columns)
                cols = [cols[-2]] + [cols[-1]] + cols[:-2]
                super_df = super_df[cols]
                # Drop columns where the HF fraction is <1% for all samples in this region
                super_df_subset = super_df.drop([col for col in super_df.columns
                                                if ((super_df[col].dtype == np.number
                                                    and (super_df[col] < 1.).all()))], axis=1)
                # Recalculate the total fraction picked up by categories
                prompt_columns = [col for col in list(super_df_subset.columns)
                                  if "+" not in col[1]
                                  and "Total" not in col[0]]
                prompt_nonprompt_cols = [col for col in list(super_df_subset.columns) 
                                         if "+" in col[1]
                                         and "Total" not in col[0]]
                tot_frac_prompt = super_df_subset.columns[-1]
                tot_frac_all = super_df_subset.columns[-2]
                super_df_subset[tot_frac_prompt] = super_df_subset[prompt_columns].sum(axis=1)
                super_df_subset[tot_frac_all] = super_df_subset[prompt_nonprompt_cols].sum(axis=1)
                # Save the dataframe into a CSV for this region
                super_df_subset.to_csv(outfile, index=False)
            

            
            for proc in proc_names:
                proc_row = super_df_subset.loc[super_df_subset['Sample'] == proc]
                proc_row.drop('Prompt + NonPrompt', axis=1, level=1, inplace=True)
                proc_cols = list(proc_row.columns)
                not_interesting = [colname[0] for colname in proc_cols if not any(x in colname[0] for x in ['1c', '2c', '1C', '1b', '2b', '1B','light'] if "+" not in colname[0])]
                totals_cols = list(set([colname[0] for colname in proc_cols if "Total" in colname[0]]))
                not_interesting = [elem for elem in not_interesting if elem not in totals_cols and "Sample" not in elem ]

                proc_row.columns = proc_row.columns.droplevel(1)
                proc_row.drop(columns=totals_cols, axis=1, inplace=True )
                proc_row['others'] = proc_row[not_interesting].sum(axis=1)
                proc_row.drop(not_interesting, axis=1, inplace=True)
                proc_row = proc_row.drop([col for col in proc_row.columns
                                                if ((proc_row[col].dtype == np.number
                                                    and (proc_row[col] < 1.).all()))], axis=1)
                proc_row.drop('Sample', axis = 1, inplace=True)
                proc_row = proc_row.round(1)
                fig = go.Figure(data=[go.Pie(labels=list(proc_row.columns),
                                            values=proc_row.values.tolist()[0], title = proc + ', ' + REGION_TO_REGION_NAME_MAP[region])])
                fig.update_traces(hoverinfo='label+percent', textinfo='label+percent', textfont_size=20, titlefont_size=28,
                                marker=dict(line=dict(color='#000000', width=2)))
                fig.update_layout(showlegend=False)
                #fig.show()
                fig.write_image(f"{out_dir}/piechart_{proc}_{region}.png")



# ==================================================================
# Read in the slimmed datasets and get a dataframe for each
# Process+Generator combo with all requested variables, if they are available
# ==================================================================
def prepare_input_data(in_dir, proc_gen_combos, requested_vars):
    # Prepare a dictionary to hold the dataframe for each proc+gen combo
    proc_details_to_vars_map = {}
    # Loop over the requested proc+generator combos
    for proc_gen_combo in proc_gen_combos:
        # Make a string with the proc and generator and simulation type
        str_combo = proc_gen_combo[0]+'_'+proc_gen_combo[1]+'_'+proc_gen_combo[2]
        # Array to hold the dataframe from each slimmed input file
        # There are multiple files due to the strucure of slimming script
        list_of_dfs = []
        # Loop over the files in the slimmed datasets directory
        for file_name in os.listdir(in_dir):
            # Skip irrelevant files
            if str_combo not in file_name:
                continue
            # Prepare a dictionary to hold the different datasets in each file
            # N-datasets for N variables kept during slimming
            var_name_to_data = {}
            # Open each file
            with h5py.File(in_dir+file_name, "r") as file:
                # Loop over the available variables, and save their data
                for var_name in list(file.keys()):
                    var_name_to_data[var_name] = np.array(file[var_name])
            # Make a dataframe with columns = variables
            df = pd.DataFrame(var_name_to_data)
            # Append the dataframe from this file to the ones from other files
            list_of_dfs.append(df)
        # If no files were found for the requested proc+gen combo, raise error
        try:
            proc_details_to_vars_map[str_combo] = pd.concat(list_of_dfs, ignore_index=True)
        except ValueError:
            raise ValueError(f'The requested process+generator combo {str_combo} is not available in the slimmed datasets...')
    # Finally, get the names of variables available from the slimming,
    # and cross-check them again requested variables
    avail_variables = list(proc_details_to_vars_map[list(proc_details_to_vars_map.keys())[0]].columns)
    # Raise error if requested variables not available in slimmed variables
    if not all(requested_variable in avail_variables for requested_variable in requested_vars):
        raise ValueError(f'The variables requested are not all in the slimmed ntuples. You can choose from: {avail_variables}\n')

    return proc_details_to_vars_map


if __name__ == '__main__':
    run()