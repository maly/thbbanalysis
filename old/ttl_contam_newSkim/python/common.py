'''
Brief: 

Common Methods for Making Plots from ROOT files 

'''
import numpy as np

def get_region_sample_definions(lazy_branches):
	#======== Region/Sample def branches :
	foam = lazy_branches['foam'][:,1] #Only foam[1]
	njets = lazy_branches['njets'][:]
	nbjets = lazy_branches['nbjets'][:]
	HF_SimpleClassification = lazy_branches['HF_SimpleClassification'][:]
	leptons_tight = lazy_branches['leptons_tight'][:,0] #only leptons_tight[0]
	
	#============ Region and Sample Definitions 
	njets_sel = (njets >= 5)
	nbjets_sel = (nbjets >= 4)
	ttH_orthog = ~(njets_sel & nbjets_sel)
	leptons_tight_found = (leptons_tight ==1)
	# tH 
	tH_foam = (foam==0.5)
	tH_RegionCut  =  (ttH_orthog & tH_foam)
	tH_SampleCut = leptons_tight_found
	# tWH
	tWH_foam = (foam==1.5)
	tWH_RegionCut = (ttH_orthog & tWH_foam)
	# ttb
	ttb_foam = (foam==2.5)
	ttb_RegionCut = (ttH_orthog & ttb_foam)
	XXX_is_b = (HF_SimpleClassification == 1)
	ttb_SampleCut = (leptons_tight_found & XXX_is_b)
	# ttc
	ttc_foam = (foam==3.5)
	ttc_RegionCut = ttH_orthog & ttc_foam
	XXX_is_c = (HF_SimpleClassification == -1) ;
	ttc_SampleCut = (leptons_tight_found & XXX_is_c)
	# ttl
	ttl_foam = (foam>=4.5)
	ttl_RegionCut = ttH_orthog & ttl_foam
	XXX_is_light = (HF_SimpleClassification == 0);
	ttl_SampleCut = (leptons_tight_found & XXX_is_light)

	REGION_CUT_MAP = { 'tH_SR' : tH_RegionCut,
						'tWH_CR' : tWH_RegionCut,
						'ttb_CR' : ttb_RegionCut,
						'ttc_CR' : ttc_RegionCut,
						'ttl_CR' : ttl_RegionCut}			
		 
	SAMPLE_CUT_MAP	= { 'tH' : tH_SampleCut,
						'ttb' : ttb_SampleCut,
						'ttc' : ttc_SampleCut,
						'ttl' : ttl_SampleCut}

	return REGION_CUT_MAP, SAMPLE_CUT_MAP

def get_evt_weight(lazy_branches):
	RunNum = lazy_branches["runNumber"][:]
	MC_Weight =  lazy_branches["weight_mc"][:]
	XS_Weight =  lazy_branches["xsec_weight"][:]
	PU_Weight =  lazy_branches["weight_pileup"][:]
	DL1r_Weights =  lazy_branches["weight_bTagSF_DL1r_Continuous"][:]
	JVT_Weight =  lazy_branches["weight_jvt"][:]
	ForwardJVT_Weight=  lazy_branches["weight_forwardjvt"][:]
	SoW =  lazy_branches["totalEventsWeighted"][:]
	lepSF_Weight =  lazy_branches["weight_leptonSF"][:]
	
	RunNum_290000_weight = np.zeros(len(RunNum))
	RunNum_290000_weight = np.where(RunNum<290000,36207.66,RunNum_290000_weight)

	RunNum_290000_310000_weight = np.zeros(len(RunNum))
	RunNum_290000 = RunNum>=290000
	RunNum_310000 = RunNum<310000
	RunNum_290000_310000_selec = RunNum_290000 & RunNum_310000
	RunNum_290000_310000_weight = np.where(RunNum_290000_310000_selec,44307.4,RunNum_290000_310000_weight)

	RunNum_310000_weight = np.zeros(len(RunNum))
	RunNum_310000_weight = np.where(RunNum>= 310000,58450.1,RunNum_310000_weight)


	evt_weight=((RunNum_290000_weight)+(RunNum_290000_310000_weight)+(RunNum_310000_weight))*MC_Weight*XS_Weight*PU_Weight*DL1r_Weights*JVT_Weight*ForwardJVT_Weight*lepSF_Weight/SoW;


	return evt_weight