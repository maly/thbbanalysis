//////////////////////////////////////////////////////////
// This class is responsible for drawing histograms and writing them out   
//////////////////////////////////////////////////////////
#ifndef TTBAR_HISTOS_h
#define TTBAR_HISTOS_h

#include <TObjArray.h>
#include <string>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1.h>
class ttbarHistos{
private:
	TObjArray* HistoList;

public:
	ttbarHistos(TFile* InFile, char* OutDir);
	void PrintPlots(char* OutDir, char* FileFormat);
	virtual ~ttbarHistos();
};
#endif // #ifdef ttbar_mc16ALL_AFII_cxx