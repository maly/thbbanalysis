#define TTBAR_HISTOS_CXX
#include "ttbarHistos.h"

ttbarHistos::ttbarHistos(TFile* InROOTFile, char* OutDir){
	HistoList = (TObjArray*) InROOTFile->Get("histo_list")
	PrintPlots(OutDir, "pdf");
}
void cbntHistos::PrintPlots(char* OutDir, char* FileFormat) {   
	TCanvas cPlotsCanvas;   
	cPlotsCanvas.cd();   
	TH1F *Histo = (TH1F*) HistoList->First();  
	for (int iObj = 0; iObj < HistoList->GetEntries(); iObj++) {
		Histo = (TH1F*) HistoList->At(iObj);      
		Histo->SetLineColor(4);      
		Histo->SetLineWidth(3);      
		Histo->Draw("E");

		//Saving Plots 
		std::string StringHistName(Histo->GetName());
		std::string StringFileFormat(FileFormat);
		std::string StringOutDir(OutDir);
		StringFileName = StringOutDir+StringHistName+StringFileFormat
		const char *FileName = StringFileName.c_str();
		cPlotsCanvas.SetLogy(0);     //  Should depend on histo name?              
		cPlotsCanvas.Print(FileName,FileFormat);   
	}
}
