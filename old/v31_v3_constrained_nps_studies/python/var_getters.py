'''
Brief:

Plot styling methods and maps

'''
import numpy as np

TREXFITTER_SYSTS_CATEG_GETTERS = {
 'none':'NONE',
 'btag b0':'btag b0\\*',
 'btag b1':'btag b1\\*',
 'btag b2':'btag b2\\*',
 'btag b3':'btag b3\\*',
 'btag b4':'btag b4\\*',
 'btag c0':'ATLAS_FTAG_C0\\*',
 'btag c1':'ATLAS_FTAG_C1\\*',
 'btag l0':'ATLAS_FTAG_LIGHT0\\*',
 'btag l1':'ATLAS_FTAG_LIGHT1\\*',
 'mu':'ATLAS_MU\\*',
 'elec':'ATLAS_E\\*',
 'jes0':'ATLAS_JES0_\\*',
 'jes1':'ATLAS_JES1_\\*',
 'jer':'ATLAS_JER_\\*',
 'met':'ATLAS_MET_\\*',
}

SYST_WEIGHTS_GETTERS = {
 'weightbTagSF_DL1r_Continuous_eigenvars_Light': 'weightbTagSF_DL1r_Continuous_eigenvars_Light',
 'weightbTagSF_DL1r_Continuous_eigenvars_C': 'weightbTagSF_DL1r_Continuous_eigenvars_C',
 'weightbTagSF_DL1r_Continuous_eigenvars_B': 'weightbTagSF_DL1r_Continuous_eigenvars_B',
 'weightforwardjvt': 'weightforwardjvt',
 'weightjvt': 'weightjvt',
 'weightglobalLeptonTriggerSF_MU_Trigger_SYST': 'weightglobalLeptonTriggerSF_MU_Trigger_SYST',
 'weightglobalLeptonTriggerSF_MU_Trigger_STAT': 'weightglobalLeptonTriggerSF_MU_Trigger_STAT',
 'weightglobalLeptonTriggerSF_EL_Trigger': 'weightglobalLeptonTriggerSF_EL_Trigger',
 'weightleptonSF_MU_SF_TTVA_SYST': 'weightleptonSF_MU_SF_TTVA_SYST',
 'weightleptonSF_MU_SF_TTVA_STAT': 'weightleptonSF_MU_SF_TTVA_STAT',
 'weightleptonSF_MU_SF_Isol_SYST': 'weightleptonSF_MU_SF_Isol_SYST',
 'weightleptonSF_MU_SF_Isol_STAT': 'weightleptonSF_MU_SF_Isol_STAT',
 'weightleptonSF_MU_SF_ID_SYST_LOWPT': 'weightleptonSF_MU_SF_ID_SYST_LOWPT',
 'weightleptonSF_MU_SF_ID_STAT_LOWPT': 'weightleptonSF_MU_SF_ID_STAT_LOWPT',
 'weightleptonSF_MU_SF_ID_SYST': 'weightleptonSF_MU_SF_ID_SYST',
 'weightleptonSF_MU_SF_ID_STAT': 'weightleptonSF_MU_SF_ID_STAT',
 'weightleptonSF_MU_SF_Trigger_SYST': 'weightleptonSF_MU_SF_Trigger_SYST',
 'weightleptonSF_MU_SF_Trigger_STAT': 'weightleptonSF_MU_SF_Trigger_STAT',
 'weightleptonSF_EL_SF_Isol': 'weightleptonSF_EL_SF_Isol',
 'weightleptonSF_EL_SF_ID': 'weightleptonSF_EL_SF_ID',
 'weightleptonSF_EL_SF_Reco': 'weightleptonSF_EL_SF_Reco',
 'weightleptonSF_EL_SF_Trigger': 'weightleptonSF_EL_SF_Trigger',
 'weightpileup': 'weightpileup',
}
SYST_WEIGHT_VARIATION_MAP = {
 'up':'_UP',
 'down':'_DOWN',
 'nominal':'',
}
SYST_TREES_BASE_GETTERS = {
 'nominal': 'nominal_Loose',
 'CategoryReduction_JET_BJES_Response':'CategoryReduction_JET_BJES_Response',
 'CategoryReduction_JET_EffectiveNP_Detector1':'CategoryReduction_JET_EffectiveNP_Detector1',
 'CategoryReduction_JET_EffectiveNP_Detector2':'CategoryReduction_JET_EffectiveNP_Detector2',
 'CategoryReduction_JET_EffectiveNP_Mixed1':'CategoryReduction_JET_EffectiveNP_Mixed1',
 'CategoryReduction_JET_EffectiveNP_Mixed2':'CategoryReduction_JET_EffectiveNP_Mixed2',
 'CategoryReduction_JET_EffectiveNP_Mixed3':'CategoryReduction_JET_EffectiveNP_Mixed3',
 'CategoryReduction_JET_EffectiveNP_Modelling1':'CategoryReduction_JET_EffectiveNP_Modelling1',
 'CategoryReduction_JET_EffectiveNP_Modelling2':'CategoryReduction_JET_EffectiveNP_Modelling2',
 'CategoryReduction_JET_EffectiveNP_Modelling3':'CategoryReduction_JET_EffectiveNP_Modelling3',
 'CategoryReduction_JET_EffectiveNP_Modelling4':'CategoryReduction_JET_EffectiveNP_Modelling4',
 'CategoryReduction_JET_EffectiveNP_Statistical1':'CategoryReduction_JET_EffectiveNP_Statistical1',
 'CategoryReduction_JET_EffectiveNP_Statistical2':'CategoryReduction_JET_EffectiveNP_Statistical2',
 'CategoryReduction_JET_EffectiveNP_Statistical3':'CategoryReduction_JET_EffectiveNP_Statistical3',
 'CategoryReduction_JET_EffectiveNP_Statistical4':'CategoryReduction_JET_EffectiveNP_Statistical4',
 'CategoryReduction_JET_EffectiveNP_Statistical5':'CategoryReduction_JET_EffectiveNP_Statistical5',
 'CategoryReduction_JET_EffectiveNP_Statistical6':'CategoryReduction_JET_EffectiveNP_Statistical6',
 'CategoryReduction_JET_EtaIntercalibration_Modelling':'CategoryReduction_JET_EtaIntercalibration_Modelling',
 'CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data':'CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data',
 'CategoryReduction_JET_EtaIntercalibration_NonClosure_highE':'CategoryReduction_JET_EtaIntercalibration_NonClosure_highE',
 'CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta':'CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta',
 'CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta':'CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta',
 'CategoryReduction_JET_EtaIntercalibration_TotalStat':'CategoryReduction_JET_EtaIntercalibration_TotalStat',
 'CategoryReduction_JET_Flavor_Composition':'CategoryReduction_JET_Flavor_Composition',
 'CategoryReduction_JET_Flavor_Response':'CategoryReduction_JET_Flavor_Response',
 'CategoryReduction_JET_JER_DataVsMC_MC16':'CategoryReduction_JET_JER_DataVsMC_MC16',
 'CategoryReduction_JET_JER_EffectiveNP_1':'CategoryReduction_JET_JER_EffectiveNP_1',
 'CategoryReduction_JET_JER_EffectiveNP_2':'CategoryReduction_JET_JER_EffectiveNP_2',
 'CategoryReduction_JET_JER_EffectiveNP_3':'CategoryReduction_JET_JER_EffectiveNP_3',
 'CategoryReduction_JET_JER_EffectiveNP_4':'CategoryReduction_JET_JER_EffectiveNP_4',
 'CategoryReduction_JET_JER_EffectiveNP_5':'CategoryReduction_JET_JER_EffectiveNP_5',
 'CategoryReduction_JET_JER_EffectiveNP_6':'CategoryReduction_JET_JER_EffectiveNP_6',
 'CategoryReduction_JET_JER_EffectiveNP_7restTerm':'CategoryReduction_JET_JER_EffectiveNP_7restTerm',
 'CategoryReduction_JET_Pileup_OffsetMu':'CategoryReduction_JET_Pileup_OffsetMu',
 'CategoryReduction_JET_Pileup_OffsetNPV':'CategoryReduction_JET_Pileup_OffsetNPV',
 'CategoryReduction_JET_Pileup_PtTerm':'CategoryReduction_JET_Pileup_PtTerm',
 'CategoryReduction_JET_Pileup_RhoTopology':'CategoryReduction_JET_Pileup_RhoTopology',
 'CategoryReduction_JET_PunchThrough_MC16':'CategoryReduction_JET_PunchThrough_MC16',
 'CategoryReduction_JET_SingleParticle_HighPt':'CategoryReduction_JET_SingleParticle_HighPt',
 'EG_RESOLUTION_ALL':'EG_RESOLUTION_ALL',
 'EG_SCALE_AF2':'EG_SCALE_AF2',
 'EG_SCALE_ALL':'EG_SCALE_ALL',
 'MET_SoftTrk_ResoPara':'MET_SoftTrk_ResoPara',
 'MET_SoftTrk_ResoPerp':'MET_SoftTrk_ResoPerp',
 'MET_SoftTrk_Scale':'MET_SoftTrk_Scale',
 'MUON_ID':'MUON_ID',
 'MUON_MS':'MUON_MS',
 'MUON_SAGITTA_RESBIAS':'MUON_SAGITTA_RESBIAS',
 'MUON_SAGITTA_RHO':'MUON_SAGITTA_RHO',
 'MUON_SCALE':'MUON_SCALE',
}

SYST_TREE_VARIATION_MAP = {
 'up':'__1up_Loose',
 'down':'__1down_Loose',
 'none':'_Loose',
}


def get_btag_syst(flavour='0', i_j=(0, 0)):
    def get_syst(tree):
        i = i_j[0]
        j = i_j[1]
        flav = flavour.upper()
        if(i == 0):
            idx = j
        else:
            idx = int(str(i) + str(j))
        syst_up_num = 'weight_bTagSF_DL1r_Continuous_eigenvars_' + flav + '_up'
        syst_down_num = 'weight_bTagSF_DL1r_Continuous_eigenvars_' + flav + '_down'
        denom = 'weight_bTagSF_DL1r_Continuous'
        syst_up = np.array(tree[syst_up_num])[idx]/np.array(tree[denom])
        syst_down = np.array(tree[syst_down_num])[idx]/np.array(denom)
        return syst_up, syst_down
    return get_syst


def get_syst_tree(tree_name, one_sided=False, side=''):
    if(not one_sided):
        up_tree = SYST_TREES_BASE_GETTERS[tree_name]+SYST_TREE_VARIATION_MAP['up']
        down_tree = SYST_TREES_BASE_GETTERS[tree_name]+SYST_TREE_VARIATION_MAP['down']
        print(f'INFO::Processing two-sided Systematic')

        return up_tree, down_tree
    else:
        tree = SYST_TREES_BASE_GETTERS[tree_name]+SYST_TREE_VARIATION_MAP[side]
        print(f'INFO::Processing one-sided Systematic')

        return tree


TREX_SYST_GETTER = {
    'JES effective NP modelling 1':
        get_syst_tree('CategoryReduction_JET_EffectiveNP_Modelling1'),
    'JES pileup rho topology':
        get_syst_tree('CategoryReduction_JET_Pileup_RhoTopology'),
    'JES pileup offset NPV':
        get_syst_tree('CategoryReduction_JET_Pileup_OffsetNPV'),
    'JES pileup offset mu':
        get_syst_tree('CategoryReduction_JET_Pileup_OffsetMu'),
    'JES flavour response':
        get_syst_tree('CategoryReduction_JET_Flavor_Response'),
    'JES flavour composition':
        get_syst_tree('CategoryReduction_JET_Flavor_Composition'),
    'JES eta intercalibration modelling':
        get_syst_tree('CategoryReduction_JET_EtaIntercalibration_Modelling'),
    'JES BJES':
        get_syst_tree('CategoryReduction_JET_BJES_Response'),
    'JER effective NP 7 rest term':
        get_syst_tree('CategoryReduction_JET_JER_EffectiveNP_7restTerm'),
    'JER effective NP 6':
        get_syst_tree('CategoryReduction_JET_JER_EffectiveNP_6'),
    'JER effective NP 5':
        get_syst_tree('CategoryReduction_JET_JER_EffectiveNP_5'),
    'JER effective NP 4':
        get_syst_tree('CategoryReduction_JET_JER_EffectiveNP_4'),
    'JER effective NP 3':
        get_syst_tree('CategoryReduction_JET_JER_EffectiveNP_3'),
    'JER effective NP 2':
        get_syst_tree('CategoryReduction_JET_JER_EffectiveNP_2'),
    'JER effective NP 1':
        get_syst_tree('CategoryReduction_JET_JER_EffectiveNP_1'),
    'JER DataVsMC':
        get_syst_tree('CategoryReduction_JET_JER_DataVsMC_MC16'),
    'MET soft reso (perp.)':
        get_syst_tree('MET_SoftTrk_ResoPerp', one_sided=True, side='none'),
    'MET soft reso (para.)':
        get_syst_tree('MET_SoftTrk_ResoPara', one_sided=True, side='none'),
    'btag b-jets EV 00':
        get_btag_syst(flavour='b', i_j=(0, 0)),
    'btag b-jets EV 01':
        get_btag_syst(flavour='b', i_j=(0, 1)),
    'btag b-jets EV 02':
        get_btag_syst(flavour='b', i_j=(0, 2)),
    'btag b-jets EV 03':
        get_btag_syst(flavour='b', i_j=(0, 3)),
    'btag b-jets EV 04':
        get_btag_syst(flavour='b', i_j=(0, 4)),
    'btag b-jets EV 05':
        get_btag_syst(flavour='b', i_j=(0, 5)),
    'btag b-jets EV 06':
        get_btag_syst(flavour='b', i_j=(0, 6)),
    'btag b-jets EV 07':
        get_btag_syst(flavour='b', i_j=(0, 7)),
    'btag b-jets EV 08':
        get_btag_syst(flavour='b', i_j=(0, 8)),
    'btag b-jets EV 09':
        get_btag_syst(flavour='b', i_j=(0, 9)),
    'btag b-jets EV 10':
        get_btag_syst(flavour='b', i_j=(1, 0)),
    'btag b-jets EV 11':
        get_btag_syst(flavour='b', i_j=(1, 1)),
    'btag b-jets EV 12':
        get_btag_syst(flavour='b', i_j=(1, 2)),
    'btag b-jets EV 13':
        get_btag_syst(flavour='b', i_j=(1, 3)),
    'btag b-jets EV 14':
        get_btag_syst(flavour='b', i_j=(1, 4)),
    'btag b-jets EV 15':
        get_btag_syst(flavour='b', i_j=(1, 5)),
    'btag b-jets EV 16':
        get_btag_syst(flavour='b', i_j=(1, 6)),
    'btag b-jets EV 17':
        get_btag_syst(flavour='b', i_j=(1, 7)),
    'btag b-jets EV 18':
        get_btag_syst(flavour='b', i_j=(1, 8)),
    'btag b-jets EV 19':
        get_btag_syst(flavour='b', i_j=(1, 9)),
    'btag b-jets EV 20':
        get_btag_syst(flavour='b', i_j=(2, 0)),
    'btag b-jets EV 21':
        get_btag_syst(flavour='b', i_j=(2, 1)),
    'btag b-jets EV 22':
        get_btag_syst(flavour='b', i_j=(2, 2)),
    'btag b-jets EV 23':
        get_btag_syst(flavour='b', i_j=(2, 3)),
    'btag b-jets EV 24':
        get_btag_syst(flavour='b', i_j=(2, 4)),
    'btag b-jets EV 25':
        get_btag_syst(flavour='b', i_j=(2, 5)),
    'btag b-jets EV 26':
        get_btag_syst(flavour='b', i_j=(2, 6)),
    'btag b-jets EV 27':
        get_btag_syst(flavour='b', i_j=(2, 7)),
    'btag b-jets EV 28':
        get_btag_syst(flavour='b', i_j=(2, 8)),
    'btag b-jets EV 29':
        get_btag_syst(flavour='b', i_j=(2, 9)),
    'btag b-jets EV 30':
        get_btag_syst(flavour='b', i_j=(3, 0)),
    'btag b-jets EV 31':
        get_btag_syst(flavour='b', i_j=(3, 1)),
    'btag b-jets EV 32':
        get_btag_syst(flavour='b', i_j=(3, 2)),
    'btag b-jets EV 33':
        get_btag_syst(flavour='b', i_j=(3, 3)),
    'btag b-jets EV 34':
        get_btag_syst(flavour='b', i_j=(3, 4)),
    'btag b-jets EV 35':
        get_btag_syst(flavour='b', i_j=(3, 5)),
    'btag b-jets EV 36':
        get_btag_syst(flavour='b', i_j=(3, 6)),
    'btag b-jets EV 37':
        get_btag_syst(flavour='b', i_j=(3, 7)),
    'btag b-jets EV 38':
        get_btag_syst(flavour='b', i_j=(3, 8)),
    'btag b-jets EV 39':
        get_btag_syst(flavour='b', i_j=(3, 9)),
    'btag b-jets EV 40':
        get_btag_syst(flavour='b', i_j=(4, 0)),
    'btag b-jets EV 41':
        get_btag_syst(flavour='b', i_j=(4, 1)),
    'btag b-jets EV 42':
        get_btag_syst(flavour='b', i_j=(4, 2)),
    'btag b-jets EV 43':
        get_btag_syst(flavour='b', i_j=(4, 3)),
    'btag b-jets EV 44':
        get_btag_syst(flavour='b', i_j=(4, 4)),
    'btag c-jets EV 00':
        get_btag_syst(flavour='c', i_j=(0, 0)),
    'btag c-jets EV 01':
        get_btag_syst(flavour='c', i_j=(0, 1)),
    'btag c-jets EV 02':
        get_btag_syst(flavour='c', i_j=(0, 2)),
    'btag c-jets EV 03':
        get_btag_syst(flavour='c', i_j=(0, 3)),
    'btag c-jets EV 04':
        get_btag_syst(flavour='c', i_j=(0, 4)),
    'btag c-jets EV 05':
        get_btag_syst(flavour='c', i_j=(0, 5)),
    'btag c-jets EV 06':
        get_btag_syst(flavour='c', i_j=(0, 6)),
    'btag c-jets EV 07':
        get_btag_syst(flavour='c', i_j=(0, 7)),
    'btag c-jets EV 08':
        get_btag_syst(flavour='c', i_j=(0, 8)),
    'btag c-jets EV 09':
        get_btag_syst(flavour='c', i_j=(0, 9)),
    'btag c-jets EV 10':
        get_btag_syst(flavour='c', i_j=(1, 0)),
    'btag c-jets EV 11':
        get_btag_syst(flavour='c', i_j=(1, 1)),
    'btag c-jets EV 12':
        get_btag_syst(flavour='c', i_j=(1, 2)),
    'btag c-jets EV 13':
        get_btag_syst(flavour='c', i_j=(1, 3)),
    'btag c-jets EV 14':
        get_btag_syst(flavour='c', i_j=(1, 4)),
    'btag c-jets EV 15':
        get_btag_syst(flavour='c', i_j=(1, 5)),
    'btag c-jets EV 16':
        get_btag_syst(flavour='c', i_j=(1, 6)),
    'btag c-jets EV 17':
        get_btag_syst(flavour='c', i_j=(1, 7)),
    'btag c-jets EV 18':
        get_btag_syst(flavour='c', i_j=(1, 8)),
    'btag c-jets EV 19':
        get_btag_syst(flavour='c', i_j=(1, 9)),
    'btag light-jets EV 00':
        get_btag_syst(flavour='light', i_j=(0, 0)),
    'btag light-jets EV 01':
        get_btag_syst(flavour='light', i_j=(0, 1)),
    'btag light-jets EV 02':
        get_btag_syst(flavour='light', i_j=(0, 2)),
    'btag light-jets EV 03':
        get_btag_syst(flavour='light', i_j=(0, 3)),
    'btag light-jets EV 04':
        get_btag_syst(flavour='light', i_j=(0, 4)),
    'btag light-jets EV 05':
        get_btag_syst(flavour='light', i_j=(0, 5)),
    'btag light-jets EV 06':
        get_btag_syst(flavour='light', i_j=(0, 6)),
    'btag light-jets EV 07':
        get_btag_syst(flavour='light', i_j=(0, 7)),
    'btag light-jets EV 08':
        get_btag_syst(flavour='light', i_j=(0, 8)),
    'btag light-jets EV 09':
        get_btag_syst(flavour='light', i_j=(0, 9)),
    'btag light-jets EV 10':
        get_btag_syst(flavour='light', i_j=(1, 0)),
    'btag light-jets EV 11':
        get_btag_syst(flavour='light', i_j=(1, 1)),
    'btag light-jets EV 12':
        get_btag_syst(flavour='light', i_j=(1, 2)),
    'btag light-jets EV 13':
        get_btag_syst(flavour='light', i_j=(1, 3)),
    'btag light-jets EV 14':
        get_btag_syst(flavour='light', i_j=(1, 4)),
    'btag light-jets EV 15':
        get_btag_syst(flavour='light', i_j=(1, 5)),
    'btag light-jets EV 16':
        get_btag_syst(flavour='light', i_j=(1, 6)),
    'btag light-jets EV 17':
        get_btag_syst(flavour='light', i_j=(1, 7)),
    'btag light-jets EV 18':
        get_btag_syst(flavour='light', i_j=(1, 8)),
    'btag light-jets EV 19':
        get_btag_syst(flavour='light', i_j=(1, 9)),
 }

