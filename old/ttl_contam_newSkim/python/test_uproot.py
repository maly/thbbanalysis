import uproot3
import uproot as uproot4
import numpy as np

#===================== Getting Set ========================
nominal_Loose_4 = uproot4.open("/afs/cern.ch/work/m/maly/public/thbb/thbbanalysis/ttl_contam_newSkim/ttbar/ttbar_mc16ALL_AFII.root:nominal_Loose")
nominal_Loose_3 = uproot3.open("/afs/cern.ch/work/m/maly/public/thbb/thbbanalysis/ttl_contam_newSkim/ttbar/ttbar_mc16ALL_AFII.root")['nominal_Loose']

print("NumEntries = ",nominal_Loose_3.numentries)
#print(nominal_Loose_3.keys())
#===================== Accessing TTree with array ========================
n_tophad_jets_CBT123_ttAll = nominal_Loose_3["n_tophad_jets_CBT123_ttAll"].array()

#===================== Accessing TTree with array and Cache ========================
#Cache dict -- filled with unique ID -> array pairs 
# Unique ID is used to identify previously read array 
#======== Auto Cache Size 
#MyCache = uproot3.ArrayCache("20MB")
#nominal_Loose_3.arrays("njets_CBT*", cache=MyCache);
#print(len(MyCache), len(nominal_Loose_4.keys(filter_name= "njets_CBT*")))
#======== Manual Cache Size and handling 
#MyCache={}
#njets_per_CBT = nominal_Loose.arrays(["njets_CBT*"], cache=MyCache) #First time reads from file 
#njets_per_CBT = nominal_Loose.arrays(["njets_CBT*"], cache=MyCache) #Second time reads from Cache 
#MyCache.clear() 

#============== Accessing TTree with Lazy Arrays  ========================
#LazyData = nominal_Loose_3.lazyarrays("*")
#LazyData_Chunky = nominal_Loose_3.lazyarrays("*",entrysteps=1000)
#LazyData_Chunky_N_CBT = LazyData["njets_CBT5"]
#print(LazyData_Chunky_N_CBT)
#Lightweight skimming and derived quantities 
#njets_CBT = nominal_Loose_3.lazyarrays(["*CBT*"], persistvirtual=True)
#weights = nominal_Loose_3.lazyarrays(["*weight*","*Weight"], persistvirtual=True)
#RunNum = nominal_Loose_3.lazyarrays(["*run*"], persistvirtual=True)


#njets_CBT['n_tophad_jets'] = njets_CBT["n_tophad_jets_CBT0"]+njets_CBT["n_tophad_jets_CBT123"]+njets_CBT["n_tophad_jets_CBT4"]+njets_CBT["n_tophad_jets_CBT5"]
#import awkward0 
#awkward0.save("derived_quant.awkd",njets_CBT,mode="w")
#njets_CBT_v2 = awkward0.load("derived_quant.awkd")
#njets_CBT_v2["n_tophad_jets_CBT0"] #retrieved from pointers to ROOT file 
#njets_CBT_v2["n_tophad_jets"] #retrireved from Awk file
#Lightweight selection 
#sel_njets_CBT= njets_CBT
#print(len(sel_njets_CBT))
#sel_njets_CBT = sel_njets_CBT[sel_njets_CBT['n_tophad_jets_CBT5'] > 1.]
#print(len(sel_njets_CBT))
#sel_njets_CBT = sel_njets_CBT[sel_njets_CBT['n_tophad_jets_CBT123'] >0.]
#print(len(sel_njets_CBT))

#awkward0.save("selected-events.awkd", sel_njets_CBT, mode="w")

#njets_CBT_v3 = awkward0.load("selected-events.awkd")
#print(njets_CBT_v3)
#n_tophad_jets_CBT5 = np.array(njets_CBT_v3["n_tophad_jets_CBT5"])
#n_tophad_jets_CBT123 = np.array(njets_CBT_v3["n_tophad_jets_CBT123"])



#============== Accessing TTree with Iteration  ========================
# Accessing TTree with iteration
import time
start_time = time.time()

histogram = None
for data in nominal_Loose_3.iterate(["*CBT*", "*weight*","*Weight*","runNumber"], namedecode="utf-8", entrysteps=200000):
	# operate on a batch of data in the loop
	n_tophad_jets = data["n_nontophad_jets_CBT0"]+data["n_nontophad_jets_CBT123"]+data["n_nontophad_jets_CBT4"]+data["n_nontophad_jets_CBT5"]
	RunNum = data["runNumber"]
	MC_Weight =  data["weight_mc"]
	XS_Weight =  data["xsec_weight"]
	PU_Weight =  data["weight_pileup"]
	DL1r_Weights =  data["weight_bTagSF_DL1r_Continuous"]
	JVT_Weight =  data["weight_jvt"]
	ForwardJVT_Weight=  data["weight_forwardjvt"]
	SoW =  data["totalEventsWeighted"]
	lepSF_Weight =  data["weight_leptonSF"]
	RunNum_290000_weight = np.zeros(len(RunNum))
	RunNum_290000_weight = [36207.66 for element in RunNum if element < 290000]
	RunNum_290000_310000_weight = np.zeros(len(RunNum))
	RunNum_290000_310000_weight = [44307.4 for element in RunNum if element >=290000 and element<310000]
	RunNum_310000_weight = np.zeros(len(RunNum))
	RunNum_310000_weight = [58450.1 for element in RunNum if element >= 310000]

	evt_weight=((RunNum_290000_weight)+(RunNum_290000_310000_weight)+(RunNum_310000_weight))*MC_Weight*XS_Weight*PU_Weight*DL1r_Weights*JVT_Weight*ForwardJVT_Weight/SoW*lepSF_Weight;
	#print(RunNum,MC_Weight,XS_Weight,PU_Weight,DL1r_Weights,JVT_Weight,ForwardJVT_Weight,SoW,lepSF_Weight)
	# accumulate results
	counts, edges = np.histogram(n_tophad_jets, bins=6, range=(0, 5),weights= evt_weight)
	if histogram is None:
	    histogram = counts, edges
	else:
	    histogram = histogram[0] + counts, edges

current_time = time.time()
elapsed_time = current_time - start_time
print(elapsed_time)
import matplotlib.pyplot as plt
counts, edges = histogram
plt.step(x=edges, y=np.append(counts, 0), where="post");
plt.xlim(edges[0], edges[-1]);
plt.ylim(0, counts.max() * 1.1);
plt.xlabel("Num Non TopHad Jets");
plt.ylabel("events per bin");
plt.savefig('NumNonTopHadJets.pdf')
#============== Accessing Jagged array branches ========================
#jets_pt = nominal_Loose_3.array("jets_pt")
