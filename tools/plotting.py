"""
This is responisble for building methods that can be used
in styling a matplotlib figure with some ATLAS settings

It also contains a definition of a Canvas class and
CanvasWithRatio class that are ATLAS suitable
"""

import os

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.ticker import FuncFormatter
from matplotlib.figure import Figure
from matplotlib import gridspec
from matplotlib.artist import setp
from matplotlib.ticker import AutoMinorLocator
import matplotlib.ticker

import math
import numpy as np

# ==============================================
# ================== Classes ===================
# ==============================================
# Canvas class for a normal plot without ratio panel
# ==============================================
class Canvas:
    # Set some default filename in case none provided
    default_name = 'Canvas.pdf'

    # Class constructor
    def __init__(self, out_path=None, figsize=(22.5, 15), fontsize=18):
        # Build a figure with requested size
        self.fig = Figure(figsize)
        # Choose a tight-layout to reduce white space
        self.fig.set_tight_layout(True)
        # Get the Canvas containing the figure
        self.canvas = FigureCanvas(self.fig)
        # Get the axes to plot on
        self.ax = self.fig.add_subplot(1, 1, 1)
        # Get the output file path
        self.out_path = str(out_path)

        # Style the axies with ATLAS settings
        atlas_style(self.ax, fontsize=fontsize)

    # Define a saving method to write out the plot
    def save(self, out_path=None):
        # Make sure there is a file name provided either
        # via a direct call to this func or via construct
        output = out_path if out_path is not None else self.out_path
        assert output, "An output file name is required"

        # Sepearate file name and directory
        out_dir, out_file = os.path.split(output)
        # Make the output directory if not there
        if out_dir and not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        # Now we print out the Canvas onto the file
        self.canvas.print_figure(output, bbox_inches='tight')

    # ================
    # Methods to use canvas in "with" statements
    # ================#
    # Method called at start of "with" block
    def __enter__(self):
        # Set the outpath using constructor or default name
        if not self.out_path:
            self.out_path = self.default_name
        # return the Canvas
        return self

    # Method called at end of "with" block
    def __exit__(self, extype, exval, extb):
        if extype:
            return None
        # Save the Canvas in the outpath
        self.save(out_path=self.out_path)
        return True


# ==============================================
# Canvas class for a plot with ratio panel
# ==============================================
class CanvasWithRatio:
    # Set some default filename in case none provided
    default_name = 'CanvasWithRatio.pdf'

    # Class constructor
    def __init__(self, out_path=None, figsize=(15, 10)):
        # Build a figure with requested size
        self.fig = Figure(figsize)
        # Get the Canvas containing the figure
        self.canvas = FigureCanvas(self.fig)
        # GridSpec customizes location of subplots
        gs = gridspec.GridSpec(2, 1, height_ratios=[5, 1])
        # Make axes on the subplots
        self.ax = self.fig.add_subplot(gs[0])
        # Ratio panel shares x-axis of "ax"
        self.ax2 = self.fig.add_subplot(gs[1], sharex=self.ax)
        # Adjust the height of space between subplots
        self.fig.subplots_adjust(hspace=0.1)
        # Get the output file path
        self.out_path = str(out_path)
        # Style the axies with ATLAS settings
        atlas_style(self.ax)
        atlas_style(self.ax2)

        # hide the x-tick lables on the main plot
        setp(self.ax.get_xticklabels(), visible=False)

    # Define a saving method to write out the plot
    def save(self, out_path=None):
        # Make sure there is a file name provided either
        # via a direct call to this func or via construct.
        output = out_path if out_path is not None else self.out_path
        assert output, "an output file name is required"
        # Sepearate file name and directory
        out_dir, out_file = os.path.split(output)
        # Make the output directory if not there
        if out_dir and not os.path.isdir(out_dir):
            os.makedirs(out_dir)

        # Now we print out the Canvas onto the file
        self.canvas.print_figure(output, bbox_inches='tight')
    # ================
    # Methods to use canvas in "with" statements
    # ================
    # Method called at start of "with" block

    def __enter__(self):
        # Set the outpath using constructor or default name
        if not self.out_path:
            self.out_path = self.default_name
        # return the Canvas
        return self

    # Method called at end of "with" block
    def __exit__(self, extype, exval, extb):
        if extype:
            return None
        # Save the Canvas in the outpath
        self.save(out_path=self.out_path)
        return True


# ==================================================================
# Put text on the plot
# ==================================================================
def stick_text_on_plot(ax, **text_settings):
    text_arr = text_settings['text']
    textsize = text_settings['textsize']
    xpos = text_settings['xpos']
    ypos = text_settings['ypos']
    vspace = text_settings['vspace']

    yval = ypos
    for txt in text_arr:
        ax.text(x=xpos, y=yval, s=txt, fontsize=textsize, transform=ax.transAxes)
        yval -= vspace


# ==================================================================
# Set the plot label sizes and legend options
# ==================================================================
def apply_plot_settings(ax, **plot_settings):
    if 'xlabel' in plot_settings.keys():
        xlabel = plot_settings['xlabel']
        ax.set_xlabel(xlabel=xlabel)
        if 'xlabel_size' in plot_settings.keys():
            xlabel_size = plot_settings['xlabel_size']
            ax.set_xlabel(xlabel=xlabel, fontsize=xlabel_size)
    if 'ylabel' in plot_settings.keys():
        ylabel = plot_settings['ylabel']
        ax.set_ylabel(ylabel=ylabel)
        if 'ylabel_size' in plot_settings.keys():
            ylabel_size = plot_settings['ylabel_size']
            ax.set_ylabel(ylabel=ylabel, fontsize=ylabel_size)
    if 'xticklabels_size' in plot_settings.keys():
        xticklabels_size = plot_settings['xticklabels_size']
        ax.tick_params(axis='x', labelsize=xticklabels_size)
    if 'yticklabels_size' in plot_settings.keys():
        yticklabels_size = plot_settings['yticklabels_size']
        ax.tick_params(axis='y', labelsize=yticklabels_size)
    legend_args = {}
    if 'legend_size' in plot_settings.keys():
        legend_size = plot_settings['legend_size']
        legend_args['fontsize'] = legend_size
    if 'legend_pos' in plot_settings.keys():
        legend_pos = plot_settings['legend_pos']
        legend_args['loc'] = legend_pos
    if len(list(legend_args.keys())) !=0 :
        ax.legend(**legend_args)


# ==================================================================
# ATLAS Axes styling. This is standard and shouldn't need to changE
# ==================================================================
def atlas_style(ax, fontsize=18):
    where = {x: True for x in ['bottom', 'top', 'left', 'right']}
    ax.tick_params(which='both', direction='in', labelsize=fontsize, **where)
    ax.tick_params(which='minor', length=6)
    ax.tick_params(which='major', length=12)
    ax.grid(True, which='major', alpha=0.30)
    ax.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5))


# ==============================================
# ============= AddInfo to Canvas =============
# ==============================================
# ATLAS Label
# ==============================================
def add_atlas_label(ax, x=0.05, y=0.91,  vshift=0.04,
                    prelim=False, internal=True, simulation=False):

    text_fd = dict(ha='left', va='center')
    atlas_fd = dict(weight='bold', style='italic', size=34, **text_fd)
    internal_fd = dict(size=32, **text_fd)
    shift = 0.15*0.7
    ax.text(x, y, 'ATLAS', fontdict=atlas_fd, transform=ax.transAxes)
    if(internal and not prelim and not simulation):
        ax.text(x+shift, y,
                'Internal',
                fontdict=internal_fd,
                transform=ax.transAxes)
    elif(prelim and not internal and not simulation):
        ax.text(x+shift, y,
                'Preliminary',
                fontdict=internal_fd,
                transform=ax.transAxes)
    elif(simulation and not internal and not prelim):
        ax.text(x+shift, y,
                'Simulation',
                fontdict=internal_fd,
                transform=ax.transAxes)
    elif(simulation and prelim and not internal):
        ax.text(x+shift, y,
                'Simulation Preliminary',
                fontdict=internal_fd,
                transform=ax.transAxes)
    elif(internal and prelim and not simulation):
        ax.text(x+shift, y,
                'Internal Preliminary',
                fontdict=internal_fd,
                transform=ax.transAxes)
    elif(internal and simulation and not prelim):
        ax.text(x+shift, y,
                'Simulation Internal',
                fontdict=internal_fd,
                transform=ax.transAxes)
    else:
        ax.text(x+shift, y,
                '',
                fontdict=internal_fd,
                transform=ax.transAxes)
    info_df = dict(size=24, **text_fd)
    ax.text(x, y-vshift,
            r'$\sqrt{s}=13$ TeV',
            fontdict=info_df,
            transform=ax.transAxes)

# ==================================================================
# Add the kinematic cuts info (preselection + pt_range + mass + efficiencies )
# ==================================================================
def add_presel_cuts(ax, x=0.05, y=0.05, pt_range=None, eff=None,
                    vshift=0.03, mass_in_presel=True, in_a_box=False):

    text_settings = dict(transform=ax.transAxes, size=27, ha='left', va='center')
    preamble = 'Preselection Cuts:'
    ax.text(x, y, preamble, **text_settings, weight='bold')
    y -= vshift
    cuts = [r'$N_{lep} = 1$', '$N_{bjets} \geq 3$', r'$MET > 25$ GeV']
    for cut in cuts:
        ax.text(x, y, cut, **text_settings)
        y -= vshift

    if(in_a_box):
        box_props = dict(boxstyle='round', facecolor='w')
        # Text settings
        text_settings = dict(transform=ax.transAxes, size=24,
                             ha='left', va='bottom', bbox=box_props)
        text = (
            r'$\sqrt{s} = 13\,{\rm TeV}$' + '\n'
            + 'Selection:\n'
            + eta + '\n'
            + mass + '\n'
            + pt_string + '\n')[:-1]
        ax.text(x, y, text, **text_settings)


# ==============================================
# ============= Axes settings =============
# ==============================================
def helvetify():
    """
    Load 'Helvetica' default font (may be Arial for now)
    """
    from matplotlib import rc
    # 'Comic Sans MS', 'Trebuchet MS' works, Arial works in png...
    rc('font', **{'family': 'sans-serif', 'sans-serif': ['Arial']})


def log_formatting(value, pos):
    """
    This is for use with the FuncFormatter.

    It writes 1 and 10 as plane numbers, everything else as exponential.
    """
    roundval = round(value)
    if roundval == 1:
        base = 1
        exp = ''
    elif roundval == 10:
        base = 10
        exp = ''
    else:
        base = 10
        exp = round(math.log(value, 10))
    return r'{}$^{{\mathdefault{{ {} }} }}$'.format(base, exp)


def xlabdic(fontsize=20):
    # options for the x axis
    return dict(ha='right', x=0.98, fontsize=fontsize)


def ylabdic(fontsize=20):
    # options for the y axis
    return dict(ha='right', y=0.98, fontsize=fontsize)


def log_style(ax):
    ax.set_yscale('log')
    ax.yaxis.set_major_formatter(FuncFormatter(log_formatting))
    y_minor = matplotlib.ticker.LogLocator(base = 10.0, subs = np.arange(1.0, 10.0) * 0.1, numticks = 10)
    ax.yaxis.set_minor_locator(y_minor)
    ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
    ax.grid(True, which='minor', axis='y', alpha=0.03)