//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Mar 29 16:15:46 2021 by ROOT version 6.22/06
// from TTree nominal_Loose/nominal_Loose
// found on file: ttbar_mc16ALL_AFII.root
//////////////////////////////////////////////////////////

#ifndef ttbar_mc16ALL_AFII_h
#define ttbar_mc16ALL_AFII_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"


class ttbar_mc16ALL_AFII {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   //Useful Vectors
   std::vector<string> region_names;
   std::vector<string> sample_names;
   std::vector<string> sample_names_latex;
// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   vector<float>   *leptons_pt;
   vector<float>   *leptons_eta;
   vector<float>   *leptons_phi;
   vector<float>   *leptons_e;
   vector<int>     *leptons_ID;
   vector<bool>    *leptons_tight;
   vector<int>     *leptons_truthIFFClass;
   vector<float>   *jets_pt;
   vector<float>   *jets_eta;
   vector<float>   *jets_phi;
   vector<float>   *jets_e;
   vector<int>     *jets_tagWeightBin_DL1r_Continuous;
   vector<float>   *bjets_pt;
   vector<float>   *bjets_eta;
   vector<float>   *bjets_phi;
   vector<float>   *bjets_e;
   vector<int>     *bjets_tagWeightBin_DL1r_Continuous;
   vector<float>   *nonbjets_pt;
   vector<float>   *nonbjets_eta;
   vector<float>   *nonbjets_phi;
   vector<float>   *nonbjets_e;
   vector<int>     *nonbjets_tagWeightBin_DL1r_Continuous;
   vector<float>   *fwdjets_pt;
   vector<float>   *fwdjets_eta;
   vector<float>   *fwdjets_phi;
   vector<float>   *fwdjets_e;
   vector<int>     *fwdjets_tagWeightBin_DL1r_Continuous;
   vector<int>     *jets_truthPartonLabel;
   vector<int>     *jets_truthflav;
   vector<int>     *bjets_truthPartonLabel;
   vector<int>     *bjets_truthflav;
   vector<int>     *nonbjets_truthPartonLabel;
   vector<int>     *nonbjets_truthflav;
   vector<int>     *fwdjets_truthPartonLabel;
   vector<int>     *fwdjets_truthflav;
   Float_t         Ht;
   Float_t         njets;
   Float_t         nalljets;
   Float_t         nbjets;
   Float_t         nfwdjets;
   Float_t         nnonbjets;
   Float_t         njets_CBT0;
   Float_t         njets_CBT123;
   Float_t         njets_CBT2;
   Float_t         njets_CBT3;
   Float_t         njets_CBT4;
   Float_t         njets_CBT5;
   vector<float>   *met_pz;
   Float_t         alljet_m;
   Float_t         bjets_maxeta;
   Float_t         foxWolfram_0_momentum;
   Float_t         foxWolfram_1_momentum;
   Float_t         foxWolfram_2_momentum;
   Float_t         foxWolfram_3_momentum;
   Float_t         foxWolfram_4_momentum;
   Float_t         aplanarity;
   Float_t         sphericity;
   Float_t         sphericity_t;
   Float_t         W1_pt;
   Float_t         W1_eta;
   Float_t         W1_phi;
   Float_t         W1_m;
   Float_t         W2_pt;
   Float_t         W2_eta;
   Float_t         W2_phi;
   Float_t         W2_m;
   Float_t         top1_pt;
   Float_t         top1_eta;
   Float_t         top1_phi;
   Float_t         top1_m;
   Float_t         top1_y;
   Float_t         top2_pt;
   Float_t         top2_eta;
   Float_t         top2_phi;
   Float_t         top2_m;
   Float_t         top2_y;
   Float_t         top_nohiggsbbreco_eta;
   Float_t         top_nohiggsbbreco_phi;
   Float_t         top_nohiggsbbreco_m;
   Float_t         top_nohiggsbbreco_y;
   Float_t         higgs_pt;
   Float_t         higgs_eta;
   Float_t         higgs_phi;
   Float_t         higgs_m;
   Float_t         higgs_maxptbb_pt;
   Float_t         higgs_maxptbb_eta;
   Float_t         higgs_maxptbb_phi;
   Float_t         higgs_maxptbb_m;
   Float_t         higgs_notoprecobb_pt;
   Float_t         higgs_notoprecobb_eta;
   Float_t         higgs_notoprecobb_phi;
   Float_t         higgs_notoprecobb_m;
   vector<float>   *tophiggs_chi2;
   Float_t         chi2_min;
   Float_t         chi2_min_bjet_m;
   Float_t         chi2_min_bjet_eta;
   Float_t         chi2_min_top_m;
   Float_t         chi2_min_top_eta;
   Float_t         chi2_min_top_pt;
   Float_t         chi2_min_top_phi;
   Float_t         chi2_min_higgs_m;
   Float_t         chi2_min_higgs_eta;
   Float_t         chi2_min_higgs_pt;
   Float_t         chi2_min_higgs_phi;
   Float_t         chi2_min_Imvmass_tH;
   Float_t         chi2_min_DeltaPhi_tH;
   Float_t         chi2_min_DeltaEta_tH;
   Float_t         chi2_min_bbnonbjet_m;
   Float_t         bbs_top_m;
   Float_t         higgs_bb_nonbjet_m;
   Float_t         higgs_bb_m;
   Float_t         nonbjet_pt;
   Float_t         nonbjet_eta;
   Float_t         higgs_1b_1nonbjet_m;
   Float_t         rapgap_maxptjet;
   Float_t         rapgap_top_fwdjet;
   Float_t         rapgap_higgs_fwdjet;
   Float_t         rapgap_topb_fwdjet;
   Float_t         rapgap_higgsb_fwdjet;
   Float_t         inv3jets;
   Float_t         chi2_min_toplep_m;
   Float_t         chi2_min_toplep_pt;
   Float_t         chi2_min_toplep_eta;
   Float_t         chi2_min_toplep_phi;
   Float_t         chi2_min_tophad_m;
   Float_t         chi2_min_tophad_pt;
   Float_t         chi2_min_tophad_eta;
   Float_t         chi2_min_tophad_phi;
   Float_t         chi2_min_Whad_m;
   Float_t         chi2_min_Whad_pt;
   Float_t         chi2_min_Whad_eta;
   Float_t         chi2_min_Whad_phi;
   Float_t         chi2_min_deltaRq1q2;
   Float_t         chi2_min_deltaR_Wq1q2;
   Float_t         n_tophad_jets_CBT0;
   Float_t         n_tophad_jets_CBT123;
   Float_t         n_tophad_jets_CBT4;
   Float_t         n_tophad_jets_CBT5;
   Float_t         n_nontophad_jets_CBT0;
   Float_t         n_nontophad_jets_CBT123;
   Float_t         n_nontophad_jets_CBT4;
   Float_t         n_nontophad_jets_CBT5;
   Float_t         chi2_min_ttl;
   Float_t         chi2_min_tophad_m_ttAll;
   Float_t         chi2_min_tophad_pt_ttAll;
   Float_t         chi2_min_tophad_eta_ttAll;
   Float_t         chi2_min_tophad_phi_ttAll;
   Float_t         chi2_min_Whad_m_ttAll;
   Float_t         chi2_min_Whad_pt_ttAll;
   Float_t         chi2_min_Whad_eta_ttAll;
   Float_t         chi2_min_Whad_phi_ttAll;
   Float_t         n_tophad_jets_CBT0_ttAll;
   Float_t         n_tophad_jets_CBT123_ttAll;
   Float_t         n_tophad_jets_CBT4_ttAll;
   Float_t         n_tophad_jets_CBT5_ttAll;
   Float_t         n_nontophad_jets_CBT0_ttAll;
   Float_t         n_nontophad_jets_CBT123_ttAll;
   Float_t         n_nontophad_jets_CBT4_ttAll;
   Float_t         n_nontophad_jets_CBT5_ttAll;
   Float_t         chi2_min_ttAll;
   Float_t         mtw;
   Float_t         mlb0;
   Float_t         mlb1;
   Float_t         mlb2;
   ULong64_t       totalEvents;
   Float_t         totalEventsWeighted;
   vector<float>   *mc_generator_weights_norm;
   vector<string>  *mc_generator_weights_name;
   Float_t         xsec_weight;
   vector<double>  *BDT;
   vector<double>  *foam;
   vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_tauSF;
   Float_t         weight_globalLeptonTriggerSF;
   Float_t         weight_bTagSF_DL1r_Continuous;
   Float_t         weight_jvt;
   Float_t         weight_forwardjvt;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   UInt_t          backgroundFlags;
   UInt_t          hasBadMuon;
   vector<float>   *tau_pt;
   vector<float>   *tau_eta;
   vector<float>   *tau_phi;
   vector<float>   *tau_charge;
   Float_t         met_met;
   Float_t         met_phi;
   Int_t           ejets_2015;
   Int_t           ejets_2016;
   Int_t           ejets_2017;
   Int_t           ejets_2018;
   Int_t           mujets_2015;
   Int_t           mujets_2016;
   Int_t           mujets_2017;
   Int_t           mujets_2018;
   UInt_t          lbn;
   Float_t         Vtxz;
   UInt_t          npVtx;
   Int_t           HF_Classification;
   Int_t           HF_SimpleClassification;
   Int_t           HF_ClassificationGhost;
   Int_t           HF_SimpleClassificationGhost;
   Int_t           ttbb_categories;
   Int_t           nPDFFlavor;
   vector<float>   *tau_nTrack;
   vector<float>   *tau_tight;
   vector<float>   *tau_RNNScore;
   vector<float>   *tau_BDTScore;
   vector<float>   *tau_JetCaloWidth;
   vector<int>     *tau_true_pdg;
   vector<float>   *tau_true_pt;
   vector<float>   *tau_true_eta;
   Float_t         met_px;
   Float_t         met_py;
   Float_t         met_sumet;

   // List of branches
   TBranch        *b_leptons_pt;   //!
   TBranch        *b_leptons_eta;   //!
   TBranch        *b_leptons_phi;   //!
   TBranch        *b_leptons_e;   //!
   TBranch        *b_leptons_ID;   //!
   TBranch        *b_leptons_tight;   //!
   TBranch        *b_leptons_truthIFFClass;   //!
   TBranch        *b_jets_pt;   //!
   TBranch        *b_jets_eta;   //!
   TBranch        *b_jets_phi;   //!
   TBranch        *b_jets_e;   //!
   TBranch        *b_jets_tagWeightBin_DL1r_Continuous;   //!
   TBranch        *b_bjets_pt;   //!
   TBranch        *b_bjets_eta;   //!
   TBranch        *b_bjets_phi;   //!
   TBranch        *b_bjets_e;   //!
   TBranch        *b_bjets_tagWeightBin_DL1r_Continuous;   //!
   TBranch        *b_nonbjets_pt;   //!
   TBranch        *b_nonbjets_eta;   //!
   TBranch        *b_nonbjets_phi;   //!
   TBranch        *b_nonbjets_e;   //!
   TBranch        *b_nonbjets_tagWeightBin_DL1r_Continuous;   //!
   TBranch        *b_fwdjets_pt;   //!
   TBranch        *b_fwdjets_eta;   //!
   TBranch        *b_fwdjets_phi;   //!
   TBranch        *b_fwdjets_e;   //!
   TBranch        *b_fwdjets_tagWeightBin_DL1r_Continuous;   //!
   TBranch        *b_jets_truthPartonLabel;   //!
   TBranch        *b_jets_truthflav;   //!
   TBranch        *b_bjets_truthPartonLabel;   //!
   TBranch        *b_bjets_truthflav;   //!
   TBranch        *b_nonbjets_truthPartonLabel;   //!
   TBranch        *b_nonbjets_truthflav;   //!
   TBranch        *b_fwdjets_truthPartonLabel;   //!
   TBranch        *b_fwdjets_truthflav;   //!
   TBranch        *b_Ht;   //!
   TBranch        *b_njets;   //!
   TBranch        *b_nalljets;   //!
   TBranch        *b_nbjets;   //!
   TBranch        *b_nfwdjets;   //!
   TBranch        *b_nnonbjets;   //!
   TBranch        *b_njets_CBT0;   //!
   TBranch        *b_njets_CBT123;   //!
   TBranch        *b_njets_CBT2;   //!
   TBranch        *b_njets_CBT3;   //!
   TBranch        *b_njets_CBT4;   //!
   TBranch        *b_njets_CBT5;   //!
   TBranch        *b_met_pz;   //!
   TBranch        *b_alljet_m;   //!
   TBranch        *b_bjets_maxeta;   //!
   TBranch        *b_foxWolfram_0_momentum;   //!
   TBranch        *b_foxWolfram_1_momentum;   //!
   TBranch        *b_foxWolfram_2_momentum;   //!
   TBranch        *b_foxWolfram_3_momentum;   //!
   TBranch        *b_foxWolfram_4_momentum;   //!
   TBranch        *b_aplanarity;   //!
   TBranch        *b_sphericity;   //!
   TBranch        *b_sphericity_t;   //!
   TBranch        *b_W1_pt;   //!
   TBranch        *b_W1_eta;   //!
   TBranch        *b_W1_phi;   //!
   TBranch        *b_W1_m;   //!
   TBranch        *b_W2_pt;   //!
   TBranch        *b_W2_eta;   //!
   TBranch        *b_W2_phi;   //!
   TBranch        *b_W2_m;   //!
   TBranch        *b_top1_pt;   //!
   TBranch        *b_top1_eta;   //!
   TBranch        *b_top1_phi;   //!
   TBranch        *b_top1_m;   //!
   TBranch        *b_top1_y;   //!
   TBranch        *b_top2_pt;   //!
   TBranch        *b_top2_eta;   //!
   TBranch        *b_top2_phi;   //!
   TBranch        *b_top2_m;   //!
   TBranch        *b_top2_y;   //!
   TBranch        *b_top_nohiggsbbreco_eta;   //!
   TBranch        *b_top_nohiggsbbreco_phi;   //!
   TBranch        *b_top_nohiggsbbreco_m;   //!
   TBranch        *b_top_nohiggsbbreco_y;   //!
   TBranch        *b_higgs_pt;   //!
   TBranch        *b_higgs_eta;   //!
   TBranch        *b_higgs_phi;   //!
   TBranch        *b_higgs_m;   //!
   TBranch        *b_higgs_maxptbb_pt;   //!
   TBranch        *b_higgs_maxptbb_eta;   //!
   TBranch        *b_higgs_maxptbb_phi;   //!
   TBranch        *b_higgs_maxptbb_m;   //!
   TBranch        *b_higgs_notoprecobb_pt;   //!
   TBranch        *b_higgs_notoprecobb_eta;   //!
   TBranch        *b_higgs_notoprecobb_phi;   //!
   TBranch        *b_higgs_notoprecobb_m;   //!
   TBranch        *b_tophiggs_chi2;   //!
   TBranch        *b_chi2_min;   //!
   TBranch        *b_chi2_min_bjet_m;   //!
   TBranch        *b_chi2_min_bjet_eta;   //!
   TBranch        *b_chi2_min_top_m;   //!
   TBranch        *b_chi2_min_top_eta;   //!
   TBranch        *b_chi2_min_top_pt;   //!
   TBranch        *b_chi2_min_top_phi;   //!
   TBranch        *b_chi2_min_higgs_m;   //!
   TBranch        *b_chi2_min_higgs_eta;   //!
   TBranch        *b_chi2_min_higgs_pt;   //!
   TBranch        *b_chi2_min_higgs_phi;   //!
   TBranch        *b_chi2_min_Imvmass_tH;   //!
   TBranch        *b_chi2_min_DeltaPhi_tH;   //!
   TBranch        *b_chi2_min_DeltaEta_tH;   //!
   TBranch        *b_chi2_min_bbnonbjet_m;   //!
   TBranch        *b_bbs_top_m;   //!
   TBranch        *b_higgs_bb_nonbjet_m;   //!
   TBranch        *b_higgs_bb_m;   //!
   TBranch        *b_nonbjet_pt;   //!
   TBranch        *b_nonbjet_eta;   //!
   TBranch        *b_higgs_1b_1nonbjet_m;   //!
   TBranch        *b_rapgap_maxptjet;   //!
   TBranch        *b_rapgap_top_fwdjet;   //!
   TBranch        *b_rapgap_higgs_fwdjet;   //!
   TBranch        *b_rapgap_topb_fwdjet;   //!
   TBranch        *b_rapgap_higgsb_fwdjet;   //!
   TBranch        *b_inv3jets;   //!
   TBranch        *b_chi2_min_toplep_m;   //!
   TBranch        *b_chi2_min_toplep_pt;   //!
   TBranch        *b_chi2_min_toplep_eta;   //!
   TBranch        *b_chi2_min_toplep_phi;   //!
   TBranch        *b_chi2_min_tophad_m;   //!
   TBranch        *b_chi2_min_tophad_pt;   //!
   TBranch        *b_chi2_min_tophad_eta;   //!
   TBranch        *b_chi2_min_tophad_phi;   //!
   TBranch        *b_chi2_min_Whad_m;   //!
   TBranch        *b_chi2_min_Whad_pt;   //!
   TBranch        *b_chi2_min_Whad_eta;   //!
   TBranch        *b_chi2_min_Whad_phi;   //!
   TBranch        *b_chi2_min_deltaRq1q2;   //!
   TBranch        *b_chi2_min_deltaR_Wq1q2;   //!
   TBranch        *b_n_tophad_jets_CBT0;   //!
   TBranch        *b_n_tophad_jets_CBT123;   //!
   TBranch        *b_n_tophad_jets_CBT4;   //!
   TBranch        *b_n_tophad_jets_CBT5;   //!
   TBranch        *b_n_nontophad_jets_CBT0;   //!
   TBranch        *b_n_nontophad_jets_CBT123;   //!
   TBranch        *b_n_nontophad_jets_CBT4;   //!
   TBranch        *b_n_nontophad_jets_CBT5;   //!
   TBranch        *b_chi2_min_ttl;   //!
   TBranch        *b_chi2_min_tophad_m_ttAll;   //!
   TBranch        *b_chi2_min_tophad_pt_ttAll;   //!
   TBranch        *b_chi2_min_tophad_eta_ttAll;   //!
   TBranch        *b_chi2_min_tophad_phi_ttAll;   //!
   TBranch        *b_chi2_min_Whad_m_ttAll;   //!
   TBranch        *b_chi2_min_Whad_pt_ttAll;   //!
   TBranch        *b_chi2_min_Whad_eta_ttAll;   //!
   TBranch        *b_chi2_min_Whad_phi_ttAll;   //!
   TBranch        *b_n_tophad_jets_CBT0_ttAll;   //!
   TBranch        *b_n_tophad_jets_CBT123_ttAll;   //!
   TBranch        *b_n_tophad_jets_CBT4_ttAll;   //!
   TBranch        *b_n_tophad_jets_CBT5_ttAll;   //!
   TBranch        *b_n_nontophad_jets_CBT0_ttAll;   //!
   TBranch        *b_n_nontophad_jets_CBT123_ttAll;   //!
   TBranch        *b_n_nontophad_jets_CBT4_ttAll;   //!
   TBranch        *b_n_nontophad_jets_CBT5_ttAll;   //!
   TBranch        *b_chi2_min_ttAll;   //!
   TBranch        *b_mtw;   //!
   TBranch        *b_mlb0;   //!
   TBranch        *b_mlb1;   //!
   TBranch        *b_mlb2;   //!
   TBranch        *b_totalEvents;   //!
   TBranch        *b_totalEventsWeighted;   //!
   TBranch        *b_mc_generator_weights_norm;   //!
   TBranch        *b_mc_generator_weights_name;   //!
   TBranch        *b_xsec_weight;   //!
   TBranch        *b_BDT;   //!
   TBranch        *b_foam;   //!
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_tauSF;   //!
   TBranch        *b_weight_globalLeptonTriggerSF;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_forwardjvt;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_hasBadMuon;   //!
   TBranch        *b_tau_pt;   //!
   TBranch        *b_tau_eta;   //!
   TBranch        *b_tau_phi;   //!
   TBranch        *b_tau_charge;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_ejets_2015;   //!
   TBranch        *b_ejets_2016;   //!
   TBranch        *b_ejets_2017;   //!
   TBranch        *b_ejets_2018;   //!
   TBranch        *b_mujets_2015;   //!
   TBranch        *b_mujets_2016;   //!
   TBranch        *b_mujets_2017;   //!
   TBranch        *b_mujets_2018;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_Vtxz;   //!
   TBranch        *b_npVtx;   //!
   TBranch        *b_HF_Classification;   //!
   TBranch        *b_HF_SimpleClassification;   //!
   TBranch        *b_HF_ClassificationGhost;   //!
   TBranch        *b_HF_SimpleClassificationGhost;   //!
   TBranch        *b_ttbb_categories;   //!
   TBranch        *b_nPDFFlavor;   //!
   TBranch        *b_tau_nTrack;   //!
   TBranch        *b_tau_tight;   //!
   TBranch        *b_tau_RNNScore;   //!
   TBranch        *b_tau_BDTScore;   //!
   TBranch        *b_tau_JetCaloWidth;   //!
   TBranch        *b_tau_true_pdg;   //!
   TBranch        *b_tau_true_pt;   //!
   TBranch        *b_tau_true_eta;   //!
   TBranch        *b_met_px;   //!
   TBranch        *b_met_py;   //!
   TBranch        *b_met_sumet;   //!

   ttbar_mc16ALL_AFII(TTree *tree=0);
   virtual ~ttbar_mc16ALL_AFII();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   virtual void               Draw_StackedHistos_FromRootFile(string& file_name, std::vector<string> &legend_entries, string& region_name,string& var_name_latex);
   virtual const char*        string_to_char(string& str);
};

#endif

#ifdef ttbar_mc16ALL_AFII_cxx
ttbar_mc16ALL_AFII::ttbar_mc16ALL_AFII(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("ttbar_mc16ALL_AFII.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("ttbar_mc16ALL_AFII.root");
      }
      f->GetObject("nominal_Loose",tree);

   }
   Init(tree);
}

ttbar_mc16ALL_AFII::~ttbar_mc16ALL_AFII()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ttbar_mc16ALL_AFII::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ttbar_mc16ALL_AFII::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ttbar_mc16ALL_AFII::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   leptons_pt = 0;
   leptons_eta = 0;
   leptons_phi = 0;
   leptons_e = 0;
   leptons_ID = 0;
   leptons_tight = 0;
   leptons_truthIFFClass = 0;
   jets_pt = 0;
   jets_eta = 0;
   jets_phi = 0;
   jets_e = 0;
   jets_tagWeightBin_DL1r_Continuous = 0;
   bjets_pt = 0;
   bjets_eta = 0;
   bjets_phi = 0;
   bjets_e = 0;
   bjets_tagWeightBin_DL1r_Continuous = 0;
   nonbjets_pt = 0;
   nonbjets_eta = 0;
   nonbjets_phi = 0;
   nonbjets_e = 0;
   nonbjets_tagWeightBin_DL1r_Continuous = 0;
   fwdjets_pt = 0;
   fwdjets_eta = 0;
   fwdjets_phi = 0;
   fwdjets_e = 0;
   fwdjets_tagWeightBin_DL1r_Continuous = 0;
   jets_truthPartonLabel = 0;
   jets_truthflav = 0;
   bjets_truthPartonLabel = 0;
   bjets_truthflav = 0;
   nonbjets_truthPartonLabel = 0;
   nonbjets_truthflav = 0;
   fwdjets_truthPartonLabel = 0;
   fwdjets_truthflav = 0;
   met_pz = 0;
   tophiggs_chi2 = 0;
   mc_generator_weights_norm = 0;
   mc_generator_weights_name = 0;
   BDT = 0;
   foam = 0;
   mc_generator_weights = 0;
   tau_pt = 0;
   tau_eta = 0;
   tau_phi = 0;
   tau_charge = 0;
   tau_nTrack = 0;
   tau_tight = 0;
   tau_RNNScore = 0;
   tau_BDTScore = 0;
   tau_JetCaloWidth = 0;
   tau_true_pdg = 0;
   tau_true_pt = 0;
   tau_true_eta = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("leptons_pt", &leptons_pt, &b_leptons_pt);
   fChain->SetBranchAddress("leptons_eta", &leptons_eta, &b_leptons_eta);
   fChain->SetBranchAddress("leptons_phi", &leptons_phi, &b_leptons_phi);
   fChain->SetBranchAddress("leptons_e", &leptons_e, &b_leptons_e);
   fChain->SetBranchAddress("leptons_ID", &leptons_ID, &b_leptons_ID);
   fChain->SetBranchAddress("leptons_tight", &leptons_tight, &b_leptons_tight);
   fChain->SetBranchAddress("leptons_truthIFFClass", &leptons_truthIFFClass, &b_leptons_truthIFFClass);
   fChain->SetBranchAddress("jets_pt", &jets_pt, &b_jets_pt);
   fChain->SetBranchAddress("jets_eta", &jets_eta, &b_jets_eta);
   fChain->SetBranchAddress("jets_phi", &jets_phi, &b_jets_phi);
   fChain->SetBranchAddress("jets_e", &jets_e, &b_jets_e);
   fChain->SetBranchAddress("jets_tagWeightBin_DL1r_Continuous", &jets_tagWeightBin_DL1r_Continuous, &b_jets_tagWeightBin_DL1r_Continuous);
   fChain->SetBranchAddress("bjets_pt", &bjets_pt, &b_bjets_pt);
   fChain->SetBranchAddress("bjets_eta", &bjets_eta, &b_bjets_eta);
   fChain->SetBranchAddress("bjets_phi", &bjets_phi, &b_bjets_phi);
   fChain->SetBranchAddress("bjets_e", &bjets_e, &b_bjets_e);
   fChain->SetBranchAddress("bjets_tagWeightBin_DL1r_Continuous", &bjets_tagWeightBin_DL1r_Continuous, &b_bjets_tagWeightBin_DL1r_Continuous);
   fChain->SetBranchAddress("nonbjets_pt", &nonbjets_pt, &b_nonbjets_pt);
   fChain->SetBranchAddress("nonbjets_eta", &nonbjets_eta, &b_nonbjets_eta);
   fChain->SetBranchAddress("nonbjets_phi", &nonbjets_phi, &b_nonbjets_phi);
   fChain->SetBranchAddress("nonbjets_e", &nonbjets_e, &b_nonbjets_e);
   fChain->SetBranchAddress("nonbjets_tagWeightBin_DL1r_Continuous", &nonbjets_tagWeightBin_DL1r_Continuous, &b_nonbjets_tagWeightBin_DL1r_Continuous);
   fChain->SetBranchAddress("fwdjets_pt", &fwdjets_pt, &b_fwdjets_pt);
   fChain->SetBranchAddress("fwdjets_eta", &fwdjets_eta, &b_fwdjets_eta);
   fChain->SetBranchAddress("fwdjets_phi", &fwdjets_phi, &b_fwdjets_phi);
   fChain->SetBranchAddress("fwdjets_e", &fwdjets_e, &b_fwdjets_e);
   fChain->SetBranchAddress("fwdjets_tagWeightBin_DL1r_Continuous", &fwdjets_tagWeightBin_DL1r_Continuous, &b_fwdjets_tagWeightBin_DL1r_Continuous);
   fChain->SetBranchAddress("jets_truthPartonLabel", &jets_truthPartonLabel, &b_jets_truthPartonLabel);
   fChain->SetBranchAddress("jets_truthflav", &jets_truthflav, &b_jets_truthflav);
   fChain->SetBranchAddress("bjets_truthPartonLabel", &bjets_truthPartonLabel, &b_bjets_truthPartonLabel);
   fChain->SetBranchAddress("bjets_truthflav", &bjets_truthflav, &b_bjets_truthflav);
   fChain->SetBranchAddress("nonbjets_truthPartonLabel", &nonbjets_truthPartonLabel, &b_nonbjets_truthPartonLabel);
   fChain->SetBranchAddress("nonbjets_truthflav", &nonbjets_truthflav, &b_nonbjets_truthflav);
   fChain->SetBranchAddress("fwdjets_truthPartonLabel", &fwdjets_truthPartonLabel, &b_fwdjets_truthPartonLabel);
   fChain->SetBranchAddress("fwdjets_truthflav", &fwdjets_truthflav, &b_fwdjets_truthflav);
   fChain->SetBranchAddress("Ht", &Ht, &b_Ht);
   fChain->SetBranchAddress("njets", &njets, &b_njets);
   fChain->SetBranchAddress("nalljets", &nalljets, &b_nalljets);
   fChain->SetBranchAddress("nbjets", &nbjets, &b_nbjets);
   fChain->SetBranchAddress("nfwdjets", &nfwdjets, &b_nfwdjets);
   fChain->SetBranchAddress("nnonbjets", &nnonbjets, &b_nnonbjets);
   fChain->SetBranchAddress("njets_CBT0", &njets_CBT0, &b_njets_CBT0);
   fChain->SetBranchAddress("njets_CBT123", &njets_CBT123, &b_njets_CBT123);
   fChain->SetBranchAddress("njets_CBT2", &njets_CBT2, &b_njets_CBT2);
   fChain->SetBranchAddress("njets_CBT3", &njets_CBT3, &b_njets_CBT3);
   fChain->SetBranchAddress("njets_CBT4", &njets_CBT4, &b_njets_CBT4);
   fChain->SetBranchAddress("njets_CBT5", &njets_CBT5, &b_njets_CBT5);
   fChain->SetBranchAddress("met_pz", &met_pz, &b_met_pz);
   fChain->SetBranchAddress("alljet_m", &alljet_m, &b_alljet_m);
   fChain->SetBranchAddress("bjets_maxeta", &bjets_maxeta, &b_bjets_maxeta);
   fChain->SetBranchAddress("foxWolfram_0_momentum", &foxWolfram_0_momentum, &b_foxWolfram_0_momentum);
   fChain->SetBranchAddress("foxWolfram_1_momentum", &foxWolfram_1_momentum, &b_foxWolfram_1_momentum);
   fChain->SetBranchAddress("foxWolfram_2_momentum", &foxWolfram_2_momentum, &b_foxWolfram_2_momentum);
   fChain->SetBranchAddress("foxWolfram_3_momentum", &foxWolfram_3_momentum, &b_foxWolfram_3_momentum);
   fChain->SetBranchAddress("foxWolfram_4_momentum", &foxWolfram_4_momentum, &b_foxWolfram_4_momentum);
   fChain->SetBranchAddress("aplanarity", &aplanarity, &b_aplanarity);
   fChain->SetBranchAddress("sphericity", &sphericity, &b_sphericity);
   fChain->SetBranchAddress("sphericity_t", &sphericity_t, &b_sphericity_t);
   fChain->SetBranchAddress("W1_pt", &W1_pt, &b_W1_pt);
   fChain->SetBranchAddress("W1_eta", &W1_eta, &b_W1_eta);
   fChain->SetBranchAddress("W1_phi", &W1_phi, &b_W1_phi);
   fChain->SetBranchAddress("W1_m", &W1_m, &b_W1_m);
   fChain->SetBranchAddress("W2_pt", &W2_pt, &b_W2_pt);
   fChain->SetBranchAddress("W2_eta", &W2_eta, &b_W2_eta);
   fChain->SetBranchAddress("W2_phi", &W2_phi, &b_W2_phi);
   fChain->SetBranchAddress("W2_m", &W2_m, &b_W2_m);
   fChain->SetBranchAddress("top1_pt", &top1_pt, &b_top1_pt);
   fChain->SetBranchAddress("top1_eta", &top1_eta, &b_top1_eta);
   fChain->SetBranchAddress("top1_phi", &top1_phi, &b_top1_phi);
   fChain->SetBranchAddress("top1_m", &top1_m, &b_top1_m);
   fChain->SetBranchAddress("top1_y", &top1_y, &b_top1_y);
   fChain->SetBranchAddress("top2_pt", &top2_pt, &b_top2_pt);
   fChain->SetBranchAddress("top2_eta", &top2_eta, &b_top2_eta);
   fChain->SetBranchAddress("top2_phi", &top2_phi, &b_top2_phi);
   fChain->SetBranchAddress("top2_m", &top2_m, &b_top2_m);
   fChain->SetBranchAddress("top2_y", &top2_y, &b_top2_y);
   fChain->SetBranchAddress("top_nohiggsbbreco_eta", &top_nohiggsbbreco_eta, &b_top_nohiggsbbreco_eta);
   fChain->SetBranchAddress("top_nohiggsbbreco_phi", &top_nohiggsbbreco_phi, &b_top_nohiggsbbreco_phi);
   fChain->SetBranchAddress("top_nohiggsbbreco_m", &top_nohiggsbbreco_m, &b_top_nohiggsbbreco_m);
   fChain->SetBranchAddress("top_nohiggsbbreco_y", &top_nohiggsbbreco_y, &b_top_nohiggsbbreco_y);
   fChain->SetBranchAddress("higgs_pt", &higgs_pt, &b_higgs_pt);
   fChain->SetBranchAddress("higgs_eta", &higgs_eta, &b_higgs_eta);
   fChain->SetBranchAddress("higgs_phi", &higgs_phi, &b_higgs_phi);
   fChain->SetBranchAddress("higgs_m", &higgs_m, &b_higgs_m);
   fChain->SetBranchAddress("higgs_maxptbb_pt", &higgs_maxptbb_pt, &b_higgs_maxptbb_pt);
   fChain->SetBranchAddress("higgs_maxptbb_eta", &higgs_maxptbb_eta, &b_higgs_maxptbb_eta);
   fChain->SetBranchAddress("higgs_maxptbb_phi", &higgs_maxptbb_phi, &b_higgs_maxptbb_phi);
   fChain->SetBranchAddress("higgs_maxptbb_m", &higgs_maxptbb_m, &b_higgs_maxptbb_m);
   fChain->SetBranchAddress("higgs_notoprecobb_pt", &higgs_notoprecobb_pt, &b_higgs_notoprecobb_pt);
   fChain->SetBranchAddress("higgs_notoprecobb_eta", &higgs_notoprecobb_eta, &b_higgs_notoprecobb_eta);
   fChain->SetBranchAddress("higgs_notoprecobb_phi", &higgs_notoprecobb_phi, &b_higgs_notoprecobb_phi);
   fChain->SetBranchAddress("higgs_notoprecobb_m", &higgs_notoprecobb_m, &b_higgs_notoprecobb_m);
   fChain->SetBranchAddress("tophiggs_chi2", &tophiggs_chi2, &b_tophiggs_chi2);
   fChain->SetBranchAddress("chi2_min", &chi2_min, &b_chi2_min);
   fChain->SetBranchAddress("chi2_min_bjet_m", &chi2_min_bjet_m, &b_chi2_min_bjet_m);
   fChain->SetBranchAddress("chi2_min_bjet_eta", &chi2_min_bjet_eta, &b_chi2_min_bjet_eta);
   fChain->SetBranchAddress("chi2_min_top_m", &chi2_min_top_m, &b_chi2_min_top_m);
   fChain->SetBranchAddress("chi2_min_top_eta", &chi2_min_top_eta, &b_chi2_min_top_eta);
   fChain->SetBranchAddress("chi2_min_top_pt", &chi2_min_top_pt, &b_chi2_min_top_pt);
   fChain->SetBranchAddress("chi2_min_top_phi", &chi2_min_top_phi, &b_chi2_min_top_phi);
   fChain->SetBranchAddress("chi2_min_higgs_m", &chi2_min_higgs_m, &b_chi2_min_higgs_m);
   fChain->SetBranchAddress("chi2_min_higgs_eta", &chi2_min_higgs_eta, &b_chi2_min_higgs_eta);
   fChain->SetBranchAddress("chi2_min_higgs_pt", &chi2_min_higgs_pt, &b_chi2_min_higgs_pt);
   fChain->SetBranchAddress("chi2_min_higgs_phi", &chi2_min_higgs_phi, &b_chi2_min_higgs_phi);
   fChain->SetBranchAddress("chi2_min_Imvmass_tH", &chi2_min_Imvmass_tH, &b_chi2_min_Imvmass_tH);
   fChain->SetBranchAddress("chi2_min_DeltaPhi_tH", &chi2_min_DeltaPhi_tH, &b_chi2_min_DeltaPhi_tH);
   fChain->SetBranchAddress("chi2_min_DeltaEta_tH", &chi2_min_DeltaEta_tH, &b_chi2_min_DeltaEta_tH);
   fChain->SetBranchAddress("chi2_min_bbnonbjet_m", &chi2_min_bbnonbjet_m, &b_chi2_min_bbnonbjet_m);
   fChain->SetBranchAddress("bbs_top_m", &bbs_top_m, &b_bbs_top_m);
   fChain->SetBranchAddress("higgs_bb_nonbjet_m", &higgs_bb_nonbjet_m, &b_higgs_bb_nonbjet_m);
   fChain->SetBranchAddress("higgs_bb_m", &higgs_bb_m, &b_higgs_bb_m);
   fChain->SetBranchAddress("nonbjet_pt", &nonbjet_pt, &b_nonbjet_pt);
   fChain->SetBranchAddress("nonbjet_eta", &nonbjet_eta, &b_nonbjet_eta);
   fChain->SetBranchAddress("higgs_1b_1nonbjet_m", &higgs_1b_1nonbjet_m, &b_higgs_1b_1nonbjet_m);
   fChain->SetBranchAddress("rapgap_maxptjet", &rapgap_maxptjet, &b_rapgap_maxptjet);
   fChain->SetBranchAddress("rapgap_top_fwdjet", &rapgap_top_fwdjet, &b_rapgap_top_fwdjet);
   fChain->SetBranchAddress("rapgap_higgs_fwdjet", &rapgap_higgs_fwdjet, &b_rapgap_higgs_fwdjet);
   fChain->SetBranchAddress("rapgap_topb_fwdjet", &rapgap_topb_fwdjet, &b_rapgap_topb_fwdjet);
   fChain->SetBranchAddress("rapgap_higgsb_fwdjet", &rapgap_higgsb_fwdjet, &b_rapgap_higgsb_fwdjet);
   fChain->SetBranchAddress("inv3jets", &inv3jets, &b_inv3jets);
   fChain->SetBranchAddress("chi2_min_toplep_m", &chi2_min_toplep_m, &b_chi2_min_toplep_m);
   fChain->SetBranchAddress("chi2_min_toplep_pt", &chi2_min_toplep_pt, &b_chi2_min_toplep_pt);
   fChain->SetBranchAddress("chi2_min_toplep_eta", &chi2_min_toplep_eta, &b_chi2_min_toplep_eta);
   fChain->SetBranchAddress("chi2_min_toplep_phi", &chi2_min_toplep_phi, &b_chi2_min_toplep_phi);
   fChain->SetBranchAddress("chi2_min_tophad_m", &chi2_min_tophad_m, &b_chi2_min_tophad_m);
   fChain->SetBranchAddress("chi2_min_tophad_pt", &chi2_min_tophad_pt, &b_chi2_min_tophad_pt);
   fChain->SetBranchAddress("chi2_min_tophad_eta", &chi2_min_tophad_eta, &b_chi2_min_tophad_eta);
   fChain->SetBranchAddress("chi2_min_tophad_phi", &chi2_min_tophad_phi, &b_chi2_min_tophad_phi);
   fChain->SetBranchAddress("chi2_min_Whad_m", &chi2_min_Whad_m, &b_chi2_min_Whad_m);
   fChain->SetBranchAddress("chi2_min_Whad_pt", &chi2_min_Whad_pt, &b_chi2_min_Whad_pt);
   fChain->SetBranchAddress("chi2_min_Whad_eta", &chi2_min_Whad_eta, &b_chi2_min_Whad_eta);
   fChain->SetBranchAddress("chi2_min_Whad_phi", &chi2_min_Whad_phi, &b_chi2_min_Whad_phi);
   fChain->SetBranchAddress("chi2_min_deltaRq1q2", &chi2_min_deltaRq1q2, &b_chi2_min_deltaRq1q2);
   fChain->SetBranchAddress("chi2_min_deltaR_Wq1q2", &chi2_min_deltaR_Wq1q2, &b_chi2_min_deltaR_Wq1q2);
   fChain->SetBranchAddress("n_tophad_jets_CBT0", &n_tophad_jets_CBT0, &b_n_tophad_jets_CBT0);
   fChain->SetBranchAddress("n_tophad_jets_CBT123", &n_tophad_jets_CBT123, &b_n_tophad_jets_CBT123);
   fChain->SetBranchAddress("n_tophad_jets_CBT4", &n_tophad_jets_CBT4, &b_n_tophad_jets_CBT4);
   fChain->SetBranchAddress("n_tophad_jets_CBT5", &n_tophad_jets_CBT5, &b_n_tophad_jets_CBT5);
   fChain->SetBranchAddress("n_nontophad_jets_CBT0", &n_nontophad_jets_CBT0, &b_n_nontophad_jets_CBT0);
   fChain->SetBranchAddress("n_nontophad_jets_CBT123", &n_nontophad_jets_CBT123, &b_n_nontophad_jets_CBT123);
   fChain->SetBranchAddress("n_nontophad_jets_CBT4", &n_nontophad_jets_CBT4, &b_n_nontophad_jets_CBT4);
   fChain->SetBranchAddress("n_nontophad_jets_CBT5", &n_nontophad_jets_CBT5, &b_n_nontophad_jets_CBT5);
   fChain->SetBranchAddress("chi2_min_ttl", &chi2_min_ttl, &b_chi2_min_ttl);
   fChain->SetBranchAddress("chi2_min_tophad_m_ttAll", &chi2_min_tophad_m_ttAll, &b_chi2_min_tophad_m_ttAll);
   fChain->SetBranchAddress("chi2_min_tophad_pt_ttAll", &chi2_min_tophad_pt_ttAll, &b_chi2_min_tophad_pt_ttAll);
   fChain->SetBranchAddress("chi2_min_tophad_eta_ttAll", &chi2_min_tophad_eta_ttAll, &b_chi2_min_tophad_eta_ttAll);
   fChain->SetBranchAddress("chi2_min_tophad_phi_ttAll", &chi2_min_tophad_phi_ttAll, &b_chi2_min_tophad_phi_ttAll);
   fChain->SetBranchAddress("chi2_min_Whad_m_ttAll", &chi2_min_Whad_m_ttAll, &b_chi2_min_Whad_m_ttAll);
   fChain->SetBranchAddress("chi2_min_Whad_pt_ttAll", &chi2_min_Whad_pt_ttAll, &b_chi2_min_Whad_pt_ttAll);
   fChain->SetBranchAddress("chi2_min_Whad_eta_ttAll", &chi2_min_Whad_eta_ttAll, &b_chi2_min_Whad_eta_ttAll);
   fChain->SetBranchAddress("chi2_min_Whad_phi_ttAll", &chi2_min_Whad_phi_ttAll, &b_chi2_min_Whad_phi_ttAll);
   fChain->SetBranchAddress("n_tophad_jets_CBT0_ttAll", &n_tophad_jets_CBT0_ttAll, &b_n_tophad_jets_CBT0_ttAll);
   fChain->SetBranchAddress("n_tophad_jets_CBT123_ttAll", &n_tophad_jets_CBT123_ttAll, &b_n_tophad_jets_CBT123_ttAll);
   fChain->SetBranchAddress("n_tophad_jets_CBT4_ttAll", &n_tophad_jets_CBT4_ttAll, &b_n_tophad_jets_CBT4_ttAll);
   fChain->SetBranchAddress("n_tophad_jets_CBT5_ttAll", &n_tophad_jets_CBT5_ttAll, &b_n_tophad_jets_CBT5_ttAll);
   fChain->SetBranchAddress("n_nontophad_jets_CBT0_ttAll", &n_nontophad_jets_CBT0_ttAll, &b_n_nontophad_jets_CBT0_ttAll);
   fChain->SetBranchAddress("n_nontophad_jets_CBT123_ttAll", &n_nontophad_jets_CBT123_ttAll, &b_n_nontophad_jets_CBT123_ttAll);
   fChain->SetBranchAddress("n_nontophad_jets_CBT4_ttAll", &n_nontophad_jets_CBT4_ttAll, &b_n_nontophad_jets_CBT4_ttAll);
   fChain->SetBranchAddress("n_nontophad_jets_CBT5_ttAll", &n_nontophad_jets_CBT5_ttAll, &b_n_nontophad_jets_CBT5_ttAll);
   fChain->SetBranchAddress("chi2_min_ttAll", &chi2_min_ttAll, &b_chi2_min_ttAll);
   fChain->SetBranchAddress("mtw", &mtw, &b_mtw);
   fChain->SetBranchAddress("mlb0", &mlb0, &b_mlb0);
   fChain->SetBranchAddress("mlb1", &mlb1, &b_mlb1);
   fChain->SetBranchAddress("mlb2", &mlb2, &b_mlb2);
   fChain->SetBranchAddress("totalEvents", &totalEvents, &b_totalEvents);
   fChain->SetBranchAddress("totalEventsWeighted", &totalEventsWeighted, &b_totalEventsWeighted);
   fChain->SetBranchAddress("mc_generator_weights_norm", &mc_generator_weights_norm, &b_mc_generator_weights_norm);
   fChain->SetBranchAddress("mc_generator_weights_name", &mc_generator_weights_name, &b_mc_generator_weights_name);
   fChain->SetBranchAddress("xsec_weight", &xsec_weight, &b_xsec_weight);
   fChain->SetBranchAddress("BDT", &BDT, &b_BDT);
   fChain->SetBranchAddress("foam", &foam, &b_foam);
   fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   fChain->SetBranchAddress("weight_tauSF", &weight_tauSF, &b_weight_tauSF);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF", &weight_globalLeptonTriggerSF, &b_weight_globalLeptonTriggerSF);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous", &weight_bTagSF_DL1r_Continuous, &b_weight_bTagSF_DL1r_Continuous);
   fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
   fChain->SetBranchAddress("weight_forwardjvt", &weight_forwardjvt, &b_weight_forwardjvt);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   fChain->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
   fChain->SetBranchAddress("tau_pt", &tau_pt, &b_tau_pt);
   fChain->SetBranchAddress("tau_eta", &tau_eta, &b_tau_eta);
   fChain->SetBranchAddress("tau_phi", &tau_phi, &b_tau_phi);
   fChain->SetBranchAddress("tau_charge", &tau_charge, &b_tau_charge);
   fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("ejets_2015", &ejets_2015, &b_ejets_2015);
   fChain->SetBranchAddress("ejets_2016", &ejets_2016, &b_ejets_2016);
   fChain->SetBranchAddress("ejets_2017", &ejets_2017, &b_ejets_2017);
   fChain->SetBranchAddress("ejets_2018", &ejets_2018, &b_ejets_2018);
   fChain->SetBranchAddress("mujets_2015", &mujets_2015, &b_mujets_2015);
   fChain->SetBranchAddress("mujets_2016", &mujets_2016, &b_mujets_2016);
   fChain->SetBranchAddress("mujets_2017", &mujets_2017, &b_mujets_2017);
   fChain->SetBranchAddress("mujets_2018", &mujets_2018, &b_mujets_2018);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("Vtxz", &Vtxz, &b_Vtxz);
   fChain->SetBranchAddress("npVtx", &npVtx, &b_npVtx);
   fChain->SetBranchAddress("HF_Classification", &HF_Classification, &b_HF_Classification);
   fChain->SetBranchAddress("HF_SimpleClassification", &HF_SimpleClassification, &b_HF_SimpleClassification);
   fChain->SetBranchAddress("HF_ClassificationGhost", &HF_ClassificationGhost, &b_HF_ClassificationGhost);
   fChain->SetBranchAddress("HF_SimpleClassificationGhost", &HF_SimpleClassificationGhost, &b_HF_SimpleClassificationGhost);
   fChain->SetBranchAddress("ttbb_categories", &ttbb_categories, &b_ttbb_categories);
   fChain->SetBranchAddress("nPDFFlavor", &nPDFFlavor, &b_nPDFFlavor);
   fChain->SetBranchAddress("tau_nTrack", &tau_nTrack, &b_tau_nTrack);
   fChain->SetBranchAddress("tau_tight", &tau_tight, &b_tau_tight);
   fChain->SetBranchAddress("tau_RNNScore", &tau_RNNScore, &b_tau_RNNScore);
   fChain->SetBranchAddress("tau_BDTScore", &tau_BDTScore, &b_tau_BDTScore);
   fChain->SetBranchAddress("tau_JetCaloWidth", &tau_JetCaloWidth, &b_tau_JetCaloWidth);
   fChain->SetBranchAddress("tau_true_pdg", &tau_true_pdg, &b_tau_true_pdg);
   fChain->SetBranchAddress("tau_true_pt", &tau_true_pt, &b_tau_true_pt);
   fChain->SetBranchAddress("tau_true_eta", &tau_true_eta, &b_tau_true_eta);
   fChain->SetBranchAddress("met_px", &met_px, &b_met_px);
   fChain->SetBranchAddress("met_py", &met_py, &b_met_py);
   fChain->SetBranchAddress("met_sumet", &met_sumet, &b_met_sumet);
   Notify();
}

Bool_t ttbar_mc16ALL_AFII::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ttbar_mc16ALL_AFII::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ttbar_mc16ALL_AFII::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ttbar_mc16ALL_AFII_cxx
