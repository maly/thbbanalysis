import re as rgx


def num_obj_req(variable_str):

    if 'bjet' in variable_str:
        num = rgx.findall(r'\d+', variable_str)[0]+1
        return 'nbjets', f'nbjets>={num}'
    elif 'nonbjet' in variable_str:
        num = rgx.findall(r'\d+', variable_str)[0]+1
        return 'nonbjets', f'nonbjets>={num}'
    elif 'fwdjet' in variable_str:
        num = rgx.findall(r'\d+', variable_str)[0]+1
        return 'nfwdjets', f'nfwdjets>={num}'
    elif ('jet' in variable_str
          and 'bjet' not in variable_str
          and 'nonbjet' not in variable_str
          and 'fwdjet' not in variable_str):

        num = rgx.findall(r'\d+', variable_str)[0]+1
        return 'njets', f'njets>={num}'


def filter_by_region(df_in, region):
    df = df_in
    #df = df.drop(df[(df.njets >= 5) & (df.nbjets >= 4)].index)
    if region != 'none':
        df = df.drop(df[(df.njets >= 5) & (df.nbjets >= 4)].index)
        df = df[(df['leptons_PLIVtight[:,0]'] == True)]
    
    if region == 'SR':
        df = df[(df['foam[:,1]'] == 0.5)]
        return df
    elif region == 'CR_ttb':
        cut = 'foam[:,1]==1.5'
        df = df[(df['foam[:,1]'] == 1.5)]
        return df
    elif region == 'CR_ttc':
        cut = 'foam[:,1]==2.5'
        df = df[(df['foam[:,1]'] == 2.5)]
        return df
    elif region == 'CR_ttlight':
        df = df[(df['foam[:,1]'] == 3.5)]
        return df
    elif region == 'CR_others':
        df = df[(df['foam[:,1]'] >= 4.5)]
        return df
    elif region == 'PR':
        return df
    elif region == 'none':
        return df


def get_sample_req(sample):

    if sample == 'ttb':
        cut = 'HF_SimpleClassification == 1'
        return '('+cut+')'
    elif sample == 'ttc':
        cut = 'HF_SimpleClassification == -1'
        return '('+cut+')'
    elif sample == 'ttlight':
        cut = 'HF_SimpleClassification == 0'
        return '('+cut+')'
    else:
        return 'abs(HF_SimpleClassification) >= 0'