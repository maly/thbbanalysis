"""
@Brief:

Wrappers for basic matplotlib figures.

Credit to @DanGuest and @InesOchoa
"""
import os 
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.ticker import FuncFormatter
from matplotlib.figure import Figure
from matplotlib import gridspec
from matplotlib.artist import setp
from matplotlib.ticker import AutoMinorLocator
from style import *
class Canvas:
    default_name = 'TestPlot.pdf'
    def __init__(self, out_path=None, figsize=(15,10), ext='pdf',fontsize=18):
        self.fig = Figure(figsize)
        self.fig.set_tight_layout(True)
        self.canvas = FigureCanvas(self.fig)
        self.ax = self.fig.add_subplot(1,1,1)
        self.out_path = str(out_path)
        self.ext = ext

        atlas_tick_style(self.ax, fontsize=fontsize)

    def save(self, out_path=None, ext=None):
        outpath = out_path or self.out_path
        assert outpath, "an output file name is required"
        out_dir, out_file = os.path.split(outpath)
        out_file = '{}.{}'.format(out_file, ext.lstrip('.'))
        if out_dir and not os.path.isdir(out_dir):	os.makedirs(out_dir)
        
        self.canvas.print_figure(outpath, bbox_inches='tight')

    def __enter__(self):
        if not self.out_path:
            self.out_path = self.default_name
        return self
    def __exit__(self, extype, exval, extb):
        if extype:	return None
        self.save(self.out_path, ext=self.ext)
        return True

def atlas_tick_style(ax, fontsize=18):
    where = {x:True for x in ['bottom','top','left','right']}
    ax.tick_params(which='both', direction='in', labelsize=fontsize, **where)
    ax.tick_params(which='minor', length=6)
    ax.tick_params(which='major', length=12)
    ax.grid(True, which='major', alpha=0.10)
    ax.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5))


def add_atlas_label(ax, x=0.74, y=0.75,
                    label_break_ratio_hack=0.65,
                    add_cut_info=False,
                    cuts = None,
                    ):
    
    # Choose Wehere everything goes 
    text_fontdic = dict(ha='left', va='center')
    atlas_fontdic = dict(weight='bold', style='italic', size=28, **text_fontdic)
    internal_fontdic = dict(size=28, **text_fontdic)
    info_fontdic = dict(size=18, **text_fontdic)
    
    hshift = 0.2 * label_break_ratio_hack
    vshift=0.05
    ax.text(x,y,'ATLAS', fontdict=atlas_fontdic,transform=ax.transAxes)
    ax.text(x+hshift,y, 'Internal',fontdict=internal_fontdic,transform=ax.transAxes)
    ax.text(x,y-vshift, r'$\sqrt{s}=13$ TeV',fontdict=info_fontdic,transform=ax.transAxes)
    
    if add_cut_info:
    	for cutIndex in range(len(cuts)):

    		cut_var, cut_val = cuts[cutIndex].split('>')
    		cut_sign = '>'
    		pretty_cut_var = VARIABLE_PRETTY_NAME_MAP[cut_var]
    		prett_cut_val = cut_val + ' ' + rf'$\rm{VARIABLE_UNITS_MAP[cut_var]}$'
    		pretty_cut = pretty_cut_var + f' {cut_sign} '+ prett_cut_val
    		shift_step = cutIndex+2
    		ax.text(x,y-shift_step*vshift, pretty_cut,fontdict=info_fontdic,transform=ax.transAxes)
        

def xlabdic(fontsize=20):
    # options for the x axis
    return dict(ha='right', x=0.98, fontsize=fontsize)
    
def ylabdic(fontsize=20):
    # options for the y axis
    return dict(ha='right', y=0.98, fontsize=fontsize)

