//////////////////////////////////////////////////////////
// This class is responsible for reading in Ntuples from 
// a Root default Analysis Class (made by MakeClass)
// then book, fill and save histograms from the different 
// plot classes with their pre-defined settings in their classes 
//////////////////////////////////////////////////////////
#ifndef TTBAR_ANALYSIS_h
#define TTBAR_ANALYSIS_h

#include "ttbar_mc16ALL_AFII.h"
#include "NumJetsPlots.h"
#include "string"
#include <TObjArray.h>

class ttbarAnalysis{
private:
	ttbar_mc16ALL_AFII* ttbar_tree; //This is the class of nominal Loose Tree 
	std::string 		outfile; //Name of output file 
	TObjArray*			histos_list;  
	NumJetsPlots*		NumJetsPlotsObj; //Class object to book and fill histograms 
public:
	ttbarAnalysis(ttbar_mc16ALL_AFII* ttbar_chain, string OutFileName);
	void init(); //initialization code
	void book_histo(); //books histogram using the plotting class
	void loop(int num_events);
	void execute(Long64_t entry);
	void finalize(); 
	virtual ~ttbarAnalysis();
};
#endif // #ifdef ttbar_mc16ALL_AFII_cxx