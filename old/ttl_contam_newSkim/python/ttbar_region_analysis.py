'''
Brief: 

Use uproot3 to read in NTuples from a .root file and plot histograms from it
This is intended as a starting point and if it was found fast it can be 
extended to a larger automated project 
'''
import uproot3 as uproot
import numpy as np
import time
import matplotlib.pyplot as plt
from var_getters import VAR_EDGES_MAP
from filters import get_overall_selection
from mpl_wrappers import Canvas, add_atlas_label, xlabdic,ylabdic
from style import (
	VARIABLE_PRETTY_NAME_MAP,
	VARIABLE_SHORT_NAME_MAP,
	SAMPLE_PRETTY_NAME_MAP,
	SAMPLE_LINE_COLOR_MAP,
	SAMPLE_LINE_STYLE_MAP,
	REGION_PRETTY_NAME_MAP
	)
from common import get_region_sample_definions, get_evt_weight

#============ PS Regions and Samples of Interest
regions = ['ttc_CR']#,'ttc_CR','ttl_CR']
samples = ['ttb','ttc','ttl']
#============ Variables to Plot 
variables  = ['n_tophad_jets_CBT0_ttAll','n_tophad_jets_CBT123_ttAll','n_tophad_jets_CBT4_ttAll',
				'n_tophad_jets_CBT5_ttAll','n_nontophad_jets_CBT0_ttAll',
				'n_nontophad_jets_CBT123_ttAll','n_nontophad_jets_CBT4_ttAll',
				'n_nontophad_jets_CBT5_ttAll']
#============ Further Selection 
cuts = ['n_nontophad_jets_CBT5_ttAll>1', 'n_tophad_jets_CBT5_ttAll>0']

 #========================================================================
 # Main()
 #========================================================================

def run():
	start_time = time.time()
	#============ Reading in Nominal Loose Tree from ROOT file 
	nl = uproot.open("/afs/cern.ch/work/m/maly/public/thbb/thbbanalysis/ttl_contam_newSkim/ttbar/ttbar_mc16ALL_AFII.root")['nominal_Loose']
	# Read all branches of the Tree into a lazy array in 200,000 entries per chunk 
	lazy_branches = nl.lazyarrays("*",entrysteps=200000)#, persistvirtual=True)
	#Get the region and sample cuts
	REGION_CUT_MAP, SAMPLE_CUT_MAP = get_region_sample_definions(lazy_branches)
	TotalNumEvents = nl.numentries 
	make_histogram(lazy_branches,variables, 
					regions, REGION_CUT_MAP, 
					samples,SAMPLE_CUT_MAP,
					cuts, TotalNumEvents,
					norm = True,
					overlay_samples = True)
	current_time = time.time()
	elapsed_time = current_time - start_time
	print("Time Elapsed ", elapsed_time)

 #========================================================================
 # Function makes histograms and calls a drawing method 
 #========================================================================
def make_histogram(lazy_branches,variables, 
					regions, region_cut_dict, 
					samples,sample_cut_dict,
					cuts, TotalNumEvents,
					norm = True,
					overlay_samples = True):
	# Loop over variables to plot 
	for variable in variables:

		# Get variable histo Edges
		edges = VAR_EDGES_MAP[variable]
		# For this variable, region to samples' histos map for overlaying
		region_to_sample_histos_map = {}
		# Get the variable to plot 
		variable_branch = lazy_branches[variable][:]
		# Get Event Weights 
		evt_weights = get_evt_weight(lazy_branches)

		# Loop over PS regions 
		for region in regions:
			#Sample to histo map for one region 
			sample_to_histos_in_region_map = {}
			# Loop over samples 
			for sample in samples: 
				# Get the bool array based on selection criteria for region-sample-cuts combo 
				# Size = TotalNumEvents 
				selection = get_overall_selection(lazy_branches,variable,cuts,region,region_cut_dict,sample,sample_cut_dict,TotalNumEvents)
				# Filter Evnt Weights 
				selec_evt_weights = evt_weights[selection]
				# Filter Variable to plot 
				selec_variable_branch = variable_branch[selection]
				# Get the weighted histogram NumEntries per bin
				count = np.histogram(selec_variable_branch, edges, weights= selec_evt_weights)[0]
				# Map sample to histogram for this region 
				sample_to_histos_in_region_map[sample] = count
			# Map region to all samples histograms
			region_to_sample_histos_map[region] = sample_to_histos_in_region_map
		# Join all the string cuts with "_" to put them in outfile name 
		cuts_for_filename = "_"
		cuts_for_filename = cuts_for_filename.join(cuts)
		# Draw the histogram 
		draw_histogram(region_to_sample_histos_map,edges,variable,cuts,cuts_for_filename,overlay_samples = overlay_samples, norm = norm)
 
 #========================================================================
 # Function draws histograms on a Canvas with appropriate settings 
 #========================================================================

def draw_histogram(region_to_sample_histos_map,edges,variable,cuts,cuts_for_filename,overlay_samples=True, norm = True):
	# If user wants to overlay samples on one plot for each region 
	if(overlay_samples):
		# Loop over regions 
		for region, samples_data in region_to_sample_histos_map.items():

			# Get the maximums of each sample's yaxis histos (largest will be used to set upper ylim)
			sample_maxes=[]
			# Get shorter variable name to use in file name 
			shorter_variable_name = VARIABLE_SHORT_NAME_MAP[variable]
			# Complete outpath for plots (add as arg option?)
			outpath = f'plots/{shorter_variable_name}_{region}_{cuts_for_filename}.pdf'

			# Make a Canvas and open it
			with Canvas(outpath) as can:
				# Loop over samples while canvas is open 
				for sample, sample_hist in samples_data.items():
					# Get the sample histogram settings 
					color = SAMPLE_LINE_COLOR_MAP[sample]
					linestyle = SAMPLE_LINE_STYLE_MAP[sample]
					label = SAMPLE_PRETTY_NAME_MAP[sample]
					# Plot histogram as a step function 
					if(norm):	yvals = np.append(sample_hist, 0)/sum(sample_hist)
					else:	yvals = np.append(sample_hist, 0)
					can.ax.step(x=edges, y=yvals, where="post", label = label,color=color,linestyle=linestyle);
					# Put legend 
					can.ax.legend(loc = 'upper right',frameon=True, fontsize=20)
					# Keep track of the maximum y-value 
					sample_max = yvals.max()
					sample_maxes.append(sample_max)

				# Add ATLAS label and cuts on plot 
				add_atlas_label(can.ax,add_cut_info=True, cuts=cuts)
				# Set the x and y axis limits 
				can.ax.set_xlim(edges[0], edges[-1]*1.2);
				can.ax.set_ylim(0, np.array(sample_maxes).max() * 1.1)
				# Get the x-axis label 
				pretty_xlabel = VARIABLE_PRETTY_NAME_MAP[variable]
				can.ax.set_xlabel(f'{pretty_xlabel}',**xlabdic(28))
				# Get the y-axis label -- always assumes weighted events 
				if(norm):	ylabel = f'Normalized Weighted Events'
				else:	ylabel = f'Weighted Events'
				can.ax.set_ylabel(ylabel,**ylabdic(28));
	# If we plot each sample for each region on a different canvas 
	else:
		# Loop over regions 
		for region, samples_data in region_to_sample_histos_map.items():
			# Get shorter variable name to use in file name 
			shorter_variable_name = VARIABLE_SHORT_NAME_MAP[variable]
			# Loop over samples while canvas is open 
			for sample, sample_hist in samples_data.items():
				# Get the sample histogram settings 
				color = SAMPLE_LINE_COLOR_MAP[sample]
				linestyle = SAMPLE_LINE_STYLE_MAP[sample]
				label = SAMPLE_PRETTY_NAME_MAP[sample]
				# Complete outpath for plots (add as arg option?)
				outpath = f'plots/{shorter_variable_name}_{region}_{sample}_{cuts_for_filename}.pdf'
				# Make a Canvas and open it
				with Canvas(outpath) as can:
					# Plot histogram as a step function 
					if(norm):	yvals = np.append(sample_hist, 0)/sum(sample_hist)
					else:	yvals = np.append(sample_hist, 0)
					can.ax.step(x=edges, y=yvals, where="post", label = label,color=color,linestyle=linestyle);
					# Put legend 
					can.ax.legend(loc = 'upper right',frameon=True, fontsize=20)
					# Keep track of the maximum y-value 
					sample_max = yvals.max()
					# Add ATLAS label and cuts on plot 
					add_atlas_label(can.ax,add_cut_info=True, cuts=cuts)
					# Set the x and y axis limits 
					can.ax.set_xlim(edges[0], edges[-1]*1.2);
					can.ax.set_ylim(0, sample_max * 1.1)
					# Get the x-axis label 
					pretty_xlabel = VARIABLE_PRETTY_NAME_MAP[variable]
					can.ax.set_xlabel(f'{pretty_xlabel}',**xlabdic(28))
					# Get the y-axis label -- always assumes weighted events 
					if(norm):	ylabel = f'Normalized Weighted Events'
					else:	ylabel = f'Weighted Events'
					can.ax.set_ylabel(ylabel,**ylabdic(28));




if __name__ == '__main__':
    run()