'''
Brief:
Common Methods for Making Plots from ROOT files

'''
import numpy as np


REGIONS_GETTERS = {
 'ttc_CR': 'CR_ttc',
 'ttb_CR': 'CR_ttb',
 'ttl_CR': 'CR_ttlight',
 'others_CR': 'CR_others',
 'SR': 'SR'
}
PROC_TREX_NAME_MAP = {
 'singletop_Wtchannel': 'singletop_Wtchannel',
 'singletop_tchannel': 'singletop_tchannel',
 'singletop_schannel': 'singletop_schannel',
 'wjets': 'Wjets',
 'zjets': 'Zjets',
 'diboson': 'VV',
 'ttw': 'ttW',
 'ttz': 'ttZ',
 'tth': 'ttH',
 'ttb': 'ttb',
 'ttc': 'ttc',
 'ttl': 'ttlight',
 }
PROC_DSIDs_MAP = {
 'singletop_Wtchannel': ['410646', '410647'],
 'singletop_tchannel': ['410658', '410659'],
 'singletop_schannel': ['410644', '410645'],
 'wjets': ['364156', '364159', '364162', '364165',
           '364170', '364173', '364176', '364179',
           '364184', '364187', '364190', '364193', '364157',
           '364160', '364163', '364166', '364171', '364174',
           '364177', '364180', '364185', '364188', '364191',
           '364194', '364158', '364161', '364164', '364167',
           '364172', '364175', '364178', '364181', '364186',
           '364189', '364192', '364195', '364168', '364169',
           '364182', '364183', '364196', '364197'],
 'zjets': ['364100', '364103', '364106', '364109', '364114', '364117',
           '364120', '364123', '364128', '364131', '364134', '364137',
           '364101', '364104', '364107', '364110', '364115', '364118',
           '364121', '364124', '364129', '364132', '364135', '364138',
           '364102', '364105', '364108', '364111', '364116', '364119',
           '364122', '364125', '364130', '364133', '364136', '364139',
           '364112', '364113', '364126', '364127', '364140', '364141'],
 'diboson': ['364250', '364253', '364254', '364255',
             '364288', '364289', '364290', '363355',
             '363356', '363357', '363358', '363359',
             '363360', '363489', '363494'],
 'ttw': ['410155'],
 'ttz': ['410156', '410157', '410218', '410219', '410220'],
 'tth': ['346343', '346344', '346345'],
 'ttb': ['410470'],
 'ttc': ['410470'],
 'ttl': ['410470'],
}


def get_sample_files(sample):
	# The paths to different subcampaigns
	mc16a_files_path = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/tH_v31_minintuples_v3/mc16a_syst/'
	mc16d_files_path = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/tH_v31_minintuples_v3/mc16d_syst/'
	mc16e_files_path = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/tH_v31_minintuples_v3/mc16e_syst/'
	sample_dsids = PROC_DSIDs_MAP[sample]
	paths = []
	for dsid in sample_dsids:
		paths.append(mc16a_files_path+dsid+'_user*')
		paths.append(mc16d_files_path+dsid+'_user*')
		paths.append(mc16e_files_path+dsid+'_user*')

	return paths


def get_region_sample_definions(lazy_branches):
    # ======== Region/Sample def branches :
    foam = lazy_branches['foam'][:, 1]  # Only foam[1]
    njets = lazy_branches['njets'][:]
    nbjets = lazy_branches['nbjets'][:]
    HF_SimpleClassification = lazy_branches['HF_SimpleClassification'][:]
    # only leptons_tight[0]
    leptons_tight = lazy_branches['leptons_tight'][:, 0]

    # ============ Region Definitions
    # Common selections
    njets_sel = (njets >= 5)
    nbjets_sel = (nbjets >= 4)
    ttH_orthog = ~(njets_sel & nbjets_sel)
    # SR
    tH_foam = (foam == 0.5)
    SR_RegionCut = (ttH_orthog & tH_foam)
    # CR_ttb
    ttb_foam = (foam == 1.5)
    ttb_RegionCut = (ttH_orthog & ttb_foam)
    # CR_ttc
    ttc_foam = (foam == 2.5)
    ttc_RegionCut = ttH_orthog & ttc_foam
    # CR_ttl
    ttl_foam = (foam == 3.5)
    ttl_RegionCut = ttH_orthog & ttl_foam
    # CR_others
    others_foam = (foam >= 4.5)
    others_RegionCut = (ttH_orthog & others_foam)

    REGION_CUT_MAP = {'SR': SR_RegionCut,
                      'ttb_CR': ttb_RegionCut,
                      'ttc_CR': ttc_RegionCut,
                      'ttl_CR': ttl_RegionCut,
                      'others_CR': others_RegionCut}

    # ============ Sample Definitions
    # Common Selections
    leptons_tight_found = (leptons_tight == 1)
    # tH
    tH_SampleCut = leptons_tight_found
    # ttb
    XXX_is_b = (HF_SimpleClassification == 1)
    ttb_SampleCut = (leptons_tight_found & XXX_is_b)
    # ttc
    XXX_is_c = (HF_SimpleClassification == -1)
    ttc_SampleCut = (leptons_tight_found & XXX_is_c)
    # ttl
    XXX_is_light = (HF_SimpleClassification == 0)
    ttl_SampleCut = (leptons_tight_found & XXX_is_light)
    # ttH
    ttH_sample_cut = leptons_tight_found
    # ttZ
    ttZ_sample_cut = leptons_tight_found
    # ttW
    ttW_sample_cut = leptons_tight_found
    # single top
    singletop_sample_cut = leptons_tight_found
    # W+jets
    w_jets_sample_cut = leptons_tight_found
    # Z+jets
    z_jets_sample_cut = leptons_tight_found
    # diboson
    diboson_sample_cut = leptons_tight_found

    SAMPLE_CUT_MAP = {'tH': tH_SampleCut,
                      'ttb': ttb_SampleCut,
                      'ttc': ttc_SampleCut,
                      'tth': ttH_SampleCut,
                      'ttz': ttZ_SampleCut,
                      'ttw': ttW_SampleCut,
                      'singletop': singletop_SampleCut,
                      'wjets': w_jets_sample_cut,
                      'zjets': z_jets_sample_cut,
                      'diboson': diboson_sample_cut,
                      'singletop_Wtchannel': singletop_SampleCut,
                      'singletop_tchannel': singletop_SampleCut,
                      'singletop_schannel': singletop_SampleCut,
                      }

    return REGION_CUT_MAP, SAMPLE_CUT_MAP


def get_evt_weight(lazy_branches):
    RunNum = lazy_branches["runNumber"][:]
    MC_Weight = lazy_branches["weight_mc"][:]
    XS_Weight = lazy_branches["xsec_weight"][:]
    PU_Weight = lazy_branches["weight_pileup"][:]
    DL1r_Weights = lazy_branches["weight_bTagSF_DL1r_Continuous"][:]
    JVT_Weight = lazy_branches["weight_jvt"][:]
    ForwardJVT_Weight = lazy_branches["weight_forwardjvt"][:]
    SoW = lazy_branches["totalEventsWeighted"][:]
    lepSF_Weight = lazy_branches["weight_leptonSF"][:]

    RunNum_290000_weight = np.zeros(len(RunNum))
    RunNum_290000_weight = np.where(
        RunNum < 290000, 36207.66, RunNum_290000_weight)

    RunNum_290000_310000_weight = np.zeros(len(RunNum))
    RunNum_290000 = RunNum >= 290000
    RunNum_310000 = RunNum < 310000
    RunNum_290000_310000_selec = RunNum_290000 & RunNum_310000
    RunNum_290000_310000_weight = np.where(
        RunNum_290000_310000_selec, 44307.4, RunNum_290000_310000_weight)

    RunNum_310000_weight = np.zeros(len(RunNum))
    RunNum_310000_weight = np.where(
        RunNum >= 310000, 58450.1, RunNum_310000_weight)

    evt_weight = ((RunNum_290000_weight)+(RunNum_290000_310000_weight)+(RunNum_310000_weight)) * \
        MC_Weight*XS_Weight*PU_Weight*DL1r_Weights * \
        JVT_Weight*ForwardJVT_Weight*lepSF_Weight/SoW

    return evt_weight
