'''
Brief:

Use uproot3 to read in NTuples from a .root file and plot histograms from it
This is intended as a starting point and if it was found fast it can be
extended to a larger automated project
'''
import uproot4 as uproot
import numpy as np
import uproot3
import time
import os
from glob import glob
import matplotlib.pyplot as plt
from pathlib import Path
#from var_getters import VAR_EDGES_MAP
from filters import get_overall_selection
from mpl_wrappers import Canvas, add_atlas_label, xlabdic, ylabdic
from style import (
    VARIABLE_PRETTY_NAME_MAP,
    VARIABLE_SHORT_NAME_MAP,
    SAMPLE_PRETTY_NAME_MAP,
    SAMPLE_LINE_COLOR_MAP,
    SAMPLE_LINE_STYLE_MAP,
    REGION_PRETTY_NAME_MAP
)
from var_getters import TREX_SYST_GETTER
from common import get_sample_files, get_region_sample_definions, get_evt_weight

# ============ Systematics
systs = ['JES effective NP modelling 1', 'JES pileup rho topology',
         'JES pileup offset NPV', 'JES pileup offset mu',
         'JES flavour response', 'JES flavour composition',
         'JES eta intercalibration modelling', 'JES BJES']

# one-sided systs which are constrained 
'''
         'MET soft reso (perp.)', 'MET soft reso (para.)'
# Btag systematics:
         'btag light-jets EV 10', 'btag light-jets EV 0',
         'btag c-jets EV 0', 'btag b-jets EV 1', 'btag b-jets EV 0'

# The theory modelling systematics -- these are one-sided and use full samples
# Only constrained ones
theory_syts = ['tt+light PS & had.', 'tt+light NLO gen.',
               'ttbar light FSR', 'tt+>=1c PS & had.',
               'tt+>=1c NLO gen.', 'ttbar cc FSR',
               'tt+>=1b PS & had.', 'tt+>=1b NLO gen.',
               'ttbar bb FSR', 'XS tt (inclusive)']
'''
# ============ PS Regions and Samples of Interest
regions = ['ttc_CR', 'ttb_CR', 'ttl_CR', 'others_CR', 'SR']
samples = ['singletop_Wtchannel', 'singletop_tchannel', 'singletop_schannel',
           'wjets', 'zjets', 'diboson',
           'ttw', 'ttz', 'tth',
           'ttb', 'ttc', 'ttl']

# ============ Variables to Plot
variables = ['']

# ============ Further Selection
cuts = ['']

# ========================================================================
# Main()
# ========================================================================


def run():
    start_time = time.time()
    for syst in systs:
        print(f'INFO:: Processing {syst} systematic')
        # Get the systematic tree
        up_syst, down_syst = TREX_SYST_GETTER[syst]
        # Loop over all samples
        for sample in samples:
            print(f'INFO:: Processing {sample} sample')
            # Load up the data for this sample:
            file_paths = get_sample_files(sample)
            file_paths = [fp for fp in file_paths for glob_out in glob(fp) if os.path.isfile(glob_out)]
            up_file_paths = [fp + ':' + up_syst for fp in file_paths]
            down_file_paths = [fp + ':' + down_syst for fp in file_paths]

            up_numentries = uproot3.numentries(file_paths, up_syst, total=False)
            down_numentries = uproot3.numentries(file_paths, down_syst, total=False)
            nominal_numentries = uproot3.numentries(file_paths, "nominal_Loose", total=False)

            total_up_numentries = sum([file_numentries for file_name, file_numentries in up_numentries.items()])
            total_down_numentries = sum([file_numentries for file_name, file_numentries in down_numentries.items()])
            total_nom_numentries = sum([file_numentries for file_name, file_numentries in nominal_numentries.items()])

            if(total_up_numentries != total_down_numentries):
                print(f'=================SUMMARY======================')
                print("Up and Down Systematics don't have equal lenghts")
                print(f'NOMINAL - {up_syst} contains {total_nom_numentries - total_up_numentries} entries')
                print(f'NOMINAL - {down_syst} contains {total_nom_numentries - total_down_numentries } entries')

                print(f'=================END SUMMARY=========================')

            empty_files = [file_name for file_name, file_numentries in up_numentries.items() if file_numentries == 0]
            print(f'WARNING:: The following files are empty: \n')
            print(*empty_files, sep='\n')
            # try:
            #     up_tree = uproot.lazy(up_file_paths, filter_branches='weight_mc', step_size='1 GB')
            #     down_tree = uproot.lazy(down_file_paths, filter_branches='weight_mc', step_size='1 GB')
            # except TypeError as e:
            #     print(f'=================================================')
            #     print(f'Systematic Tree {syst} missing in file')
            #     print(f'ERROR:: {e}')
            #     print(f'=================================================')

                #up_numentries = uproot3.numentries(file_paths, up_syst)
                #down_numentries = uproot3.numentries(file_paths, down_syst)
                #up_tree = uproot.lazy(up_file_paths, allow_missing=True, step_size='1 GB')
                #down_tree = uproot.lazy(down_file_paths, allow_missing=True, step_size='1 GB')



            # up_branches = up_tree.lazyarrays("*")
            # down_branches = down_tree.lazyarrays("*")
            # UP_REGION_CUT_MAP, UP_SAMPLE_CUT_MAP = get_region_sample_definions(up_branches)
            # DOWN_REGION_CUT_MAP, DOWN_SAMPLE_CUT_MAP = get_region_sample_definions(down_branches)



            # for region in regions:

            #     up_selection = get_region_and_sample_selection(region, UP_REGION_CUT_MAP,
            #                                                    sample, UP_SAMPLE_CUT_MAP)
            #     down_selection = get_region_and_sample_selection(region, DOWN_REGION_CUT_MAP,
            #                                                      sample, DOWN_SAMPLE_CUT_MAP)

            #     dummy_branch = lazy_branches['e'][:]




    # ============ Reading in Nominal Loose Tree from ROOT file
    file = uproot.lazy(
        '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/tH_v31_minintuples_v3/mc16a_syst/410470_user.*:nominal_Loose')

    # for f in file:
    #     n+=1
    #     print(n)
#     # Read all branches of the Tree into a lazy array in 200,000 entries per chunk
#     # , persistvirtual=True)
#     lazy_branches = nl.lazyarrays("*", entrysteps=200000)
#     # Get the region and sample cuts
#     REGION_CUT_MAP, SAMPLE_CUT_MAP = get_region_sample_definions(lazy_branches)
#     TotalNumEvents = nl.numentries
#     make_histogram(lazy_branches, variables,
#                    regions, REGION_CUT_MAP,
#                    samples, SAMPLE_CUT_MAP,
#                    cuts, TotalNumEvents,
#                    norm=True,
#                    overlay_samples=True)
#     current_time = time.time()
#     elapsed_time = current_time - start_time
#     print("Time Elapsed ", elapsed_time)

# ========================================================================
# Function makes histograms and calls a drawing method
# ========================================================================


def make_histogram(lazy_branches, variables,
                   regions, region_cut_dict,
                   samples, sample_cut_dict,
                   cuts, TotalNumEvents,
                   norm=True,
                   overlay_samples=True):
    # Loop over variables to plot
    for variable in variables:

        # Get variable histo Edges
        edges = VAR_EDGES_MAP[variable]
        # For this variable, region to samples' histos map for overlaying
        region_to_sample_histos_map = {}
        # Get the variable to plot
        variable_branch = lazy_branches[variable][:]
        # Get Event Weights
        evt_weights = get_evt_weight(lazy_branches)

        # Loop over PS regions
        for region in regions:
            # Sample to histo map for one region
            sample_to_histos_in_region_map = {}
            # Loop over samples
            for sample in samples:
                # Get the bool array based on selection criteria for region-sample-cuts combo
                # Size = TotalNumEvents
                selection = get_overall_selection(
                    lazy_branches, variable, cuts, region, region_cut_dict, sample, sample_cut_dict, TotalNumEvents)
                # Filter Evnt Weights
                selec_evt_weights = evt_weights[selection]
                # Filter Variable to plot
                selec_variable_branch = variable_branch[selection]
                # Get the weighted histogram NumEntries per bin
                count = np.histogram(selec_variable_branch,
                                     edges, weights=selec_evt_weights)[0]
                # Map sample to histogram for this region
                sample_to_histos_in_region_map[sample] = count
            # Map region to all samples histograms
            region_to_sample_histos_map[region] = sample_to_histos_in_region_map
        # Join all the string cuts with "_" to put them in outfile name
        cuts_for_filename = "_"
        cuts_for_filename = cuts_for_filename.join(cuts)
        # Draw the histogram
        draw_histogram(region_to_sample_histos_map, edges, variable, cuts,
                       cuts_for_filename, overlay_samples=overlay_samples, norm=norm)

# ========================================================================
# Function draws histograms on a Canvas with appropriate settings
# ========================================================================


def draw_histogram(region_to_sample_histos_map, edges, variable, cuts, cuts_for_filename, overlay_samples=True, norm=True):
    # If user wants to overlay samples on one plot for each region
    if(overlay_samples):
        # Loop over regions
        for region, samples_data in region_to_sample_histos_map.items():

            # Get the maximums of each sample's yaxis histos (largest will be used to set upper ylim)
            sample_maxes = []
            # Get shorter variable name to use in file name
            shorter_variable_name = VARIABLE_SHORT_NAME_MAP[variable]
            # Complete outpath for plots (add as arg option?)
            outpath = f'plots/{shorter_variable_name}_{region}_{cuts_for_filename}.pdf'

            # Make a Canvas and open it
            with Canvas(outpath) as can:
                # Loop over samples while canvas is open
                for sample, sample_hist in samples_data.items():
                    # Get the sample histogram settings
                    color = SAMPLE_LINE_COLOR_MAP[sample]
                    linestyle = SAMPLE_LINE_STYLE_MAP[sample]
                    label = SAMPLE_PRETTY_NAME_MAP[sample]
                    # Plot histogram as a step function
                    if(norm):
                        yvals = np.append(sample_hist, 0)/sum(sample_hist)
                    else:
                        yvals = np.append(sample_hist, 0)
                    can.ax.step(x=edges, y=yvals, where="post",
                                label=label, color=color, linestyle=linestyle)
                    # Put legend
                    can.ax.legend(loc='upper right', frameon=True, fontsize=20)
                    # Keep track of the maximum y-value
                    sample_max = yvals.max()
                    sample_maxes.append(sample_max)

                # Add ATLAS label and cuts on plot
                add_atlas_label(can.ax, add_cut_info=True, cuts=cuts)
                # Set the x and y axis limits
                can.ax.set_xlim(edges[0], edges[-1]*1.2)
                can.ax.set_ylim(0, np.array(sample_maxes).max() * 1.1)
                # Get the x-axis label
                pretty_xlabel = VARIABLE_PRETTY_NAME_MAP[variable]
                can.ax.set_xlabel(f'{pretty_xlabel}', **xlabdic(28))
                # Get the y-axis label -- always assumes weighted events
                if(norm):
                    ylabel = f'Normalized Weighted Events'
                else:
                    ylabel = f'Weighted Events'
                can.ax.set_ylabel(ylabel, **ylabdic(28))
    # If we plot each sample for each region on a different canvas
    else:
        # Loop over regions
        for region, samples_data in region_to_sample_histos_map.items():
            # Get shorter variable name to use in file name
            shorter_variable_name = VARIABLE_SHORT_NAME_MAP[variable]
            # Loop over samples while canvas is open
            for sample, sample_hist in samples_data.items():
                # Get the sample histogram settings
                color = SAMPLE_LINE_COLOR_MAP[sample]
                linestyle = SAMPLE_LINE_STYLE_MAP[sample]
                label = SAMPLE_PRETTY_NAME_MAP[sample]
                # Complete outpath for plots (add as arg option?)
                outpath = f'plots/{shorter_variable_name}_{region}_{sample}_{cuts_for_filename}.pdf'
                # Make a Canvas and open it
                with Canvas(outpath) as can:
                    # Plot histogram as a step function
                    if(norm):
                        yvals = np.append(sample_hist, 0)/sum(sample_hist)
                    else:
                        yvals = np.append(sample_hist, 0)
                    can.ax.step(x=edges, y=yvals, where="post",
                                label=label, color=color, linestyle=linestyle)
                    # Put legend
                    can.ax.legend(loc='upper right', frameon=True, fontsize=20)
                    # Keep track of the maximum y-value
                    sample_max = yvals.max()
                    # Add ATLAS label and cuts on plot
                    add_atlas_label(can.ax, add_cut_info=True, cuts=cuts)
                    # Set the x and y axis limits
                    can.ax.set_xlim(edges[0], edges[-1]*1.2)
                    can.ax.set_ylim(0, sample_max * 1.1)
                    # Get the x-axis label
                    pretty_xlabel = VARIABLE_PRETTY_NAME_MAP[variable]
                    can.ax.set_xlabel(f'{pretty_xlabel}', **xlabdic(28))
                    # Get the y-axis label -- always assumes weighted events
                    if(norm):
                        ylabel = f'Normalized Weighted Events'
                    else:
                        ylabel = f'Weighted Events'
                    can.ax.set_ylabel(ylabel, **ylabdic(28))


if __name__ == '__main__':
    run()
