'''
Brief: 

Plot styling methods and maps 

'''
import numpy as np

VARIABLE_PRETTY_NAME_MAP = {
	'n_tophad_jets_CBT0_ttAll': r'$N^{\rm{HadTop}}_{\rm{jets, CBT0}}$, $t\bar{t}$+jets hyp',
	'n_tophad_jets_CBT123_ttAll':  r'$N^{\rm{HadTop}}_{\rm{jets, CBT123}}$, $t\bar{t}$+jets hyp',
	'n_tophad_jets_CBT4_ttAll':  r'$N^{\rm{HadTop}}_{\rm{jets, CBT4}}$, $t\bar{t}$+jets hyp',
	'n_tophad_jets_CBT5_ttAll':  r'$N^{\rm{HadTop}}_{\rm{jets, CBT5}}$, $t\bar{t}$+jets hyp',
	'n_nontophad_jets_CBT0_ttAll':  r'$N^{\rm{NotHadTop}}_{\rm{jets, CBT0}}$, $t\bar{t}$+jets hyp',
	'n_nontophad_jets_CBT123_ttAll':  r'$N^{\rm{NotHadTop}}_{\rm{jets, CBT123}}$, $t\bar{t}$+jets hyp',
	'n_nontophad_jets_CBT4_ttAll':  r'$N^{\rm{NotHadTop}}_{\rm{jets, CBT4}}$, $t\bar{t}$+jets hyp',
	'n_nontophad_jets_CBT5_ttAll':  r'$N^{\rm{NotHadTop}}_{\rm{jets, CBT5}}$, $t\bar{t}$+jets hyp',
 }
VARIABLE_UNITS_MAP = {
	'n_tophad_jets_CBT0_ttAll': '',
	'n_tophad_jets_CBT123_ttAll':  '',
	'n_tophad_jets_CBT4_ttAll':  '',
	'n_tophad_jets_CBT5_ttAll':  '',
	'n_nontophad_jets_CBT0_ttAll':  '',
	'n_nontophad_jets_CBT123_ttAll':  '',
	'n_nontophad_jets_CBT4_ttAll':  '',
	'n_nontophad_jets_CBT5_ttAll':  '',
 
}
VARIABLE_SHORT_NAME_MAP = {
	'n_tophad_jets_CBT0_ttAll': 'Njets_HadTop_CBT0_ttALL',
	'n_tophad_jets_CBT123_ttAll':  'Njets_HadTop_CBT123_ttALL',
	'n_tophad_jets_CBT4_ttAll':  'Njets_HadTop_CBT4_ttALL',
	'n_tophad_jets_CBT5_ttAll':  'Njets_HadTop_CBT5_ttALL',
	'n_nontophad_jets_CBT0_ttAll': 'Njets_NotHadTop_CBT0_ttALL',
	'n_nontophad_jets_CBT123_ttAll':  'Njets_NotHadTop_CBT123_ttALL',
	'n_nontophad_jets_CBT4_ttAll':  'Njets_NotHadTop_CBT4_ttALL',
	'n_nontophad_jets_CBT5_ttAll':  'Njets_NotHadTop_CBT5_ttALL',
 }

SAMPLE_PRETTY_NAME_MAP = {
 	'ttb':	r'$t\bar{t} + \geq 1b$',
 	'ttc':	r'$t\bar{t} + \geq 1c$',
 	'ttl':	r'$t\bar{t} + \geq 0\rm{ lights}$',
 }

SAMPLE_LINE_COLOR_MAP = {
 	'ttb':	'blue',
 	'ttc':	'red',
 	'ttl':	'green',
 }
SAMPLE_LINE_STYLE_MAP = {
 	'ttb':	'--',
 	'ttc':	'--',
 	'ttl':	'--',
 }

REGION_PRETTY_NAME_MAP = {
 	'ttb_CR':	r'ttb CR',
 	'ttc_CR':	r'ttc CR',
 	'ttl_CR':	r'ttl CR',
 	'tH_CR'	:	r'SR',
 }