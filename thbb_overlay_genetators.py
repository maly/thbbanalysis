'''
Author: Mohamed Aly
Email: maly@cern.ch
Description:

This script is responsible for loading slimmed Ntuples and plotting requested
variables for one process but different generators in various PS regions. This script
CANNOT be used with real DATA.

  - The path to the slimmed datasets -- same as output path form thbb_slimming.py
  - The process to be studied
  - The variables to plot
  - The MC generators
  - PS regions to make the plots in.
  - The path where the plots should be saved

'''

import uproot4 as uproot
from argparse import ArgumentParser, ArgumentTypeError
import sys
import os
import numpy as np
import pandas as pd
import h5py
import awkward as ak
from termcolor import cprint
from pathlib import Path
from tools.common import dhelp, REGION_TO_REGION_NAME_MAP
from tools.input_studies_common import (PROC_TO_AVAIL_GENS_MAP,
                                        GEN_IN_ARG_TO_GEN_NAME_MAP)
from tools.var_getters import VAR_TO_BRANCH_NAME_MAP, VAR_TO_EDGES_MAP
from tools.selectors import filter_by_region
from tools.style import GEN_SIM_TO_COLOR_MAP, GEN_SIM_TO_PRETTY_NAME, VAR_TO_PRETTY_NAME, PROC_TO_PRETTY_NAME, GEN_SIM_TO_SHORT_NAME
from tools.plotting import (CanvasWithRatio, atlas_style, add_atlas_label, apply_plot_settings,
                            stick_text_on_plot, add_presel_cuts)
# ==================================================================
# Get arguments from CL
# ==================================================================
# Help messages
# ================
_OUTDIR_HELP = dhelp('Path to directory where output files will be stored ')
_INPUT_HELP = dhelp('The directory where the slimmed json files are stored')
_PROCESSES_HELP = dhelp('The samples to check in form: proc_name,(optional)gen,(optional)FS/AFII')
_GENS_HELP = dhelp('The generators to compare')
_VARS_HELP = dhelp('The branches to load from the Ntuples')
_REGIONS_HELP = dhelp('The Phase-Space regions to make plots in')
# ================
# Defaults
# ================

DEFAULT_IN_DIR = '/eos/user/m/maly/thbb/analysis_files/slimmed_Ntuples/'
DEFAULT_OUT_DIR = './output/overlay_generators/'
DEFAULT_PROC = 'ttb'
DEFAULT_GENS = [['nominal','FS'], ['PH7new','AFII']]
DEFAULT_VARS = ['bdt_tH', 'bdt_ttb', 'bdt_ttc', 'bdt_ttlight', 'bdt_others']
DEFAULT_REGIONS = ['preselec', 'tH', 'ttb', 'ttc', 'ttlight', 'others']



def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('--indir', default=DEFAULT_IN_DIR, help=_INPUT_HELP)
    parser.add_argument('--outdir', type=Path, default=DEFAULT_OUT_DIR, help=_OUTDIR_HELP)
    parser.add_argument('--process', choices=PROC_TO_AVAIL_GENS_MAP.keys(), default=DEFAULT_PROC, help=_PROCESSES_HELP)
    parser.add_argument('--gen-sim', nargs="+", action = 'append', help=_GENS_HELP)
    parser.add_argument('--regions', nargs="+", choices=REGION_TO_REGION_NAME_MAP.keys(), default=DEFAULT_REGIONS, help=_REGIONS_HELP)
    parser.add_argument('--variables', nargs="+", choices=VAR_TO_BRANCH_NAME_MAP.keys(), default=DEFAULT_VARS, help=_VARS_HELP)
    parser.add_argument('--tree', default='nominal_Loose')
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--unweight', action='store_true')
    return parser.parse_args()


def run():
	# ======================
	# Prepare args
	# ======================
	args = get_args()
	# Be Verbose ?
	verbose = args.verbose
	# Make unweighted histos ?
	unweight = args.unweight
	# Ntuples Directory
	in_dir = args.indir
	# Slimmed files directory
	out_dir = args.outdir
	out_dir.mkdir(parents=True, exist_ok=True)
	# The prcoess requested
	proc = args.process
	# The MC generators to compare
	gen_sim_combos = args.gen_sim
	if any(len(gen_sim) != 2 for gen_sim in gen_sim_combos):
		cprint(f'ERROR: Make sure you provide pairs of arguments for --gen-sim')
		exit(0)
	if len(gen_sim_combos) == 0:
		gen_sim_combos = DEFAULT_GENS	
	# The variables to plot
	variables = args.variables
	# The PS regions to compare
	regions = args.regions
	if verbose:
		cprint(f'INFO:: The sample to check is {proc} and the generators to compare are {gen_sim_combos}\n', 'green')
	tree = args.tree
	# Loaad the input datasets
	proc_gen_sim_combos = [(proc, gen_sim[0], gen_sim[1]) for gen_sim in gen_sim_combos]
	proc_gen_sim_to_vars_map = prepare_input_data(in_dir, proc_gen_sim_combos, variables, tree)
	# Loop over the regions of phase-space to make plots in
	for region in regions:
		# Get the region name as used in the analysis configuration
		proper_region = REGION_TO_REGION_NAME_MAP[region]
		if verbose:
			cprint(f'INFO:: Processing the following region: {proper_region}', 'green')
		# Loop over the requested variables
		for var in variables:
			if verbose:
				cprint(f'INFO:: Processing the following variable: {var}', 'green')
			# For each region, a canvas is built for each variable to overlay different generators
			hists = []
			# Get the variable edges
			var_edges = VAR_TO_EDGES_MAP[var]
			nominal_exists = False
			with CanvasWithRatio(out_dir/proc/var/f'{proc}_{var}_{proper_region}.pdf', figsize=(20, 20)) as can:
				max_histos = []
				# Loop over the generators
				for proc_gen_sim, unfiltered_data in proc_gen_sim_to_vars_map.items():
					# Filter the data to select the current region
					data = filter_by_region(unfiltered_data, proper_region)
					# Get the variable data
					var_data = np.array(data[var])
					# Get the generator name and proc name (same per script run)
					gen, sim = proc_gen_sim.split("_")[-2:]
					gen_sim = '_'.join([gen,sim])
					proc = ''.join(proc_gen_sim.split("_")[:-2])
					# Make plots pretty with latex names and nice colors
					gen_name = GEN_SIM_TO_PRETTY_NAME[gen_sim]
					gen_short = GEN_SIM_TO_SHORT_NAME[gen_sim]
					gen_color = GEN_SIM_TO_COLOR_MAP[gen_sim]
					proc_name = PROC_TO_PRETTY_NAME[proc]
					var_name = VAR_TO_PRETTY_NAME[var]
					# Get the weights
					weights = np.array(data['weights'])
					if unweight:
						weights = np.ones(weights.shape)
					# Make the histogram
					hist = can.ax.hist(var_data, bins=var_edges, weights=weights, stacked=False, histtype='step', edgecolor=gen_color, linewidth=3, label=gen_name, fill=False)															
					# Get the errors on weighted bins
					errs, bin_centers = get_errs(var_data, weights, hist)
					# Plot error bars
					can.ax.errorbar(bin_centers, hist[0], yerr=errs, linestyle="none", color=gen_color)
					hists.append({'hist': list(hist[0]), 'name':gen_short,
								  'err': errs, 'color':gen_color})
					if 'nominal' in gen_sim and nominal_exists is False:
						nominal_exists = True
				# =================== Ratio Plots ======================
				maxi = max(hists, key=lambda h: sum(h['hist']))
				if nominal_exists:
					nominal = [hist for hist in hists if 'nominal' in hist['name'] ][0]
					not_nominal_idx = [idx for idx, hist in enumerate(hists) if 'nominal' not in hist['name'] ]
				largest_hist_idx = hists.index(maxi) if not nominal_exists else nominal
				max_ratios = []
				min_ratios = []
				if nominal_exists and len(hists)==3:
					alt1 = np.array(hists[not_nominal_idx[0]]['hist'])
					alt2 = np.array(hists[not_nominal_idx[1]]['hist'])
					alt1_name = np.array(hists[not_nominal_idx[0]]['name'])
					alt2_name = np.array(hists[not_nominal_idx[1]]['name'])
					alt1_err = np.array(hists[not_nominal_idx[0]]['err'])
					alt2_err = np.array(hists[not_nominal_idx[1]]['err'])
					nominal_hist = np.array(nominal['hist'])
					nominal_err =  np.array(nominal['err'])
					nominal_name =  np.array(nominal['name'])
					ratio =np.subtract(alt1,alt2)/nominal_hist
					ratio_err = ratio*np.sqrt((alt1_err/alt1)**2 +(alt2_err/alt2)**2+(nominal_err/nominal_hist)**2)				
					can.ax2.errorbar((var_edges[1:]+var_edges[:-1])/2,
										 ratio, 
										 yerr=ratio_err,
										** {'linestyle': 'none',
											'ms': 15,
											'marker': 'x',
											'color': 'red'})
					apply_plot_settings(can.ax2, 
											**{'ylabel': r'diff/nominal',
											   'ylabel_size': 24,
											   'yticklabels_size': 24,})		
					min_ratios.append(min(ratio))
					max_ratios.append(max(ratio))
				
				else:							
					for idx, hist_map in enumerate(hists):
						if idx == largest_hist_idx:
							continue
						else:
							hist = np.array(hist_map['hist'])
							hist_name = hist_map['name']
							baseline_name = hists[largest_hist_idx]['name']
							hist_err = hist_map['err']
							hist_color = hist_map['color']

							ratio = hist/np.array(hists[largest_hist_idx]['hist'])
							ratio_err = ratio*np.sqrt( (hist_err/hist)**2 +
														(hists[largest_hist_idx]['err']/np.array(hists[largest_hist_idx]['hist']))**2)
							can.ax2.errorbar((var_edges[1:]+var_edges[:-1])/2,
											ratio, 
											yerr=ratio_err,
											** {'linestyle': 'none',
												'ms': 15,
												'marker': 'x',
												'color': hist_color})
							apply_plot_settings(can.ax2, 
												**{'ylabel': rf'$\frac{{{hist_name}}}{{{baseline_name}}}$',
												'ylabel_size': 38,
												'yticklabels_size': 24,})		
							min_ratios.append(min(ratio))
							max_ratios.append(max(ratio))
				if min(min_ratios) < 0:
					min_factor = 1.7
				else:
					min_factor = 0.2
				if max(max_ratios)<0:
					max_factor=0.6
				else:
					max_factor=1.7
				can.ax2.set_ylim([min_factor*min(min_ratios), max_factor*max(max_ratios)])
				maxima = [max(h['hist']) for h in hists ]
				can.ax.set_ylim([0, 1.2*max(maxima)])
				apply_plot_settings(can.ax2, **{'xlabel': var_name,
								 				'xlabel_size': 30,
												 'xticklabels_size': 24,})

				# Settings to make plots pretty
				plot_settings = {

								 'ylabel': 'Weighted Number of Events' if unweight is not True else 'Raw Number of Events',
								 'ylabel_size': 30,
								 'yticklabels_size': 24,
								 'legend_size':	28,
								 'legend_pos': 'upper right',
								 }

				apply_plot_settings(can.ax, **plot_settings)


				text_settings = {
					'text':	["Region: " + proper_region, "Sample: " + proc_name],
					'textsize':	28,
					'xpos':		0.75,
					'ypos':		0.75,
					'vspace':	0.03,

				}
				stick_text_on_plot(can.ax, **text_settings)
				add_atlas_label(can.ax, y=0.96, internal=False, simulation=True, prelim=True)
				if proper_region != 'none': 
					add_presel_cuts(can.ax, x=0.75, y=0.7)


# ==================================================================
# Read in the slimmed datasets and get a dataframe for each
# Process+Generator combo with all requested variables, if they are available
# ==================================================================
def prepare_input_data(in_dir, proc_gen_sim_combos, requested_vars, tree):
	# Prepare a dictionary to hold the dataframe for each proc+gen+sim combo
	proc_gen_sim_to_vars_map = {}
	# Loop over the requested proc+generator+sim combos
	for proc_gen_sim_combo in proc_gen_sim_combos:
		# Make a string with the proc and generator and simType combined
		str_combo = proc_gen_sim_combo[0]+'_'+proc_gen_sim_combo[1]+'_'+proc_gen_sim_combo[2]
		# Array to hold the dataframe from each slimmed input file
		# There are multiple files due to the strucure of slimming script
		list_of_dfs = []
		# Loop over the files in the slimmed datasets directory
		for file_name in os.listdir(in_dir):
			# Skip irrelevant files
			if str_combo not in file_name or tree not in file_name:
				continue
			# Prepare a dictionary to hold the different datasets in each file
			# N-datasets for N variables kept during slimming
			var_name_to_data = {}
			# Open each file
			with h5py.File(in_dir+file_name, "r") as file:
				# Loop over the available variables, and save their data
				for var_name in list(file.keys()):
					var_name_to_data[var_name] = np.array(file[var_name])
			# Make a dataframe with columns = variables
			df = pd.DataFrame(var_name_to_data)
			# Append the dataframe from this file to the ones from other files
			list_of_dfs.append(df)
		# If no files were found for the requested proc+gen combo, raise error
		try:
			proc_gen_sim_to_vars_map[str_combo] = pd.concat(list_of_dfs, ignore_index=True)
		except ValueError:
			raise ValueError(f'The requested process+generator+tree combo [{str_combo}_{tree}] is not available in the slimmed datasets...')
	# Finally, get the names of variables available from the slimming,
	# and cross-check them again requested variables
	avail_variables = list(proc_gen_sim_to_vars_map[list(proc_gen_sim_to_vars_map.keys())[0]].columns)
	# Raise error if requested variables not available in slimmed variables
	if not all(requested_variable in avail_variables for requested_variable in requested_vars):
		raise ValueError(f'The variables requested are not all in the slimmed ntuples. You can choose from: {avail_variables}\n')

	return proc_gen_sim_to_vars_map


# ==================================================================
# Get the error on weighted histogram
# ==================================================================
def get_errs(var_data, weights, histo):
	histo_edges = np.array(histo[1])
	all_errs = []
	all_centers = []
	for bin_edge_idx in range(len(histo_edges)-1):
		this_edge = histo_edges[bin_edge_idx]
		next_edge = histo_edges[bin_edge_idx+1]
		data_in_bin = np.logical_and(var_data > this_edge, var_data <= next_edge)
		weights_in_bin = weights[data_in_bin]
		weights_sq = [weights_in_bin**2]
		err_sq = np.sum(weights_sq)
		err = np.sqrt(err_sq)
		all_errs.append(err)
		all_centers.append((this_edge+next_edge)/2)
	return all_errs, all_centers


if __name__ == '__main__':
    run()
